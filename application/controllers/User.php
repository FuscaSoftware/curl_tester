<?php

class User extends MY_Controller
{


//    public $auth_ldap_status = false;

    public $default_helpers = ['form', 'html', 'url', 'sb_helper'];
    public $libraries = ['form_validation'];

    /**
     * Login constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function init() {
        parent::init();
    }

    /**
     */
    public function index() {
        $view_data['site_title'] = "Hazelnut - Datenbank - Spar mit! Reisen";
        $view_data['page_title'] = "User-Management";
        $output[] = ci()->get_view('usermanager/index', $view_data);
        $this->show_page($output, ['view_data' => $view_data]);
    }


    public function list() {
        $this->loader()->library('mustache_lib');
        $this->loader()->helper('sb_helper');
        $this->load->model('user/user_model');

        $view_data['site_title'] = "Hazelnut - Datenbank - Spar mit! Reisen";
        $view_data['page_title'] = "User-Management";

        $this->data = [];

        $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
            'datatable_id' => uniqid('dt_user_'),
            'columns' => [
                "id" => "id",
                "displayname" => "displayname",
                "samAccountName" => "samAccountName",
                "mailnickname" => "mailnickname",
                "mail" => "mail",
                "roles" => ['label' => "Rollen", 'class' => "user_roles"],
                "last_login" => "last_login",
            ],
            'ajax_source' => site_url() . "ajax/data_json/user/list/?request_type=ajax",
            'js_file' => "js_hazel/datatables/user.js",
            'config_key' => 'user',
        ];

        $output[] = ci()->get_view('usermanager/list', $view_data);
        $this->show_page($output, ['view_data' => $view_data]);
    }

    public function roles() {
        $this->loader()->library('mustache_lib');
        $this->loader()->helper('sb_helper');
        $this->load->model('user/user_model');

        $view_data['site_title'] = "Hazelnut - Datenbank - Spar mit! Reisen";
        $view_data['page_title'] = "Rollen";

        $this->data = [];

        $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
            'datatable_id' => uniqid('dt_user_'),
            'columns' => [
                "id" => "id",
                "role" => ['label' => "Rolle", 'class' => "user_role"],
//                    "last_login" => "last_login",
                "description" => ['label' => "Beschreibung",],
                "users" => ['label' => "Nutzer", 'class' => "users_of_role"],
//                "userHasRole" => ['label' => "Nutzer hat Rolle", 'class' => "user_has_role"],
            ],
            'ajax_source' => site_url() . "ajax/data_json/roles/list/?request_type=ajax",
            'js_file' => "js_hazel/datatables/user.js",
            'config_key' => 'user',
        ];

        $output[] = ci()->get_view('usermanager/list', $view_data);
        $this->show_page($output, ['view_data' => $view_data]);
    }

    public function rights() {
        $this->loader()->library('mustache_lib');
        $this->loader()->helper('sb_helper');
        $this->load->model('user/user_model');

        $view_data['site_title'] = "Hazelnut - Datenbank - Spar mit! Reisen";
        $view_data['page_title'] = "Rechte-Management";
        $this->data = [];

        $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
            'datatable_id' => uniqid('dt_user_'),
            'columns' => [
                "ID" => "ID",
                "controller" => "Controller",
                "method" => "Methode",
                "role" => "Rolle",
                "allow" => ['label' => "Zugang", 'class' => "switch_law"],
                "modified" => "bearbeitet",
//                "id",
//                "displayname",
//                "samAccountName" => "Username",
//                "mailnickname",
//                "mail",
//                "last_login",
            ],
            'ajax_source' => site_url() . "ajax/data_json/user/rights/?request_type=ajax",
            'js_file' => "js_hazel/datatables/user.js",
            'config_key' => 'user',
        ];

        $output[] = ci()->get_view('usermanager/list', $view_data);
        $this->show_page($output, ['view_data' => $view_data]);
    }

    public function show_profile(int $user_id) {
        $view_data['site_title'] = "Hazelnut - Datenbank - Spar mit! Reisen";
        $view_data['page_title'] = "Rechte-Management";
//        ci()->dump($_SESSION);
//        $output[] = "<h3></h3>";
        if ($user_id == $this->acl_lib->get_user_id()) {
            $output[] = "<h3>Mein Profil</h3>";
            $output[] = implode("\n<br>", [
                "displayname: " . $_SESSION['logged_in']['displayname'],
                "mailnickname: " . $_SESSION['logged_in']['mailnickname'],
                "mail: " . $_SESSION['logged_in']['mail'],
                "<br>"
            ]);
        }

//        $output[] = $this->acl_lib->;
        $roles = $this->acl_lib->get_roles_from_db();
        $output[] = "<h3>Meine Gruppen</h3>" . implode("\n<br>", $roles);
        $this->show_page($output, ['view_data' => $view_data]);
    }

    /**
     * @acl regard-params: 2
     */
    public function toggle_right() {
        $this->load->model('user/user_model');
        $data = $this->input->get_post("data");
        list($controller_id, $method_id, $role_id) = explode("|", $data['id']);
        $where = [
            'controller_id' => $controller_id,
            'method_id' => $method_id,
            'role_id' => $role_id,
        ];
        $item = $this->user_model()->get_items3([], null, [
            'where' => $where,
            'table' => $this->acl_lib->law_table,
        ])->first();
        $item->allow = ($data['allow'] == "true" || $data['allow'] == 1) ? 1 : 0;

        $r = $this->user_model()->update_item($item, $this->acl_lib->law_table, $where);
        if ($r)
            $this->message("Recht gesetzt.", "success", "default", 500);
        else
            $this->message("Fehler.", "warning", "default", 5000);
//        $this->dump_query($this->user_model()->last_query());


//        $ref = new ReflectionMethod($this, "toggle_right");
//        $doc = $ref->getDocComment();
//
//        $data = [
//            $doc
//        ];
//        $this->dump($data);

        $this->show_ajax_message($this->data);

    }

    public function toggle_role() {
        $this->load->model('user/user_model');
        /** @var Role_user_model $role_user_model */
        $role_user_model = ($this->load->model('user/role_user_model'))? $this->role_user_model : null;
        $data = $this->input->get_post("data");
        $role_id = $data['role_id'];
        $user_id = $data['user_id'];
        $where = [
//            'controller_id' => $controller_id,
//            'method_id' => $method_id,
            'role_id' => $role_id,
        ];
        $item = $this->user_model()->get_items3([], null, [
            'where' => $where,
            'table' => $this->acl_lib->law_table,
        ])->first();
        $userHasRole = ($data['userHasRole'] == "true" || $data['userHasRole'] == 1) ? 1 : 0;

        if ($userHasRole)
            $r = $role_user_model->replace((object)[
                'role_id' => $role_id,
                'user_id' => $user_id,
//                'modified' => 'unchanged',
            ], ['table' => $this->acl_lib->role_user_table]);
        else
            $r = $this->user_model()->delete_item((object)[
                'role_id' => $role_id,
                'user_id' => $user_id,
            ], $this->acl_lib->role_user_table);
        if (@$r)
            $this->message("Recht gesetzt.", "success", "default", 500);
        else
            $this->message("Fehler.", "warning", "default", 5000);
//        $this->dump_query($this->user_model()->last_query());

        $this->show_ajax_message($this->data);
    }

//    public function __call($name, $arguments) {
//        $this->show_ajax_message();
//    }

    public function user_roles($user_id = null) {
        $dataIn = $this->input->get_post('data');
        $user_id = $user_id ?? $dataIn['user_id'];
        $this->load->model('user/user_model');
        $this->model = $this->user_model();

        if (!empty($user_id)) {
            $this->data = [];
            $this->data['box_index']['title'] = false;
            $this->data['box_index']['hide_actions'] = true;
//            $modal_content = $this->get_view("cms/list_view", $this->data);#custom view
            /** @var Bs4 $bs4 */
            $bs4 = ($this->load->library('bs4')) ? $this->bs4 : null;
            $view_data = [
                'modal_id' => uniqid('modal_'),
            ];

            $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
                'datatable_id' => uniqid('dt_user_roles_'),
                'columns' => [
                    "id" => "id",
                    "role" => ['label' => "Rolle", 'class' => "user_role"],
//                    "last_login" => "last_login",
                    "description" => "description",
                    "userHasRole" => ['label' => "Nutzer hat Rolle", 'class' => "user_has_role"],
                ],
                'ajax_source' => site_url() . "/ajax/data_json/roles/link-list/$user_id/?request_type=ajax",
                'js_file' => "js_hazel/datatables/user_roles.js",
                'config_key' => 'user_roles',
                'data_attribs' => [
                    'user_id' => $user_id
                ]
            ];

            $modal_content = $bs4->datatable($view_data['dt_data']);#custom view

//		$output[] = $this->bootstrap_lib->edit_box($item, $this->data, $this->location_model, $params);
            $this->data['box_modal']['modal']['footer']['view'] = "empty_view";
            $this->data['box_modal']['modal_title'] = $this->model->item_label . " auswählen";
            $this->data['box_modal']['modal_content'] = $modal_content;
            $this->data['box_modal']['modal']['size'] = "modal-lg";
        } else {
            $this->message("User-ID  fehlt!");
        }

        $this->show_modal();
    }

    public function role_users($role_id = null) {
        $dataIn = $this->input->get_post('data');
        $role_id = $role_id ?? $dataIn['role_id'];
        $this->load->model('user/user_model');
        $this->model = $this->user_model();

        if (!empty($role_id)) {
            $this->data = [];
            $this->data['box_index']['title'] = false;
            $this->data['box_index']['hide_actions'] = true;
//            $modal_content = $this->get_view("cms/list_view", $this->data);#custom view
            /** @var Bs4 $bs4 */
            $bs4 = ($this->load->library('bs4')) ? $this->bs4 : null;
            $view_data = [
                'modal_id' => uniqid('modal_'),
            ];

            $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
                'datatable_id' => uniqid('dt_user_roles_'),
                'columns' => [
                    "id" => "id",
                    "displayname" => "displayname",
                    "samAccountName" => "samAccountName",
                    "mailnickname" => "mailnickname",
//                    "mail" => "mail",
//                    "roles" => ['label' => "Rollen", 'class' => "user_roles"],
                    "last_login" => "last_login",
                    "userHasRole" => ['label' => "Nutzer hat Rolle", 'class' => "user_has_role"],
                ],
                'ajax_source' => site_url() . "ajax/data_json/user/link-list/$role_id/?request_type=ajax",
                'js_file' => "js_hazel/datatables/user.js",
                'config_key' => 'user',
                'data_attribs' => [
                    'role_id' => $role_id
                ]
            ];

            $modal_content = $bs4->datatable($view_data['dt_data']);#custom view

//		$output[] = $this->bootstrap_lib->edit_box($item, $this->data, $this->location_model, $params);
            $this->data['box_modal']['modal']['footer']['view'] = "empty_view";
            $this->data['box_modal']['modal_title'] = $this->model->item_label . " auswählen";
            $this->data['box_modal']['modal_content'] = $modal_content;
            $this->data['box_modal']['modal']['size'] = "modal-lg";
        } else {
            $this->message("Role-ID  fehlt!");
        }

        $this->show_modal();
    }
}
