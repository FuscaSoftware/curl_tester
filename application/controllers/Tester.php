<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 12:26
 */

class Tester extends MY_Controller
{
    public $default_helpers = ['form', 'html', 'url', 'sb_helper', 'sb/gui'];
    public $helpers = ['cookie'];
    public $libraries = ['encryption_lib'];
    public $auth_ldap_status = false;
    /** @var array */
    public $cookie_data;
    private $_encryption_key = "122345678990dfghjkl";

    public function index($space_id = null) {
        if (is_null($space_id))
            redirect(site_url('1'));
        $view_data = [];
        $this->data = [];

        $view_data['data'] = $this->data;
        $view_data['page_title'] = "Curl & API Tester";
        $view_data['main_wrapper_class'] = "mini-sidebar";
        $view_data['space_id'] = $space_id;

//        $this->load->library('encryption');
//        $this->load->library('encryption_lib');
//        $this->load->library('Encryption_lib');
//        $this->cookie_data = json_decode(
//            $this->encryption_lib->decrypt(
//                $this->input->cookie($this->config->item('cookie_prefix') . 'curl_tester_enc') ?? null,
//                $this->_encryption_key
//            ), true
//        );
        $this->cookie_data = $this->_get_cookie_data();

//        $this->dump($this->cookie_data, 'cookie_data');
//        $this->dump($_COOKIE);
        if (empty($this->cookie_data)) {
            $this->cookie_data['url'] = 'https://api.spar-mit.com/api';
        }

        $output[] = ci()->get_view('curl_tester/index_view', $view_data);
//        $this->dump($this->cookie_data);
//        $this->dump($this->input->cookie('curl_tester'));
        $this->show_page($output, ['view_data' => $view_data]);
    }

    public function process() {
        $this->data = [];

        $input_data = $this->input->post_get("data");

        $this->cookie_data = $this->_get_cookie_data();
//        $this->cookie_data = [];
        $i = $input_data['space_id'] ?? 1;
        $this->cookie_data[$i]['url'] = $input_data['url'];
        $this->cookie_data[$i]['hta_user'] = $input_data['hta_user'];
        $this->cookie_data[$i]['hta_pw'] = $input_data['hta_pw'];
        $this->cookie_data[$i]['get_params'] = $input_data['get_params'];
        $this->cookie_data[$i]['post_params'] = $input_data['post_params'];
//        $this->cookie_data[$i]['post_params'] = $input_data['hta_pw'];
        $this->cookie_data[$i]['timeout'] = $input_data['timeout'];
        $this->input->set_cookie('curl_tester_enc',
            $this->encryption_lib->encrypt(json_encode($this->cookie_data), $this->_encryption_key)
            , 3600 * 24 * 30);
//        set_cookie('curl_tester', json_encode($this->cookie_data),3600*24*30);


        $request = $input_data['request'] ?? false;
        $get_input = $input_data['get_params'] ?? false;
        $post_input = $input_data['post_params'] ?? false;
        $url = $input_data['url'] ?? false;
//        $this->dump($input_data);
        /*
        $get1 = explode("\n", $get_input);
        $post1 = explode("\n", $post_input);

        $get2 = explode("=", $get1);
        $post2 = explode("=", $post1);

        # split / handle exclamation marks
        $get3;
        $post3;
        */
        $this->loader()->library('curl_lib');

        $params = [];
        if (trim($input_data['hta_user']))
            $params = $this->curl_lib()->parameter_array_htaccess($input_data['hta_user'], $input_data['hta_pw'] ?? '', $params);
        if (trim($input_data['access_token']))
            $params = $this->curl_lib()->parameter_array_authorization_token_header($input_data['access_token'], null, $params);

        $get_params = [];
        $post_params = [];
        $fn_split_params = function (string $string) {
            $array = [];
            $keyvalues = explode('&', $string);
            foreach ($keyvalues as $keyvalue) {
                $c = explode('=', $keyvalue);
                $array[$c[0]] = $c[1];
            }
            return $array;
        };
        $get_params = (trim($input_data['get_params'])) ? $fn_split_params($input_data['get_params']) : [];
        $post_params = (trim($input_data['post_params'])) ? $fn_split_params($input_data['post_params']) : [];

//        $url_segments = [];
//        if (!empty($url_segments))
//            $this->curl_lib()->get_api_data($url_segments);
//        else
        if ($url ?? false)
            $curl_response = $this->curl_lib()->get_data($url, $params, $get_params, $post_params);

        if ($curl_response ?? false) {
            $json_check = json_encode(json_decode($curl_response->plain()));
            $content = $curl_response->plain();
//            $content = json_encode(['test'=>123]);
//            $content = "sdfdsaf";
//            $this->dom_lib()->html("div#output", "<wrap>" . $content . "</wrap>");
            if (!$json_check)
                $this->message('JSON seems to be invalid.', 'danger', 'default', 2000);
            $this->dom_lib()->html("textarea#json-input", "<wrap>" . trim($content) . "</wrap>");
//            $this->dom_lib()->html2("textarea#json-input", $content );
//            $this->dom_lib()->html("pre#json-renderer", "<wrap><script>$('#btn-json-viewer');renderJson();</script></wrap>");
//            $this->dom_lib()->html("pre#json-renderer", "<wrap><script>$('#btn-json-viewer').click();</script></wrap>");
            $this->dom_lib()->html2("pre#json-renderer", "<script>$('#btn-json-viewer').click();</script>");
        }

        $this->show_ajax_message();
    }

    public function db_info() {
        $view_data = [];
        $this->data = [];

        $model = ($this->loader()->model("fake/Hazelnut_fake_model")) ? $this->Hazelnut_fake_model : null;
        $qs = [];
        $qs[] = "SHOW VARIABLES LIKE 'have_query_cache';";
        $qs[] = "SHOW VARIABLES LIKE 'query_cache_type';";
        $qs[] = "SHOW VARIABLES LIKE 'query_cache_limit';";
        $qs[] = "SHOW VARIABLES LIKE 'query_cache_size';";
        $qs[] = "SHOW VARIABLES LIKE 'key_buffer_size';";
        $qs[] = "SHOW VARIABLES LIKE 'tmp_table_size';";
        $qs[] = "SHOW VARIABLES LIKE 'max_heap_table_size';";
        $qs[] = "SHOW VARIABLES LIKE 'table_open_cache';";
        $qs[] = "SHOW VARIABLES LIKE 'performance_schema';";
        $qs[] = "SHOW VARIABLES LIKE 'innodb_buffer_pool_size';";
        $qs[] = "SHOW VARIABLES LIKE 'innodb_log_file_size';";
        $qs[] = "SHOW VARIABLES LIKE 'innodb_buffer_pool_instances';";
        foreach ($qs as $q) {
            $d1 = $model->db()->query($q)->result();
//            $this->data[] = $d1;
//            $this->data[] = [$d1[0]['Variable_name'], $d1[0]['Value']];
            $this->data[] = [$d1[0]->Variable_name, $d1[0]->Value];
        }

        $view_data['data'] = $this->data;
        $output[] = ci()->get_view('hazel_server/server_index', $view_data);
        $this->show_page($output, ['view_data' => $view_data]);
    }

    public function sample_json() {
        $this->data = [
            0 => ['a', 'b', 'c'],
            1 => ['a', 'b', 'c'],
            2 => ['a', 'b', 'c'],
            3 => ['a', 'b', 'c'],
        ];
        $this->show_ajax();
    }

    public function json_viewer_demo() {
        $view_data = [];
        $output[] = ci()->get_view('json_viewer_demo', $view_data);
//        $this->dump($this->cookie_data);
//        $this->dump($this->input->cookie('curl_tester'));
        $this->show_page($output, ['view_data' => $view_data]);
    }

    private function _get_cookie_data() {
        return json_decode(
                $this->encryption_lib->decrypt(
                    $this->input->cookie($this->config->item('cookie_prefix') . 'curl_tester_enc') ?? null,
                    $this->_encryption_key
                ), true
            ) ?? [];
    }
}