<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends MY_Controller
{
    // Paths
    const PATH_TO_CONTROLLER = 'dashboard';
    const PATH_TO_VIEW = 'hazel_dash/dashboard_view';

    public $default_helpers = ['form', 'html', 'url', 'sb_helper', 'sb/gui'];
    public $libraries = ['form_validation', 'session', 'controller_helper_lib'];
    public $models = [];

    /**
     * Dashboard constructor.
     */
    public function __construct() {
        parent::__construct();
        //@todo: remove this
//        ci()->session->set_userdata('logged_in', ['roles' => ['superuser' => true]]);
    }

    public function init() {
        parent::init();
    }

    public function index() {
//        var_dump($_SERVER);
//        var_dump($this->request_type);
//        var_dump($this->auth_ldap_status);
//die;


        if ($this->router->uri->uri_string == 'dashboard') {
            redirect(site_url());
        }

        if (isset($_SESSION['logged_in']['mailnickname'])) {
//            $producer = ci()->producer_model()->get_item_by_field("username", $_SESSION['logged_in']['mailnickname']);
//            if ($producer) {
//                $view_data['picture'] = ci()->producer_model()->get_pictures_by_producer($producer->id);
//                $view_data['producer'] = $producer;
//            }
        }

        $view_data['site_title'] = "Hazelnut - Datenbank - Spar mit! Reisen";
        $output[] = ci()->get_view(self::PATH_TO_VIEW, $view_data);
        $this->show_page($output);
    }

    public function test() {
        print_r($this->curl_lib()->get_data("http://localhost/hazelnut/"));
    }

}
