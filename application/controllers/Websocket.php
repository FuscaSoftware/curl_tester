<?php
/**
 * User: sbraun
 * Date: 05.07.18
 * Time: 16:59
 */

class Websocket extends MY_Controller
{
    public $default_helpers = [];
    public $auth_ldap_status = false;

    public function index() {
        if (!is_cli()) {
            echo "<pre>";
            echo "start by:";
            echo "\n$ php index.php websocket";
            echo "</pre>";
        } else {
            $this->_start_server();
        }
    }

    private function _start_server() {
        echo "Staring server.\n";
        if (is_cli())
            require APPPATH . 'third_party/RatchetServer/bin/chat-server.php';
    }
}