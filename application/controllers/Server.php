<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 12:26
 */

class Server extends MY_Controller
{
    public $default_helpers = ['form', 'html', 'url', 'sb_helper', 'sb/gui'];

    public function index() {
        $view_data = [];
        $this->data = [];

        $model = ($this->loader()->model("fake/Hazelnut_fake_model")) ? $this->Hazelnut_fake_model : null;
        $qs = [];
        $qs[] = "SHOW VARIABLES LIKE 'have_query_cache';";
        $qs[] = "SHOW VARIABLES LIKE 'query_cache_type';";
        $qs[] = "SHOW VARIABLES LIKE 'query_cache_limit';";
        $qs[] = "SHOW VARIABLES LIKE 'query_cache_size';";
        $qs[] = "SHOW VARIABLES LIKE 'key_buffer_size';";
        $qs[] = "SHOW VARIABLES LIKE 'tmp_table_size';";
        $qs[] = "SHOW VARIABLES LIKE 'max_heap_table_size';";
        $qs[] = "SHOW VARIABLES LIKE 'table_open_cache';";
        $qs[] = "SHOW VARIABLES LIKE 'performance_schema';";
        $qs[] = "SHOW VARIABLES LIKE 'innodb_buffer_pool_size';";
        $qs[] = "SHOW VARIABLES LIKE 'innodb_log_file_size';";
        $qs[] = "SHOW VARIABLES LIKE 'innodb_buffer_pool_instances';";
        foreach ($qs as $q) {
            $d1 = $model->db()->query($q)->result();
//            $this->data[] = $d1;
//            $this->data[] = [$d1[0]['Variable_name'], $d1[0]['Value']];
            $this->data[] = [$d1[0]->Variable_name, $d1[0]->Value];
        }

        $view_data['data'] = $this->data;
        $output[] = ci()->get_view('hazel_server/server_index', $view_data);
        $this->show_page($output, ['view_data' => $view_data]);
    }
}