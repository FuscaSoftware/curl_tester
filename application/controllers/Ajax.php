<?php

class Ajax extends MY_Controller
{


//    public $auth_ldap_status = false;

    public $default_helpers = ['form', 'html', 'url'];
    public $libraries = ['form_validation'];
    public $no_overhead = true;


    public function __construct() {
        parent::__construct();
    }

    public function init() {
        parent::init();
    }

    public function data_json(string $object_type, string $action = null, $param1 = null) {

        switch ($object_type) {
            case "user":
            case "User":
                $this->user($action);
                break;
            case "roles":
                $this->roles($action, $param1);
                break;
            case "pack":
                $this->pack($action, $param1);
                break;
            default:
//                ci()->message("Ajax does not support this type! $object_type");
        }


        $this->data['cache'] = 1;#1min (+1h)
        $this->show_ajax_message();
    }


    private function user(string $action) {
        /** @var User_model $user_model */
        $user_model = ($this->load->model('user/user_model')) ? $this->user_model : null;

        $dataIn = $this->input->get_post('data');
        $columns = $this->input->get_post('options')['columns'];

        $order_by = [
            'field' => $columns[$dataIn['order'][0]['column']],
            'direction' => $dataIn['order'][0]['dir']
        ];

        switch ($action) {
            case "rights":
                $filter = [
                    'acl_user' => [
                        [
                            'condition' => (!empty($dataIn['search']['value'])) ? ['acl_user.id' => $dataIn['search']['value']] : [1 => 1],
                        ],
                        ['or_like2' => ['displayname' => $dataIn['search']['value']]],
                        ['or_like2' => ['samAccountName' => $dataIn['search']['value']]],
//                'id' => ['condition' => $dataIn['search']['value']],
//                'name' => ['or' => $dataIn['search']['value']],
//                'id' => ['or_condition' => [$dataIn['search']['value']]],
//                'order_by' => ['field' => 'id', 'direction' => 'desc'],
                        'order_by' => $order_by,
                    ],
                ];
                $db = $user_model->db()
                    ->distinct()
                    ->select('concat(l.controller_id, "|", l.method_id, "|", l.role_id) as ID')
                    ->select('concat(c.`controller`, " (", l.controller_id, ")") as controller')
                    ->select('concat(m.`method`, " (", l.method_id, ")") as method ')
                    ->select('concat(r.`role`, " (", l.role_id, ")") as role')
                    ->select('l.allow')
                    ->select('l.modified')
                    ->from('acl_law as l')
                    ->join('acl_controller c', 'c.id = l.controller_id')
                    ->join('acl_method m', 'm.id = l.method_id')
                    ->join('acl_role r', 'r.id = l.role_id')
//                    ->select('acl_user.*')
//                    ->select("cc.*")
//                    ->select("acl_user.id as 'acl_user.id'")
//                    ->select("($last_login_q) as last_login")//            ->group_by('customer.id')
//                    ->select("($roles_q) as roles")//            ->group_by('customer.id')
                ;
                break;
            case "list":
            default:
                $filter = [
                    'acl_user' => [
                        [
                            'condition' => (!empty($dataIn['search']['value'])) ? ['acl_user.id' => $dataIn['search']['value']] : [1 => 1],
                        ],
                        ['or_like2' => ['displayname' => $dataIn['search']['value']]],
                        ['or_like2' => ['samAccountName' => $dataIn['search']['value']]],
//                'id' => ['condition' => $dataIn['search']['value']],
//                'name' => ['or' => $dataIn['search']['value']],
//                'id' => ['or_condition' => [$dataIn['search']['value']]],
//                'order_by' => ['field' => 'id', 'direction' => 'desc'],
                        'order_by' => $order_by,
                    ],
                ];
                $last_login_q = "select ul.created as last_login from acl_user_login_log ul where acl_user.samAccountName = ul.user_name order by last_login desc limit 1";
                $roles_q = "select group_concat(`description` separator ',') as roles from acl_role as r, acl_role_user ru, acl_user u
                            where r.id = ru.role_id AND ru.user_id = u.id AND u.`id` = acl_user.id";
                $db = $user_model->db()
                    ->select('acl_user.*')
//            ->select("cc.*")
//            ->select("acl_user.id as 'acl_user.id'")
                    ->select("($roles_q) as roles")//            ->group_by('customer.id')
                    ->select("($last_login_q) as last_login")//            ->group_by('customer.id')
                ;
        }

        $collection = $user_model->get_items3($filter);
        $this->data = [
            'recordsTotal' => $collection->count(),
            'recordsFiltered' => $collection->count(),
            'data' => $user_model->mh()->get_keyless_items(
//                $this->switches(
                $collection->get_array($dataIn['length'], $dataIn['start'])
//                )
                ,
                $columns
            ),
//            'test' => $collection->get_array($dataIn['length'], $dataIn['start']),
        ];
//        $this->dump(
//            $collection->get_array(1, 0)
//        );
//        $this->dump_query($columns);
//        $this->dump_query($user_model->last_query());
    }

    public function roles(string $action, $param1 = null) {
        /** @var User_model $user_model */
        $user_model = ($this->load->model('user/user_model')) ? $this->user_model : null;

        $dataIn = $this->input->get_post('data');
        $columns = $this->input->get_post('options')['columns'];

        $order_by = [
            'field' => $columns[$dataIn['order'][0]['column']],
            'direction' => $dataIn['order'][0]['dir']
        ];

        switch ($action) {
            case "link-list":
                $filter = [
                    'acl_user' => [
                        [
                            'condition' => (!empty($dataIn['search']['value'])) ? ['acl_role.role' => $dataIn['search']['value']] : [1 => 1],
                        ],
//                        ['or_like2' => ['displayname' => $dataIn['search']['value']]],
//                        ['or_like2' => ['samAccountName' => $dataIn['search']['value']]],
                        'order_by' => $order_by,
                    ],
                ];
                $cond1 = "acl_role_user ru ON acl_role.id = acl_role_user.role_id AND acl_role_user.user_id = '$param1'";
                $db = $user_model->db()
                    ->select('acl_role.*')
                    ->select('acl_role_user.user_id as userHasRole')
                    ->join("acl_role_user", $cond1, 'LEFT')
                ;
                break;
            case "list":
            default:
                $filter = [
                    'acl_user' => [
                        [
                            'condition' => (!empty($dataIn['search']['value'])) ? ['acl_role.role' => $dataIn['search']['value']] : [1 => 1],
                        ],
//                        ['or_like2' => ['displayname' => $dataIn['search']['value']]],
//                        ['or_like2' => ['samAccountName' => $dataIn['search']['value']]],
                        'order_by' => $order_by,
                    ],
                ];
                $user_q = "select count(*) from acl_role_user where acl_role.id = acl_role_user.role_id";

//                $cond1 = "acl_role_user ru ON acl_role.id = acl_role_user.role_id AND acl_role_user.user_id = $param1";
                $db = $user_model->db()
                    ->select('acl_role.*')
                    ->select("($user_q) as users")//            ->group_by('customer.id')
//                    ->select('acl_role_user.user_id as userHasRole')
//                    ->join("acl_role_user", $cond1, 'LEFT')
                ;
        }

        $collection = $user_model->get_items3($filter, null, ['table' => $this->acl_lib->role_table]);
        $this->data = [
            'recordsTotal' => $collection->count(),
            'recordsFiltered' => $collection->count(),
            'data' => $user_model->mh()->get_keyless_items(
//                $this->switches(
                $collection->get_array($dataIn['length'], $dataIn['start'])
//                )
                ,
                $columns
            ),
        ];
    }

    function pack(string $action, $param1= null) {
        /** @var Pack_model $pack_model */
        $pack_model = ($this->load->model('hazel/pack_model')) ? $this->pack_model : null;

        $dataIn = $this->input->get_post('data');
        $columns = $this->input->get_post('options')['columns'];

        $order_by = [
            'field' => $columns[$dataIn['order'][0]['column']],
            'direction' => $dataIn['order'][0]['dir']
        ];

        switch ($action) {
            case "link-list":
//                $filter = [
//                    'acl_user' => [
//                        [
//                            'condition' => (!empty($dataIn['search']['value'])) ? ['acl_role.role' => $dataIn['search']['value']] : [1 => 1],
//                        ],
////                        ['or_like2' => ['displayname' => $dataIn['search']['value']]],
////                        ['or_like2' => ['samAccountName' => $dataIn['search']['value']]],
//                        'order_by' => $order_by,
//                    ],
//                ];
//                $cond1 = "acl_role_user ru ON acl_role.id = acl_role_user.role_id AND acl_role_user.user_id = '$param1'";
//                $db = $pack_model->db()
//                    ->select('acl_role.*')
//                    ->select('acl_role_user.user_id as userHasRole')
//                    ->join("acl_role_user", $cond1, 'LEFT')
//                ;
                break;
            case "list":
            default:
                $filter = [
                    'paket' => [
                        [
//                            'condition' => (!empty($dataIn['search']['value'])) ? ['acl_role.role' => $dataIn['search']['value']] : [1 => 1],
                        ],
//                        ['or_like2' => ['displayname' => $dataIn['search']['value']]],
//                        ['or_like2' => ['samAccountName' => $dataIn['search']['value']]],
                        'order_by' => $order_by,
                    ],
                ];

                $db = $pack_model->db()
                    ->select('paket.id')
                    ->select('paket.package_number')
                    ->select('paket.package_title')
                    ->select('paket.brand')
                    ->select('paket.default_days')
                    ->select('paket.active')
                ;
        }

        $collection = $pack_model->get_items3($filter, null, ['table' => $pack_model->table]);

//        $this->dump($pack_model->last_query());

        $this->data = [
            'recordsTotal' => $collection->count(),
            'recordsFiltered' => $collection->count(),
            'data' => $pack_model->mh()->get_keyless_items(
//                $this->switches(
                $collection->get_array($dataIn['length'], $dataIn['start'])
//                )
                ,
                $columns
            ),
        ];
    }


    private function switches($array) {
        foreach ($array as $item) {
            $item->allow = $this->get_view("components/toggle_switch", ['checked' => ($item->allow)]);
        }
        return $array;
    }
}
