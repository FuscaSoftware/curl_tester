<?php

class Login extends MY_Controller
{
    public $show_menu = false;

    // Paths
    const PATH_TO_DASHBOARD_CONTROLLER = 'dashboard';
    const PATH_TO_LOGIN_CONTROLLER = 'login';
    const PATH_TO_LOGIN_VIEW = 'matrix/login/login_view';

    // Header and Footer // TODO einpacken
    const PATH_TO_HEADER = 'common/header';
    const PATH_TO_FOOTER = 'common/footer';

    // Wording
    const BENUTZERNAME = 'Benutzername';
    const KENNWORT = 'Kennwort';
    const USER_NAME = 'user_name';
    const USER_PASSWORD = 'user_password';

//    public $auth_ldap_status = false;

    public $default_helpers = ['form', 'html', 'url'];
    public $libraries = ['form_validation'];

    /**
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function init() {
        parent::init();
    }

    /**
     * @param null $request_type
     * @param null $requester
     */
    public function index($request_type = null, $requester = null)
    {
        $this->bootstrap_lib();
        $data = (is_array(@$this->data)) ? $this->data : [];
        if ($this->session && $this->session->userdata('logged_in')) {
            $this->_redirect_after_login();
        } else {
            if ($request_type == "ajax") {
                ci()->message("Sie müssen sich neu einloggen! <a target=\"_blank\" href=\"" . site_url("login") . "\">Login</a>",
                    "danger", 'default', 6000, "Session abgelaufen: ");
                ci()->show_ajax_message();
                // TODO close window after login!
            } else {
                $data["requester"] = $requester;
//                $this->load_templates(self::PATH_TO_LOGIN_VIEW, $data);
//                echo file_get_contents(FCPATH . "templates/matrix-admin-bt4-1/html/ltr/authentication-login.html");
                $this->load->view('matrix/login/login_view', $data);
            }
        }
    }

    /**
     *
     */
    private function set_rules()
    {
        // 1.input feld
        $this->form_validation->set_rules("login[" . self::USER_NAME . "]", '', 'trim|required|min_length[3]');
        // 2.input feld
        $this->form_validation->set_rules("login[" . self::USER_PASSWORD . "]", '', 'trim|required|min_length[3]');
    }

    /**
     *
     */
    public function check_login_data()
    {
        $this->set_rules();
        $this->data = [];
        if ($this->form_validation->run()) {
            $login = $this->input->post('login');
            $data = [
                self::USER_NAME => $login[self::USER_NAME],
                self::USER_PASSWORD => $login[self::USER_PASSWORD],
            ];
            if ($this->auth_ldap->authenticate_user($data)) {
                unset($data[self::USER_PASSWORD]);
                $this->_redirect_after_login($login['requester']);
            } else {
                /* used for?! : */
                $this->data['message'] = 'Invalid username and/or password';
                $this->data['bootstrap_class'] = 'alert alert-warning';
                /* : used for?! */
                $this->message("Invalid username and/or password", 'warning');
            }
        } else {
//            die('99');
            $this->message("Form-Validation failed!", 'danger');
        }
//        $this->load_templates(self::PATH_TO_LOGIN_VIEW, $data);
//        $this->index();
        redirect('login');
        die;
    }

    /**
     *
     */
    public function logout()
    {
        $this->session->sess_destroy();
//        $this->load_templates(self::PATH_TO_LOGIN_VIEW);
        redirect('login');
    }

    public function _redirect_after_login($requester = null)
    {
        $requester = ($requester)?: $this->input->post_get("requester");
        $requester = ($requester)?: @$this->input->post_get("login")['requester'];
        if ($requester) {
            $requested_url = str_replace("||", "/", urldecode($requester));
        }
        if (isset($requested_url) && !empty($requested_url)) {
            redirect(site_url($requested_url));
        } else {
            redirect(self::PATH_TO_DASHBOARD_CONTROLLER);
        }
    }

    /**
     * @param string $path_to_view
     * @param array $data
     */
    private function load_templates(
        $path_to_view,
        $data = array()
    ) {
//        $this->load->view(self::PATH_TO_HEADER);
        $this->load->view($path_to_view, $data);
//        $this->load->view(self::PATH_TO_FOOTER);
    }

}
