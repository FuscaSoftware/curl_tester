<?php
/**
 * User: sbraun
 * Date: 24.08.17
 * Time: 10:21
 */

require APPPATH . "/third_party/mustache.php/vendor/autoload.php";

//require dirname(__FILE__)."/mustache/Viewdata.php";
class Mustache_lib #extends Super_lib
{
    public $options = [];
    /* sample
            $options =
            [
//        'partials_loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/templates/partials'),
                'partials' => [
                    'head' => file_get_contents("templates/partials/head.mustache"),
                    'table' => file_get_contents("templates/partials/table.mustache"),
                ]
            ];
    */

    private static $_instance = null;
    private static $_engine = null;
    private $_template_path = "mustache/";

    public function __construct() {
        $options = [
//            'partials_loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/templates/partials'),
            'partials_loader' => new Mustache_Loader_FilesystemLoader(APPPATH . '/views/mustache/partials'),
//            'partials' => [
//                'head' => file_get_contents("templates/partials/head.mustache"),
//                'table' => file_get_contents("templates/partials/table.mustache"),
//            ]
        ];
        $this->set_options($options);
    }

    /**
     * @return Mustache_Engine
     */
    function &get_engine(): Mustache_Engine {
        if (!isset($mustache)) {
            static $mustache;
            $mustache = new Mustache_Engine(
                $this->options
            );
        }
        return $mustache;
    }

    /**
     * @param string $template_file
     * @param array  $view_data view data array overhanded to ->load->view() - avoid to use this!
     *
     * @return string
     */
    function _load_template($template_file, $view_data = []): string {
        $path = (preg_match('/^mustache\//i', $template_file)) ? '' : $this->_template_path;
        $file_ending = (preg_match('/\.mustache$/i', $template_file)) ? '' : '.mustache';
        $template = ci()->load->view("{$path}{$template_file}{$file_ending}", $view_data, true);
        return $template;
//        if (!stristr($template_file, ".mustache") && !stristr($template_file, ".html"))
//            $template_file = $template_file . ".mustache";
//        $template = file_get_contents($this->_template_path . $template_file);
//        return $template;
    }


    /**
     * @param string       $template
     * @param object|array $view_data
     *
     * @return mixed|string
     */
    function _render(string $template, $view_data): string {
        if (is_null($view_data)) {
            die('$view_data ' . "must be set!");
        } else {
            $html = $this->mustache()->render($template, $view_data);
            return $html;
        }
    }

    function set_options(array $options) {
        $this->options = array_merge($this->options, $options);
    }

    function set_template_path($path) {
        $this->_template_path = $path;
    }

    /**
     * Alias for get_engine
     * @see get_engine()
     * @return Mustache_Engine
     */
    function &mustache(): Mustache_Engine {
        return self::get_engine();
    }
}
