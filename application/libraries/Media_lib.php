<?php
#ci()->load->library("Super_lib");

/**
 * User: sebra
 * Date: 16.09.16
 * Time: 11:29
 */
class Media_lib #extends Super_lib
{
    /**
     * @param array|object $media
     * @param string       $width
     * @param bool         $no_crop [default=false]
     *
     * @return mixed
     */
    public static function url($media, $width = "1024", $no_crop = false) {
        if (is_array($media))
            $media = (object)$media;
        if (is_object($media)) {
            if (!isset($media->url)) {
                $media->url = self::_url($media, $no_crop);
            } elseif (empty($media->url)) {
                # "";
            }
            if (substr($media->url, 0, 2) == '//' && HOST_MEDIA !== 'localhost')
                $media->url = 'https:' . $media->url;
            if (stristr(@$media->url, "/media/dms/") && stristr(@$media->url, "{size}")) {
                if (!$no_crop && !empty($media->crop))
                    $width = $width . "_" . $media->crop;
                return str_replace("{size}", "w" . $width, $media->url);
            }
        }

        $height = $width * 3 / 4;
        return (isset($media->url) && strstr($media->url, "//")) ? $media->url : "https://via.placeholder.com/{$width}x{$height}";
    }

    /**
     * @param array|object $media
     * @param int          $width
     *
     * @return int
     */
    public static function get_ratio_height($media, $width = 1024) {
        if (is_array($media))
            $media = (object)$media;
        if (empty($media->height) || empty($media->width)) {
            $imgsize = getimagesize(@$media->url);
            if ($imgsize)
                return $imgsize[0] / $imgsize[1];
            else
                return 0;
        }
        $ratio = $width / $media->width;
        return round($media->height * $ratio, 0);
    }

    private static function _url($item, $no_crop = false) {
        if (!is_object($item)) {
            return false;
        }
        if (@$item->__MODEL == "old_picture") {
            $url = @$item->url;
            # if we have cookie free domain:
//            $url = str_replace(['http://', "www.spar-mit.com"], ['https://', HOST_MEDIA], $url);
            $url = str_replace(['http://'], ['https://'], $url);
        } elseif ((@$item->__MODEL == "picture" || @!$item->__MODEL) && !empty($item->path) && !empty($item->filename)) {
//            $url = "http://dms.sparmit.local/image_sources/$item->path/$item->filename";
            $url = "//" . HOST_MEDIA . "/media/dms/" . "$item->path/{size}/" . rawurlencode($item->filename);
            $item->url = $url;
//            ci()->dump(["get_url1()", $url, $item]);
        } else { #workarround / fail-case
//            echo "<pre>";
//            var_dump(array("__MODEL / url is missing for item:" => $item));
//            echo "</pre>";
            $url = "https://via.placeholder.com/150&text=Fehler:+get_url1()";
//            ci()->dump(["__MODEL / url is missing for item:" => $item]);
            if ($item->__MODEL == 'picture' && (empty($item->path) || empty($item->filename)))
                ci()->dump(["filename/path is missing for item:" => $item]);
            error_log(var_export(["__MODEL / url is missing for item:" => $item], 1));
        }
        if (isset($url))
            return $url;
        else {
            error_log("Url of picture could not be resolved: " . var_export($item, 1));
            return null;
        }
    }
}