<?php
/**
 * User: sbraun
 * Date: 02.03.18
 * Time: 10:33
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Session extends CI_Session
{

    public function __construct(array $params = array()) {
//        var_dump($_ENV);
//        var_dump($_SERVER);
//        die;
        if ($this->ignore_sessions())
            return;
        parent::__construct();
    }

    public function ignore_sessions() {
        if (1) {# ignore Sessions for curl-clients (if user-agent is not changed)
            if (is_cli())
                return true;
            if (stristr(@$_SERVER["HTTP_USER_AGENT"], 'curl'))
                return true;
        }
        if (0) {
//        $uri = str_replace("//", "/", $_SERVER['REQUEST_URI']);
//        if (strpos($uri, '/ignore_this_controller/') === 0)
//            return true;
        }
        return false;
    }

    protected function _sess_update($force = false) {
        if (0) {
            // Do NOT update an existing session on AJAX calls.
//        if ($force || !ci()->input->is_ajax_request())
        }
        return parent::_sess_update($force);
    }

}