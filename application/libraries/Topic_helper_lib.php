<?php

MY_Controller::get_instance()->load->library("super_lib");

/**
 * Description of Topic_helper_lib
 *
 * @author Marco Heine
 */
class Topic_helper_lib extends Super_lib
{

    public function add_hoteltopic($hotel_id, $country) {

        // check if Topic exists
        $topic_exists_query = "select topic.id
from topic
inner join dogtag_knots as dt on dt.object_type = 'topic' and dt.object_id = topic.id
inner join dogtag on dogtag.id = dt.dogtag_id
inner join dogtag_knots as dh on dogtag.id = dh.dogtag_id and dogtag.class = 'hotel'
inner join hotel on hotel.id = dh.object_id and dh.object_type = 'hotel' and hotel.id = $hotel_id;";

        $topic_exists = ci()->hotel_model()->db()->query($topic_exists_query)->result_object();

        if (!$topic_exists) {
            // Topic anlegen wenn Sie noch nicht existiert
            $hotel = ci()->hotel_model()->get_item_by_id($hotel_id);
            $subclass = "'region'";

            $location_topic_query = "select topic.url, topic.id as topic_id, dogtag.id as dogtag_id, title
from topic
inner join dogtag_knots as dt on dt.object_id = topic.id and dt.object_type = 'topic' and topic.class = 'reiseziele' and dt.page_linked = 1
inner join dogtag on dt.dogtag_id = dogtag.id 
inner join dogtag_knots as dh on dh.dogtag_id = dogtag.id and dh.object_type = 'hotel'
inner join hotel on hotel.id = dh.object_id and dogtag.class = 'location' and hotel.id = $hotel_id and dogtag.subclass = ";
            $region_topic = ci()->hotel_model()->db()->query($location_topic_query . $subclass)->custom_row_object(0, "Item");

            $subclass = "'city'";
            $city_topic = ci()->hotel_model()->db()->query($location_topic_query . $subclass)->custom_row_object(0, "Item");


            if ($city_topic) {
                $parent_id = $city_topic->topic_id;
            } else {
                $parent_id = $region_topic->topic_id;
            }

            $hotel_ort = mb_strtolower($hotel->address);
            $url = url_title($hotel->name, 'dash', TRUE);
            if (strpos($url, $hotel_ort) == false || strpos($url, url_title($hotel_ort)) == false) {
                $url .= '-' . url_title($hotel_ort);
            }

            $item['topic'] = array();
            $item['topic'][0] = ['id' => 0, 'name' => $hotel->name, 'title' => $hotel->name, 'class' => 'hotels', 'url' => $url, 'parent_id' => $parent_id, 'navigation_hidden' => 1];
            $r = ci()->topic_model()->save_items($item);
            $topic_id = $r[0]['id'];
            $dogtag_return = $this->add_hoteldogtags_to_hotels(($hotel_id));

            // add Producer to Topic
            $producers = ci()->producer_model()->get_items_by_knot($hotel_id, "hotel", "producer");
            if ($producers) {
                $producer = $producers[0];
            }
            ci()->knot_model()->insert_knot(['producer' => $producer->id, 'topic' => $topic_id]);

            ci()->dogtag_knot_model()->insert_knot(['dogtag' => $dogtag_return, 'topic' => $topic_id], ['weight' => 1.000, 'required' => 1, 'page_linked' => 1]);

            // add Location for hotel
            $geo = file_get_contents(site_url('public/geo/get/' . rawurlencode($hotel->name)));
            $geo = explode(";", $geo);
            $data['longitude'] = $geo[0];
            $data['latitude'] = $geo[1];
            $data['name'] = rawurlencode($country) . ' / ' . $region_topic->title . ' / ' . $city_topic->title . ' / ' . $hotel->name;
            $data['title'] = $hotel->name;
            $data['type'] = 'hotel';
            $data['foreign_keys'] = ['hotel' => $hotel_id];
            $save_data = array('location' => array($hotel_id => ($data)));
            ci()->location_model()->save($save_data);

            ci()->publisher_model()->publish_topic($topic_id);
            return $r;
        } else {
            echo "Topic existiert schon!";
        }
    }

    public function add_citytopic($hotel_id, $country, $region) {
        // check if Topic exists
        $topic_exists_query = "select topic.id
from topic
inner join dogtag_knots as dt on dt.object_type = 'topic' and dt.object_id = topic.id
inner join dogtag on dogtag.id = dt.dogtag_id
inner join dogtag_knots as dh on dogtag.id = dh.dogtag_id and dogtag.class = 'Location' and dogtag.subclass = 'city'
inner join hotel on hotel.id = dh.object_id and dh.object_type = 'hotel' and hotel.id = $hotel_id;";

        $topic_exists = ci()->hotel_model()->db()->query($topic_exists_query)->result_object();

        if (!$topic_exists) {
            // Topic anlegen wenn Sie noch nicht existiert
            $hotel = ci()->hotel_model()->get_item_by_id($hotel_id);
            $subclass = "'region'";

            $location_topic_query = "select topic.url, topic.id as topic_id, dogtag.id as dogtag_id
from topic
inner join dogtag_knots as dt on dt.object_id = topic.id and dt.object_type = 'topic' and topic.class = 'reiseziele' and dt.page_linked = 1
inner join dogtag on dt.dogtag_id = dogtag.id 
inner join dogtag_knots as dh on dh.dogtag_id = dogtag.id and dh.object_type = 'hotel'
inner join hotel on hotel.id = dh.object_id and dogtag.class = 'location' and hotel.id = $hotel_id and dogtag.subclass = ";
            $region_topic = ci()->hotel_model()->db()->query($location_topic_query . $subclass)->custom_row_object(0, "Item");

            if (!$region_topic) {
                $subclass = "'state'";
                $region_topic = ci()->hotel_model()->db()->query($location_topic_query . $subclass)->custom_row_object(0, "Item");
            }

            $url = $region_topic->url . '/' . url_title($hotel->address, 'dash', TRUE);
            $parent_id = $region_topic->topic_id;

            $item['topic'] = array();
            $item['topic'][0] = ['id' => 0, 'name' => $hotel->address, 'title' => $hotel->address, 'class' => 'reiseziele', 'url' => $url, 'parent_id' => $parent_id];
            $r = ci()->topic_model()->save_items($item);
            $topic_id = $r[0]['id'];
            $dogtag_return = $this->add_citydogtag_to_hotel($hotel_id, $country, $region);


            // add Producer to Topic
            $producers = ci()->producer_model()->get_items_by_knot($hotel_id, "hotel", "producer");
            if ($producers) {
                $producer = $producers[0];
            }
            ci()->knot_model()->insert_knot(['producer' => $producer->id, 'topic' => $topic_id]);

            ci()->dogtag_knot_model()->insert_knot(['dogtag' => $dogtag_return, 'topic' => $topic_id], ['weight' => 1.000, 'required' => 1, 'page_linked' => 1]);

            // add Location for dogtag
            $geo = file_get_contents(site_url('public/geo/get/' . rawurlencode($hotel->address)));
            $geo = explode(";", $geo);
            $data['longitude'] = $geo[0];
            $data['latitude'] = $geo[1];
            $data['name'] = rawurldecode($country) . ' / ' . rawurldecode($region) . ' / ' . $hotel->address;
            $data['title'] = $hotel->address;
            $data['type'] = 'city';
            $data['foreign_keys'] = ['dogtag' => $dogtag_return];
            $save_data = array('location' => array( $hotel_id => ($data)));
            ci()->location_model()->save($save_data);

            return $r;
        } else {
           return false;
        }

    }

    public function add_regiontopic($hotel_id, $country, $region) {

        // check if Topic exists
        $topic_exists_query = "select topic.id
from topic
inner join dogtag_knots as dt on dt.object_type = 'topic' and dt.object_id = topic.id
inner join dogtag on dogtag.id = dt.dogtag_id
inner join dogtag_knots as dh on dogtag.id = dh.dogtag_id and dogtag.class = 'Location' and dogtag.subclass = 'region'
inner join hotel on hotel.id = dh.object_id and dh.object_type = 'hotel' and hotel.id = $hotel_id;";

        $topic_exists = ci()->hotel_model()->db()->query($topic_exists_query)->result_object();

        if (!$topic_exists)
            $topic_exists = ci()->topic_model()->get_items_by_field('title', $region);

        if (!$topic_exists) {
            // Topic anlegen wenn Sie noch nicht existiert
            $subclass = "'country'";

            $location_topic_query = "select topic.url, topic.id as topic_id, dogtag.id as dogtag_id
from topic
inner join dogtag_knots as dt on dt.object_id = topic.id and dt.object_type = 'topic' and topic.class = 'reiseziele' and topic.parent_id = 11
inner join dogtag on dt.dogtag_id = dogtag.id 
inner join dogtag_knots as dh on dh.dogtag_id = dogtag.id and dh.object_type = 'hotel'
inner join hotel on hotel.id = dh.object_id and dogtag.class = 'location' and hotel.id = $hotel_id and dogtag.subclass = ";
            $country_topic = ci()->hotel_model()->db()->query($location_topic_query . $subclass)->custom_row_object(0, "Item");


            $url = url_title($region, 'dash', TRUE);

            $parent_id = $country_topic->topic_id;

            $item['topic'] = [];
            $item['topic'][0] = ['id' => 0, 'name' => ucwords($region), 'title' => ucwords($region), 'class' => 'reiseziele', 'url' => $url, 'parent_id' => $parent_id];
            $r = ci()->topic_model()->save_items($item);
            $topic_id = $r[0]['id'];
            $dogtag_return = $this->add_regiondogtag_to_hotel($hotel_id, $country, $region);


            // add Producer to Topic
            $producers = ci()->producer_model()->get_items_by_knot($hotel_id, "hotel", "producer");
            if ($producers) {
                $producer = $producers[0];
            }
            ci()->knot_model()->insert_knot(['producer' => $producer->id, 'topic' => $topic_id]);

            ci()->dogtag_knot_model()->insert_knot(['dogtag' => $dogtag_return, 'topic' => $topic_id], ['weight' => 1.000, 'required' => 1, 'page_linked' => 1]);

            // add Location for dogtag
            $geo = file_get_contents(site_url('public/geo/get/' . rawurlencode($region)));
            $geo = explode(";", $geo);
            $data['longitude'] = $geo[0];
            $data['latitude'] = $geo[1];
            $data['name'] = $country . ' / ' . $region;
            $data['title'] = ucwords($region);
            $data['type'] = 'region';
            $data['foreign_keys'] = ['dogtag' => $dogtag_return];
            $save_data = array('location' => array( $hotel_id => ($data)));
            ci()->location_model()->save($save_data);

            return $r;
        } else {
            return false;
        }

    }


    public function add_hoteldogtags_to_hotels($hotel_id = null) {

        if (!$hotel_id) {
            // für alle Hotels ohne Hotel-Hundemarke anlegen
            $query_hotels_without_dogtags = "select *
from hotel
where id not in (select hotel.id 
from dogtag
inner join dogtag_knots on dogtag.id = dogtag_knots.dogtag_id and class = 'hotel'
inner join hotel on hotel.id = dogtag_knots.object_id and dogtag_knots.object_type = 'hotel');";

            $hotels_without_dogtags = ci()->hotel_model()->db()->query($query_hotels_without_dogtags)->result_object();

            foreach ($hotels_without_dogtags as $hotel_without_dogtag) {
                $hotel = ci()->hotel_model()->get_item_by_id($hotel_without_dogtag->id);
                $item['dogtag'] = array();
                $item['dogtag'][0] = ['id' => 0, 'name' => $hotel->name, 'class' => "Hotel", 'global_score' => 1, 'scorable' => 1, 'subclass' => "", 'foreign_keys' => ['hotel' => $hotel->id]];
                ci()->dogtag_model()->save_items($item);
            }
            return($item);
        } else if ($hotel_id) {
            // check if Dogtag exists
            $dogtag_exists_query = "select dogtag.id, dogtag.name
from dogtag
inner join dogtag_knots on dogtag.id = dogtag_knots.dogtag_id and class = 'hotel'
inner join hotel on hotel.id = dogtag_knots.object_id and dogtag_knots.object_type = 'hotel' and hotel.id = $hotel_id";

            $dogtag_exists = ci()->hotel_model()->db()->query($dogtag_exists_query)->result_object();

            if (!$dogtag_exists) {
                // Hundemarke anlegen wenn Sie noch nicht existiert
                $hotel = ci()->hotel_model()->get_item_by_id($hotel_id);
                $item['dogtag'] = array();
                $item['dogtag'][0] = ['id' => 0, 'name' => $hotel->name, 'class' => "Hotel", 'global_score' => 1, 'scorable' => 1, 'subclass' => "", 'foreign_keys' => ['hotel' => $hotel->id]];
                $r = ci()->dogtag_model()->save_items($item);
                return($r[0]['id']);
            } else {
                return($dogtag_exists[0]->id);
            }
        }
    }

    public function add_citydogtag_to_hotel($hotel_id, $country, $region) {
        // check if Dogtag exists
        $dogtag_exists_query = "select dogtag.id, dogtag.name
from dogtag
inner join dogtag_knots on dogtag.id = dogtag_knots.dogtag_id and class = 'Location' and subclass = 'city'
inner join hotel on hotel.id = dogtag_knots.object_id and dogtag_knots.object_type = 'hotel' and hotel.id = $hotel_id";

        $dogtag_exists = ci()->hotel_model()->db()->query($dogtag_exists_query)->result_object();

        if (!$dogtag_exists) {
            // Hundemarke anlegen wenn Sie noch nicht existiert
            $hotel = ci()->hotel_model()->get_item_by_id($hotel_id);
            $city = $hotel->address;
            $name = rawurldecode($country) . '/' . rawurldecode($region) . ' / ' . $city;
            $item['dogtag'] = array();
            $item['dogtag'][0] = ['id' => 0, 'name' => $name, 'class' => "Location", 'global_score' => 1, 'scorable' => 1, 'subclass' => "city", 'foreign_keys' => ['hotel' => $hotel->id]];
            $r = ci()->dogtag_model()->save_items($item);
            return($r[0]['id']);
        } else {
            return($dogtag_exists[0]->id);
        }
    }

    public function add_regiondogtag_to_hotel($hotel_id, $country, $region) {
        // check if Dogtag exists
        $dogtag_exists_query = "select dogtag.id, dogtag.name
from dogtag
inner join dogtag_knots on dogtag.id = dogtag_knots.dogtag_id and class = 'Location' and subclass = 'region'
inner join hotel on hotel.id = dogtag_knots.object_id and dogtag_knots.object_type = 'hotel' and hotel.id = $hotel_id";

        $dogtag_exists = ci()->hotel_model()->db()->query($dogtag_exists_query)->result_object();

        if (!$dogtag_exists) {
            // Hundemarke anlegen wenn Sie noch nicht existiert
            $region = ucwords($region);
            $name = $country . ' / ' . $region;
            $item['dogtag'] = [];
            $item['dogtag'][0] = ['id' => 0, 'name' => $name, 'class' => "Location", 'global_score' => 1, 'scorable' => 1, 'subclass' => "region", 'foreign_keys' => ['hotel' => $hotel_id]];
            $r = ci()->dogtag_model()->save_items($item);
            return($r[0]['id']);
        } else {
            return($dogtag_exists[0]->id);
        }
    }

    public function add_hotelcontent_to_hoteltopic() {
        $query_hotel_topic = "SELECT topic.id as topic_id, hotel.id as hotel_id
FROM hotel
inner join dogtag_knots as hd on hd.object_id = hotel.id and hd.object_type = \"hotel\"
inner join dogtag on hd.dogtag_id = dogtag.id and class = \"Hotel\"
inner join dogtag_knots as dt on dt.dogtag_id = dogtag.id and dt.object_type = \"topic\" and dt.required = 1
inner join topic on dt.object_id = topic.id";

        $hotel_topics = ci()->hotel_model()->db()->query($query_hotel_topic)->result_object();

        foreach ($hotel_topics as $hotel_topic) {
            $topic_content = ci()->content_model()->get_items_by_knot2($hotel_topic->topic_id, 'topic')->first();
            if (!$topic_content) {
                $hotel_contents = ci()->content_model()->get_items_by_knot2($hotel_topic->hotel_id, 'hotel');
                foreach ($hotel_contents as $hotel_content) {
                    if (!ci()->knot_model()->get_items_by_fields(['object1_id' => $hotel_content->id, 'object1_type' => 'content', 'object2_id' => $hotel_topic->topic_id, 'object2_type' => 'topic']))
                        $new_knots[] = ci()->knot_model()->insert_knot(['content' => $hotel_content->id, 'topic' => $hotel_topic->topic_id]);
                }
            }
        }

        return @$new_knots;
    }

    /*
 * alle Topics mit diesem Attribut werden auf dem Desktop in der linken Navi/Mobile auf der Startseite angezeigt
 */
    public function set_top_topic($topic_item) {
        ci()->object_attribute_model()->db()->where('object_id', $topic_item->id);
        $topic_item->has_attribute = ci()->object_attribute_model()->get_items3(['object_type' => 'topic', 'attribute_type' => 'top_topic_for_navi'])->first();
        $topic_item->attribute_type = "top_topic_for_navi";
        $topic_item->object_type = "topic";
        $box_index['label'] = 'Top Reisethema/Reiseziel für Navi (Desktop "links", Mobile Startseite)';
        $box_index['wording_publish'] = 'Top Reisethemen/Reiseziele';
        return ci()->get_view("cms/object_attribute/list_view", ['item' => $topic_item, 'box_index' => $box_index]);
    }

}
