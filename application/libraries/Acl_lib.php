<?php

class Acl_lib
{

    private $group_id;
    /** @var MY_DB_query_builder $db */
    private $db;
    private $controller_table = "acl_controller";
    private $method_table = "acl_method";
    public $law_table = "acl_law";
    public $role_table = "acl_role";
    public $user_table = "acl_user";
    public $role_user_table = "acl_role_user";

    const PATH_TO_USER_ACL_MODEL = 'user/user_acl_model';
    const PATH_TO_USER_RIGHT_MODEL = 'user/user_right_model';
    const PATH_TO_USER_GROUP_MODEL = 'user/user_group_model';

    /**
     * Acl_lib constructor.
     */
    public function __construct() {
        if (ci()->auth_ldap_status === false)
            return null;
        $this->ci =& ci();
        if (1 || ci()->auth_ldap_status) {
            $this->ci->load->library(array('session'));
            $this->ci->load->model(array(
                self::PATH_TO_USER_ACL_MODEL,
                self::PATH_TO_USER_GROUP_MODEL,
                self::PATH_TO_USER_RIGHT_MODEL,
            ));
        }
    }

    /**
     * Checks session for superuser-role
     * @return bool
     */
    public function is_superuser() {
        $user_data = ci()->session->userdata('logged_in');
        return (@$user_data['roles']['superuser']) ? true : false;
    }

    /**
     * check (in sessions) if user has role(s)
     * @return bool
     */
    public function has_roles() {
        return (isset($_SESSION['logged_in']['roles'])) ? true : false;
    }

    public function has_role($role) {
        $user_data = $this->ci->session->userdata('logged_in');
        return (@$user_data['roles'][$role]) ? true : false;
    }

    /**
     * @return array
     * @example
     */
    private function get_roles_from_session() {
        $tmp = array();
        $roles = $_SESSION['logged_in']['roles'];
        foreach ($roles as $key => $value) {
            $tmp[$value] = $key;
        }
        return $tmp;
    }

    public function get_roles_from_db() {
        $user_id = $this->get_user_id();
        if (!$user_id)
            return null;
        $roles2 = [];
        $roles = ci()->user_model()->db->select('role_id')
            ->select('role')
            ->from("$this->role_table as role")
            ->join("$this->role_user_table as ru", "role.id = ru.role_id")
            ->where("ru.user_id", $user_id)
            ->get()
            ->result_object();
        foreach ($roles as $k => $v) {
            $roles2[$v->role_id] = $v->role;
            $_SESSION['logged_in']['roles'][$v->role] = $v->role_id;
        }
        return $roles2;
    }

    /**
     * @return int|null
     */
    public function get_user_id() {
        if (isset($_SESSION['logged_in']) && isset($_SESSION['logged_in']['user_id']))
            $user_id = (int)$_SESSION['logged_in']['user_id'] ?? null;
        else
            $user_id = false;
        return $user_id;
    }

    /**
     * @return bool
     */
    public function has_right() {
        $this->current_controller = ci()->router->directory . "" . ucfirst(ci()->router->class);
        $this->method = ci()->router->method;

        $this->db = ci()->user_model()->db;
        $law = $this->get_law(
            $this->current_controller,
            $this->method,
            $this->get_user_id()
        );
        if (is_null($law)) {
            $laws = $this->create_defaults(
                $this->current_controller,
                $this->method,
                $this->get_user_id()
            );
            $law = $laws[0];
            foreach ($laws as $law)
                $rights_msg[] = implode('/', array_values(array_slice((array)$law, 0, 3, true)));
            ci()->message("Benötigtes Recht: " . implode(" || ", $rights_msg));
            header("HTTP/1.1 401 Unauthorized");
        } else {
            if (!$law->allow) {
                ci()->message("Benötigtes Recht: " . implode('|', array_values(array_slice((array)$law, 0, 3, true))));
                header("HTTP/1.1 401 Unauthorized");
            }
        }
        return (bool)((is_object($law)) ? $law->allow : false);
        die;
    }

    /**
     * @param string $controller_name
     * @param string $method
     * @param int    $user_id
     *
     * @return array
     */
    public function create_defaults($controller_name, $method, $user_id) {
        # create default role and role_user-knot
        $this->create_default_user_role();
        # create controller
        $controller_id = $this->create_controller($controller_name);
        # create method
        $method_id = $this->create_methods([ci()->router->method]);
        # create law
        $user_roles = $this->get_roles_from_db();
        $laws = $this->create_law_rows($controller_id, $method_id, $user_roles);
        return $laws;
    }


    /**
     * @param MY_DB_query_builder $db
     * @param string              $controller
     * @param string              $method
     * @param int                 $user_id
     *
     * @return object
     */
    public function get_law($controller, $method, $user_id) {
        $q = "select
            law.controller_id, law.method_id, law.role_id, law.allow
            #law.allow 
            from acl_law as law
            inner join acl_controller as c ON law.controller_id = c.id
            inner join acl_method as m ON law.method_id = m.id
            inner join acl_role as role ON law.role_id = role.id
            inner join acl_role_user as ru ON role.id = ru.role_id
            inner join acl_user as u ON ru.user_id = u.id
            where 
            m.method = '$method'
            AND c.controller = '$controller'
            AND u.id = '$user_id'
            ORDER BY allow DESC
            LIMIT 1
            ";
        return ($obj = $this->db->query($q)->row_object()) ? $obj : null;
    }

    public function create_law_rows(int $controller_id, int $method_id, array $user_roles) {
        $db = $this->db;
        $laws = [];
        foreach ($user_roles as $k => $v) {
            $user_role_id = (is_numeric($k)) ? $k : ((is_numeric($v)) ? $v : null);

            $r = $db
                ->set('controller_id', $controller_id)
                ->set('method_id', $method_id)
                ->set('role_id', ($user_role_id) ?: 2)# 0 should not happen - 2 is defaultuser-role
                ->set('created', "now()", false)
                ->insert($this->law_table);
            if ($r)
                $laws[] = (object)['controller_id' => $controller_id, 'method_id' => $method_id, 'role_id' => $user_role_id, 'allow' => 0];
        }
        return $laws;
    }

    public function create_default_user_role() {
        $db = $this->db;
        $default_role = $db->select('*')
            ->from(User_role_model::TABLE)
            ->where('role', 'defaultuser')
            ->get()
            ->row_object();
        if (!$default_role) {
            $r = $db->set('created', 'NOW()', false)
                ->set('modified', 'NOW()', false)
                ->insert(User_role_model::TABLE, [
                    'creator' => 'default',
                    'role' => 'defaultuser'
                ]);
            $default_role_id = $db->insert_id();
        } else {
            $default_role_id = $default_role->id;
        }
        if ($default_role_id && $user_id = $this->get_user_id()) {
            $user_default_role = $db->select()
                ->where('user_id', $user_id)
                ->where('role_id', $default_role_id)
                ->get(User_role_knot_model::TABLE)->row_object();
            if ($user_default_role) {
                $_SESSION['logged_in']['roles']['defaultuser'] = $user_default_role->role_id;
            } else {
                $r = $db->set('created', 'NOW()', false)
//                ->set('modified', 'NOW()', false)
                    ->insert(User_role_knot_model::TABLE, [
                        'creator' => 'default',
                        'user_id' => $_SESSION['logged_in']['user_id'],
                        'role_id' => $default_role_id,
                    ]);
                $user_default_role_knot_id = $db->insert_id();
                $_SESSION['logged_in']['roles']['defaultuser'] = $default_role_id;
                $_SESSION['logged_in']['roles'][$default_role_id] = 'defaultuser';

            }
        }

    }

    public function create_controller($controller_name) {
        $controller = $this->db
            ->where('controller', $controller_name)
            ->get($this->controller_table)
            ->row_object();
        if ($controller) {
            $controller_id = $controller->id;
            return $controller_id;
        } else {
            $r = $this->db->set('created', 'NOW()', false)
                ->insert($this->controller_table, [
                    'creator' => 'default',
                    'controller' => $controller_name,
                ]);
            $controller_id = $this->db->insert_id();
        }
        return $controller_id;
    }

    public function create_methods($methods) {
        $current_method = ci()->router->method;
        foreach ($methods as $method_name) {
            $method = $this->db
                ->where('method', $method_name)
                ->get($this->method_table)
                ->row_object();
            if ($method) {
                if ($method_name == $current_method)
                    $method_id = $method->id;
            } else {
                $r = $this->db->set('created', 'NOW()', false)
                    ->insert($this->method_table, [
                        'creator' => 'default',
                        'method' => $method_name,
                    ]);
                if ($method_name == $current_method)
                    $method_id = $this->db->insert_id();
            }

        }
        return $method_id;
    }

}

