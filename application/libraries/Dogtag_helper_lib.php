<?php

MY_Controller::get_instance()->load->library("super_lib");

/**
 * Description of Topic_helper_lib
 *
 * @author Marco Heine
 */
class Dogtag_helper_lib extends Super_lib
{

    /**
     * Cronjob, der in den Paket- und Hotel-Texten nach Leistungen sucht und die dazugehörigen Hundemarken verknüpft
     */
    public function set_leistungen_dogtags() {

        #Übernachtungen
        $packages_with_false_dogtag = ci()->package_model()->db()->select('package.id as package_id, dogtag.id as dogtag_id, SUBSTRING(price_duration, 1, 1) as duration')
                                   ->from('package')
                                   ->join('dogtag_knots', 'object_id = package.id and object_type = "package"')
                                   ->join('dogtag', 'dogtag.id = dogtag_id and class = "Übernachtungen"')
                                   ->where('SUBSTRING(price_duration, 1, 1) != SUBSTRING(dogtag.name, 1, 1)')
                                   ->get()->result_array();

        $packages_without_dogtag_query = 'SELECT package.id as package_id, SUBSTRING(price_duration, 1, 1) as duration
from package 
where id not in (
SELECT package.id
from package
inner join dogtag_knots on object_id = package.id and object_type = "package"
inner join dogtag on dogtag.id = dogtag_id and class = "Übernachtungen"
)';
        $packages_without_dogtag = ci()->package_model()->db()->query($packages_without_dogtag_query)->result_array();

        $packages_to_edit = array_merge($packages_with_false_dogtag, $packages_without_dogtag);

        foreach ($packages_to_edit as $package) {
            if (is_numeric($package['duration'])) {
                $new_dogtag = ci()->dogtag_model()->db()->select('dogtag.id')
                    ->from('dogtag')
                    ->where('class', 'Übernachtungen')
                    ->where("SUBSTRING(name, 1, 1) = {$package['duration']}")
                    ->get()->row_object();

                $new_dogtag_id = @$new_dogtag->id;
                if ($new_dogtag_id) {
                    if (@$package['dogtag_id'])
                        ci()->dogtag_model()->db()->delete('dogtag_knots', ['dogtag_id' => $package['dogtag_id'], 'object_id' => $package['package_id'], 'object_type' => 'package']);
                    $r[] = ci()->dogtag_knot_model()->insert_item2(['dogtag_id' => $new_dogtag_id, 'object_type' => 'package', 'object_id' => $package['package_id'], 'weight' => 1, 'absolute_value' => '', 'creator' => 'sync', 'modifier' => 'sync']);
                }
            }
        }

        #Event-Start-Datum
        ci()->package_model()->db()->where('event_start > CURDATE()');
        ci()->package_model()->db()->order_by('event_start');
        $packages_with_event_date = ci()->package_model()->get_items3();

        $d_event_datum = ci()->dogtag_model()->get_items3(['name' => 'Datum', 'class' => 'Events'])->first();

        ci()->dogtag_model()->db()->delete('dogtag_knots', ['dogtag_id' => $d_event_datum->id, 'object_type' => 'package']);
        foreach ($packages_with_event_date as $key => $package) {
            $dates[$key]['event_start'] = $package->event_start;
            $dates[$key]['package_id'] = $package->id;
        }

        $date_count = count($dates);
        $interval = round(1 / $date_count, 2);

        foreach ($dates as $rank => $date) {
            $weight = 1 - ($rank * $interval);
            $r[] = ci()->dogtag_knot_model()->insert_item2(['dogtag_id' => $d_event_datum->id, 'object_type' => 'package', 'object_id' => $date['package_id'], 'weight' => $weight, 'absolute_value' => $date['event_start'], 'creator' => 'sync', 'modifier' => 'sync']);
        }

        #Inklusivleistungen
        $inklusivleistungen = ['Massage', 'Leihfahrrad', 'geführte Wanderung', 'Brauerei', 'Weinprobe', 'Schifffahrt', 'Skipass', 'Kochkurs'];

        foreach ($inklusivleistungen as $leistung) {
            $d_with_leistung = ci()->dogtag_model()->get_items3(['name' => $leistung, 'class' => 'Inklusivleistung'])->first();
            if ($d_with_leistung) {
                $p_ids_with_leistung = $this->get_packages_with_specific_leistung($leistung);

                foreach ($p_ids_with_leistung as $p_id) {
                    $knot_exists = ci()->dogtag_knot_model()->get_items3(['dogtag_id' => $d_with_leistung->id, 'object_id' => $p_id['id'], 'object_type' => 'package'])->first();
                    if (!$knot_exists) {
                        $knot_array = ['dogtag' => $d_with_leistung->id, 'package' => $p_id['id']];
                        $r[] = ci()->dogtag_knot_model()->insert_knot($knot_array);
                    }
                }
            }
        }

        #Einzelzimmer
        $d_einzelzimmer = ci()->dogtag_model()->get_items3(['name' => 'Einzelzimmer ohne Zuschlag'])->first();
        $sql_ez_ohne_aufpreis = "SELECT PaketNr
FROM Paket
join PaketPreis on Paket.PaketID = PaketPreis.PaketID
join PaketKategorie on Paket.PaketID = PaketKategorie.PaketID and keinEZbuchbar = 0
group by PaketNR
having sum(EZZuschlagpP) = 0";
        $pakete_ez_ohne_aufpreis = ci()->zombie_fake_model()->db()->query($sql_ez_ohne_aufpreis)->result_array();

        if ($pakete_ez_ohne_aufpreis) {
            ci()->dogtag_model()->db()->delete('dogtag_knots', ['dogtag_id' => $d_einzelzimmer->id, 'object_type' => 'package']);

            foreach ($pakete_ez_ohne_aufpreis as $paket_zombie) {
                $package_item = ci()->package_model()->get_item_by_field('package_number', $paket_zombie['PaketNr']);
                if ($package_item)
                    $r[] = ci()->dogtag_knot_model()->insert_knot(['dogtag' => $d_einzelzimmer->id, 'package' => $package_item->id], [], 1);
            }
        }

        #Hunde kostenlos
        $d_hund = ci()->dogtag_model()->get_items3(['name' => 'Hund kostenlos'])->first();
        $sql_hunde_ohne_aufpreis = "SELECT DISTINCT PaketNr, Beschreibung, Preis
FROM Paket join Artikel on
Paket.PaketID = Artikel.PaketID
where Beschreibung like '%Hund%' and Preis = 0;";
        $pakete_hunde_ohne_aufpreis = ci()->zombie_fake_model()->db()->query($sql_hunde_ohne_aufpreis)->result_array();

        foreach ($pakete_hunde_ohne_aufpreis as $paket_zombie) {
            $package_item = ci()->package_model()->get_item_by_field('package_number', $paket_zombie['PaketNr']);
            if ($package_item) {
                $knot_exists = ci()->dogtag_knot_model()->get_items3(['dogtag_id' => $d_hund->id, 'object_id' => $package_item->id, 'object_type' => 'package'])->first();
                if (!$knot_exists) {
                    $r[] = ci()->dogtag_knot_model()->insert_knot(['dogtag' => $d_hund->id, 'package' => $package_item->id], ['weight' => 0.5], 1);
                }
            }
        }

        #Hunde erlaubt
        $d_hund = ci()->dogtag_model()->get_items3(['name' => 'Hund erlaubt'])->first();
        $sql_hunde_erlaubt = "SELECT package_id, package_number
from search
where common_pets_allowed = 1;";
        $pakete_hunde_erlaubt = ci()->search_model()->db()->query($sql_hunde_erlaubt)->result_array();

        if ($d_hund->id && $pakete_hunde_erlaubt) {
            ci()->dogtag_model()->db()->delete('dogtag_knots', ['dogtag_id' => $d_hund->id, 'object_type' => 'package']);
            foreach ($pakete_hunde_erlaubt as $paket) {
                $r[] = ci()->dogtag_knot_model()->insert_knot(['dogtag' => $d_hund->id, 'package' => $paket['package_id']], ['weight' => 1], 1);
            }
        }

        #Hotelmarken/-ketten
        $d_hotelmarken = ci()->dogtag_model()->get_items3(['class' => 'Hotelketten']);
        $hotels = ci()->hotel_model()->get_items3();

        foreach ($d_hotelmarken as $d_h) {
            foreach ($hotels as $hotel) {
                if (strpos($hotel->name, $d_h->name) !== false) {
                    $knot_exists = ci()->dogtag_knot_model()->get_items3(['dogtag_id' => $d_h->id, 'object_id' => $hotel->id, 'object_type' => 'hotel'])->first();
                    if (!$knot_exists) {
                        $knot_array = ['dogtag' => $d_h->id, 'hotel' => $hotel->id];
                        $r[] = ci()->dogtag_knot_model()->insert_knot($knot_array);
                    }
                }
            }
        }

        #Regionen mit mehreren Ländern (Beispiel Hundemarke "D / Ostsee" -> auch mit nur "Ostsee" verknüpfen)
        $region_dogtags = [
            1786 => [403, 413, 890], #Adria
            1784 => [196, 207], #Ostsee
            1783 => [221, 233], #Erzgebirge
            1785 => [424, 512, 537] #Nordsee
        ];

        foreach ($region_dogtags as $dogtag_without_country => $dogtags_with_country) {
            foreach ($dogtags_with_country as $dogtag_with_country) {
                ci()->dogtag_knot_model()->db()->where(['dogtag_id' => $dogtag_with_country, 'object_type' => 'hotel']);
                $hotel_dogtag_knots = ci()->dogtag_knot_model()->get_items3();
                foreach ($hotel_dogtag_knots as $hotel_dogtag_knot) {
                    ci()->dogtag_knot_model()->db()->where(['dogtag_id' => $dogtag_without_country, 'object_type' => 'hotel', 'object_id' => $hotel_dogtag_knot->object_id]);
                    $check_dogtag_knot= ci()->dogtag_knot_model()->get_items3()->first();
                    if (!$check_dogtag_knot) {
                        $r[] = ci()->dogtag_knot_model()->insert_knot(['dogtag' => $dogtag_without_country, 'hotel' => $hotel_dogtag_knot->object_id], ['weight' => 1]);
                    }
                }
            }
        }

        return @$r;
    }

    public function get_packages_with_specific_leistung($leistung) {
        $query = "SELECT package.id
        from package
inner join object_object_knots on object1_type = 'content' and object2_type = 'package' and object2_id = package.id
inner join content on object1_id = content.id and type_id = 14
inner join content_element_knots on content.id = content_id
inner join element on element.id = element_id and element.text like '%$leistung%' and mobile = 1 where brand = 'smr';";

        return ci()->package_model()->db()->query($query)->result_array();
    }

}
