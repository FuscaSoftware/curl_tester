<?php

/**
 * @author  sebra
 * User: sebra
 * Date: 16.09.16
 * Time: 11:29
 * @version 2021-12-07
 *
 * @uses @requirements (optional) :
 *  ci()->config->item('protocol');
 *  ci()->config->item('api');
 *       $config[api] = [
 *          api_server => ...
 *          api_params => [
 *              api_server => ...
 *              api_path => ...
 *               "protocol" => "https",
 *                  "use_htaccess_login" => false,
 *                  "htaccess_login" => [
 *                  "username" => null,
 *                  "password" => null,
 *                  ],
 *              ],
 *       ],
 *
 */

/**
 *
 */
class Curl_base
{
    /** @var array $default_params Parameters for every curl connection */
    protected $default_params = array(
        "protocol" => "https",
        "use_htaccess_login" => false,
        "htaccess_login" => [
            "username" => null,
            "password" => null,
        ],
    );

    /** @var array $error */
    public $error;
    public $cache_handler = null;
    public $cache_time = 60 * 5; # 5min

    public function __construct() {

    }

    /**
     * @param string $url
     * @param array  $params connection parameters (login, htaccess, ...)
     * @param array  $get_params
     * @param array  $post_params
     *
     * @return Curl_response
     */
    public function get_data(string $url, array $params = [], array $get_params = [], array $post_params = []): Curl_response {
        if ($this->cache_handler) {
            $cached_response = $this->get_cached_request($url, $params, $get_params, $post_params);
            if ($cached_response)
                return $cached_response;
        }

        $params = $this->params = array_merge($this->default_params, $params);
        $get_params_url = $this->getparams_to_uri($get_params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . $get_params_url);
        if (!empty($post_params)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_params));
//        curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($params['use_htaccess_login']) {
            $username = $params['htaccess_login']['username'];
            $password = $params['htaccess_login']['password'];
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }
        if ($params['authorization_token_header'] ?? false) {
//            $post = json_encode($post); // Encode the data array into a JSON string       $post = json_encode($post); // Encode the data array into a JSON string
            $authorization = $params['authorization_token_header']['token'] . $params['authorization_token_header']['token']; // Prepare the authorisation token
            curl_setopt($ch, CURLOPT_HTTPHEADER, [$params['authorization_token_header']['content_type'], $authorization]); // Inject the token into the header
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);#?
//            curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
            if (0) { # since PHP 7.3 this should work; not tested
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BEARER);
                curl_setopt($ch, CURLOPT_XOAUTH2_BEARER, $params['authorization_token_header']['token']);
            }
        }
        if (@$params['user_agent']) {
            if ($params['user_agent'] == 'random') {
                $agents = array(
                    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
                    'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
                    'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
                    'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1'

                );
                curl_setopt($ch, CURLOPT_USERAGENT, $agents[array_rand($agents)]);
            } else
                curl_setopt($ch, CURLOPT_USERAGENT, $params['user_agent']);
        } else {
            curl_setopt($ch, CURLOPT_USERAGENT, "curl/" . CURL_VERSION_LIBZ);
        }

        $output = curl_exec($ch);
        if (!$output)
            $this->error = array(
                "url" => $url,
                "error" => curl_error($ch),
                "errno" => curl_errno($ch),
                'http_status' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
            );
        else
            $this->error = [
                "url" => $url,
                'http_status' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
            ];
        $response = new Curl_response($output, $url, curl_getinfo($ch, CURLINFO_HTTP_CODE), curl_errno($ch), curl_error($ch));
        curl_close($ch);
        if ($this->cache_handler)
            $this->set_cached_request($url, $params, $get_params, $post_params, $response);
        return $response;
    }


    /**
     * @param array $url_segments (only segments after :// )
     *
     * @return string implode segments with '/' as glue
     */
    public function url_segments_to_url($url_segments): string {
        if (is_string($url_segments))
            $url_segments = [$url_segments];
        if (empty($url_segments) || !is_array($url_segments))
            return "";
        return implode("/", $url_segments);
    }

    /**
     * @param array $get_params
     *
     * @return string like ?a=1&b=2
     */
    public function getparams_to_uri(array $get_params): string {
        if (!empty($get_params)) {
            foreach ($get_params as $k => $v)
                $get_params_vars[] = urlencode($k) . "=" . urlencode($v);
            $get_params_url = "?" . implode("&", $get_params_vars);
        } else $get_params_url = "";
        return $get_params_url;
    }


    private function get_cached_request(string $url, array $params = array(), array $get_params = [], array $post_params = []) {
        if ($this->cache_handler) {
            $cache_key = json_encode([$url, $get_params, $post_params]);
            return $this->cache_handler->get($cache_key);
        }
    }

    private function set_cached_request(string $url, array $params = array(), array $get_params = [], array $post_params = [], Curl_response $response) {
        if ($this->cache_handler) {
            $cache_key = json_encode([$url, $get_params, $post_params]);
            return $this->cache_handler->save($cache_key, $response, $this->cache_time);
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @param array  $params
     *
     * @return array
     */
    public function parameter_array_htaccess(string $username, string $password, array &$params = []) {
        return array_merge($params, [
            'use_htaccess_login' => true,
            "htaccess_login" => array(
                "username" => $username,
                "password" => $password,
            ),
        ]);
    }

    /**
     * @param string      $token
     * @param string|null $prefix // null: "Authorization: Bearer ", but could also be: "PRIVATE-TOKEN "
     * @param array       $params
     *
     * @return array
     */
    public function parameter_array_authorization_token_header(string $token, string $prefix = null, array &$params = []) {
        if (is_null($prefix))
            $prefix = "Authorization: Bearer ";
        return array_merge($params, [
            "authorization_token_header" => [
                "token" => $token,
                "prefix" => $prefix,
                "content_type" => 'Content-Type: application/json',
            ],
        ]);
    }
}

class Curl_lib extends Curl_base
{
    /** @var array $api_params Parameters for every api curl connection */
    public $api_params = [];
    /** @var string $api_server set by constructor (hostname, like localhost or back.entwicklung.spar-mit.com */
    protected $api_server;

    /** @var string $api_path */
    protected $api_path = "/public/api/";
    protected $api_path_cw = "/public/api_cw/";
    protected $search_path = "/search/search/get";
    protected $distance_path = "/search/search/getdistance";
    protected $location_path = "/search/search/getlocations";
    protected $getOptions_path = "/search/search/getAllOptions";

    public function __construct() {
        parent::__construct();
        $this->CI = &ci();
        $api_config = ci()->config->item('api');
        if (isset($api_config['api_server']))
            $this->api_server = $api_config['api_server'];
        if (isset($api_config['api_params']) && is_array($api_config['api_params'])) {
            $this->api_params = array_merge($this->api_params, $api_config['api_params']);
        }
        $protocol = ci()->config->item('protocol');
        if ($protocol)
            $this->api_params['protocol'] = $protocol;

        $this->api_params = array_merge($this->default_params, $this->api_params);
    }

    /**
     * @param array $url_segments
     * @param array $params connection parameters (login, htaccess, ...)
     * @param array $get_params
     * @param array $post_params
     *
     * @return string
     */
    public function get_api_data($url_segments, $params = [], $get_params = [], $post_params = []) {
        $path = $this->url_segments_to_url($url_segments);
        $params = array_merge($this->api_params, $params);
        $protocol = $params['protocol'];

        $api_server = (@$params['api_server']) ?: $this->api_server;
        $api_path = (@$params['api_path']) ?: $this->api_path;
        $url = $protocol . "://" . $api_server . $api_path . $path;
        return $this->get_data($url, $params, $get_params, $post_params);
    }

    /**
     * @param array|string $url_segments
     * @param array        $params connection parameters (login, htaccess, ...)
     * @param array        $get_params
     * @param array        $post_params
     *
     * @return string
     */
    public function get_api_cw_data($url_segments, $params = [], $get_params = [], $post_params = []) {
        $params = array_merge($params, ['api_path' => '/public/api_cw/']);
        return $this->get_api_data($url_segments, $params, $get_params, $post_params);
    }

    /**
     * @param array|string $url_segments
     * @param array        $params connection parameters (login, htaccess, ...)
     * @param array        $get_params
     * @param array        $post_params
     *
     * @return string
     */
    public function get_search_data($url_segments, $params = [], $get_params = [], $post_params = []) {
        $path = $this->url_segments_to_url($url_segments);
        $params = array_merge($this->api_params, $params);
        $protocol = $params['protocol'];
        $api_server = $this->api_server;
        $search_path = $this->search_path;
        $url = $protocol . "://" . $api_server . $search_path . $path;
        return $this->get_data($url, $params, $get_params, $post_params);
    }

    /**
     * @param array $url_segments
     * @param array $params connection parameters (login, htaccess, ...)
     * @param array $get_params
     * @param array $post_params
     *
     * @return string
     */
    public function get_distance_data($url_segments, $params = [], $get_params = [], $post_params = []) {
        $path = $this->url_segments_to_url($url_segments);
        $params = array_merge($this->api_params, $params);
        $protocol = $params['protocol'];
        $api_server = $this->api_server;
        $search_path = $this->distance_path;
        $url = $protocol . "://" . $api_server . $search_path . $path;
        return $this->get_data($url, $params, $get_params, $post_params);
    }


    /* @param
     * @return json with range pakets
     */
    public function get_range_data($url_segments, $params = [], $get_params = [], $post_params = []) {
        $path = $this->url_segments_to_url($url_segments);
        $params = array_merge($this->api_params, $params);
        $protocol = $params['protocol'];
        $api_server = $this->api_server;
        $search_path = $this->location_path;
        $url = $protocol . "://" . $api_server . $search_path . $path;
        return $this->get_data($url, $params, $get_params, $post_params);
    }

    /*
     * which options? ->rename get_search_options
     * @params
     * return json with package information
     */
    public function get_options_data($url_segments, $params = [], $get_params = [], $post_params = []) {
        $path = $this->url_segments_to_url($url_segments);
        $params = array_merge($this->api_params, $params);
        $protocol = $params['protocol'];
        $api_server = $this->api_server;
        $search_path = $this->getOptions_path;
        $url = $protocol . "://" . $api_server . $search_path . $path;
        return $this->get_data($url, $params, $get_params, $post_params);
    }

}

class Curl_response implements Serializable
{
    private $plain_response;
    private $decoded;
    public $url;
    public $http_status;
    public $error_no;
    public $error_msg;

    public function __construct($response, $url, $http_status, $error_no, $error_msg) {
        $this->plain_response = $response;
        $this->url = $url;
        $this->http_status = $http_status;
        $this->error_no = $error_no;
        $this->error_msg = $error_msg;
    }

    public function __toString() {
        return $this->plain();
    }

    public function plain() {
        if ($this->http_status == 404)
            return '';
        return $this->plain_response;
    }

    public function decoded() {
        if (!$this->decoded)
            $this->decoded = json_decode($this->plain_response);
        return $this->decoded;
    }


    /**
     * String representation of object
     * @link  http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize() {
        return serialize(get_object_vars($this));
    }

    /**
     * Constructs the object
     * @link  http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     *
     * @return void
     * @since 5.1.0
     */
    public function unserialize($data) {
        if (method_exists('stdClass', 'unserialize'))
            return stdClass::unserialize($data);
        else {
            $values = unserialize($data);
            foreach ($values as $key => $value) {
                $this->$key = $value;
            }
        }
    }

}