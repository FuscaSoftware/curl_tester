<?php

class Auth_ldap
{
    private $ldap_host = '192.168.1.10';

    /**
     * ldap tree
     * @var string
     */
    private $base_dn = "OU=Sparmit,DC=sparmit,DC=local";

    private $attributes = array(
        'department',
        'displayname',
        'facsimileTelephoneNumber',
        'mail',
        'mailNickname',
        'mobile',
        'telephoneNumber',
        'title',
        'samAccountName',
//        'cn',
//        'extensionAttribute7',
    );

    private $ldap_connect;

    const PATH_TO_USER_LOGIN_LOG_MODEL = 'user/user_login_log_model';
    const PATH_TO_USER_MODEL = 'user/user_model';
    const PATH_TO_USER_ROLE_KNOT_MODEL = 'user/user_role_knot_model';
    const PATH_TO_USER_ROLE_MODEL = 'user/user_role_model';

    /**
     * Auth_ldap constructor.
     */
    public function __construct()
    {
        if (ci()->auth_ldap_status === false)
            return null;
        $this->ci =& ci();
        if (1 || ci()->auth_ldap_status) {
            $this->ldap_host = ci()->config->item('ldap_host') ?? $this->ldap_host;
            $this->ci->load->library(array('session'));
            $this->ci->load->model(array(
                self::PATH_TO_USER_LOGIN_LOG_MODEL,
                self::PATH_TO_USER_MODEL,
                self::PATH_TO_USER_ROLE_KNOT_MODEL,
                self::PATH_TO_USER_ROLE_MODEL,
            ));
        }
    }

    /**
     * ldap_set_option — Setzt den Wert der gegebenen Option
     */
    private function ldap_set_option()
    {
        ldap_set_option($this->ldap_connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->ldap_connect, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($this->ldap_connect, LDAP_OPT_TIMELIMIT, 10);
    }

    /**
     * @param $user_name
     * @return array
     * @example
     *
     * [cn] => Bassam Fatahulla
     * [title] => Entwicklung/ Web      | [title] => Support & Controlling| [title] => Onlinemarketing        | [title] => Student DHBW
     * [displayname] => Bassam Fatahulla
     * [department] => Entwicklung/ Web | [department] => Reiseproduktion | [department] => Sales & Marketing | [department] => DHBW-Studentin
     * [extensionattribute7] => 1
     * [mailnickname] => b.fatahulla
     * [mail] => b.fatahulla@spar-mit.com
     */
    private function prepare_filter_and_attributes($user_name)
    {
        $filter = "(samaccountname=" . $user_name . ")";
        return array($filter, $this->attributes);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function get_ldap_values($data)
    {
        $tmp = array();
        foreach ($data as $key => $value) {
            if (!empty($value) && is_array($value)) {
                $tmp[$key] = trim($value[0]);
            }
        }
        return $tmp;
    }

    /**
     * @param $data_form_ldap
     * @return array
     */
    private function get_data_for_session($data_form_ldap)
    {
        $tmp = array();
        foreach ($data_form_ldap as $key => $value) {
            if (in_array($key, array(
                User_model::PROP_TITLE,
                User_model::PROP_DISPLAYNAME,
                User_model::PROP_DEPARTMENT,
                User_model::PROP_MAILNICKNAME,
                User_model::PROP_MAIL,
            ))) {
                $tmp[$key] = $value;
            }
        }
        return $tmp;
    }

    /**
     * @param $login_data
     * @return bool
     *
     * Array
     *  (
     *      [user_name] => b.fatahulla
     *      [user_password] => Geheim
     *  )
     */
    public function authenticate_user($login_data)
    {
        if (!ci()->session)
            die('Without Session no authentication');
        if (ci()->config->item('use_ldap') == false)
            return true;

        $ping = $this->serviceping($this->ldap_host, 389, 2);
        if (!$ping)
            ci()->message("Ping to LDAP-SERVER failed! ;-(", 'warning');

        /**
         * ldap_connect — Verbindet zu einem LDAP Server
         * @var resource $ldap_connect
         * @example resource(7) of type (ldap result)
         */
        $this->ldap_connect = ($ping)? ldap_connect($this->ldap_host) : false;

        if (is_resource($this->ldap_connect) && $this->ldap_connect) {
            $user_name = $login_data['user_name'];

//            $password_substr = substr($login_data['user_password'], -3);
            $password_last_substr = substr($login_data['user_password'], -3);
            $password_first_substr = substr($login_data['user_password'], 0, 3);
            $password_substr = $password_last_substr . 'smr' . $password_first_substr;

            $hash_fom_login = password_hash($password_substr, PASSWORD_BCRYPT);
            $access_status = false;

            $ldapDn = 'sparmit' . "\\" . $user_name;
            self::ldap_set_option();

            // User
            $user_id = null;
            $user_infos = $this->ci->user_model->select_row(
                ['id', 'password'],
//              ['mailnickname' => $user_name]
                ['samAccountName' => $user_name]
            );

            $hash_from_db = null;
            if (!empty($user_infos)) {
                $user_id = $user_infos['id'];
                $hash_from_db = $user_infos['password'];
            }

            /**
             * ldap_bind — Bindung zu einem LDAP Verzeichnis
             * @var bool $ldap_bind
             */
            $ldap_bind = @ldap_bind($this->ldap_connect, $ldapDn, $login_data['user_password']);

            if ($ldap_bind) {
                // ldap success
                list($filter, $attributes) = $this->prepare_filter_and_attributes($user_name);
                /**
                 * ldap_search — Suche im LDAP Baum
                 * @var resource | bool $ldap_search
                 * @example resource(7) of type (ldap result)
                 */
                $ldap_search = @ldap_search($this->ldap_connect, $this->base_dn, $filter, $attributes);

                if ($ldap_search) {
                    $data_from_ldap = @ldap_get_entries($this->ldap_connect, $ldap_search);
                    $data_form_ldap = $this->get_ldap_values($data_from_ldap[0]);
                }

                if (isset($user_id)) {
                    // update user
                    $data_form_ldap['password'] = $hash_fom_login;
                    $this->ci->user_model->update_row($data_form_ldap, array('id' => $user_id));
                    $access_status = true;
                } else {
                    // insert user
                    $data_form_ldap['password'] = $hash_fom_login;
                    $user_id = $this->ci->user_model->insert_row($data_form_ldap);
                    $access_status = true;
                }
                ldap_close($this->ldap_connect);
            } else {
                // ldap failure
                if (isset($user_id) && password_verify($password_substr, $hash_from_db)) {
                    $data_form_db = $this->ci->user_model->select_row($this->attributes,
//                      array('mailnickname' => $user_name)
                        ['samAccountName' => $user_name]
                    );
                    $data_form_ldap = $data_form_db;
                    $access_status = true;
                }
            }

            if ($access_status) {
                $this->login_access($data_form_ldap, $user_id, $user_name);
                return true;
            } else {
                return false;
            }
        }
        else
            die("Could not connect to LDAP server.");
    }

    /**
     * after a successfull login write the user to the user table, set the session and write an entry in the log.
     * @param array $data_form_ldap
     * @param int   $user_id
     */
    public function login_access($data_form_ldap, $user_id, $user_name) {
        // add to session
        $data_form_session = self::get_data_for_session($data_form_ldap);
        $roles = $this->ci->user_role_knot_model->get_user_active_roles_by_user_id($user_id);
        if (!empty($roles)) {
            $data_form_session['roles'] = $roles;
//            ci()->dump(['roles' => $roles]);
        }
        $data_form_session['user_id'] = $user_id;
        $this->ci->session->set_userdata('logged_in', $data_form_session);

        // user login log
        $this->ci->user_login_log_model->insert_row($user_name);
    }

    public function is_authenticated()
    {
        if (!ci()->session)
            die('SESSION NOT LOADED. LOGIN NOT POSSIBLE');
        if (!ci()->session->userdata('logged_in')) {

            $where_am_i = ci()->where_am_i();
//                $requester = rawurlencode(htmlentities(json_encode(get_instance()->where_am_i())));
//                $requester = rawurlencode(htmlentities(json_encode(get_instance()->where_am_i())));
            if ($where_am_i['class'] == 'Login')
                $requester = '';
            else
                $requester = rawurlencode(str_replace("/", "||", (($where_am_i['uri_string']))));
            if (ci()->request_type == "ajax") {
                ci()->message("Sie müssen sich neu einloggen! <a target=\"_blank\" href=\"".site_url("login")."\">Login</a>", "danger", 'default', 6000, "Session abgelaufen: ");
                echo ci()->show_ajax_message(ci()->data, TRUE);
                die;
            } else {
                ci()->message("Sie haben keine gültige Session!", "warning", 'default', 1500, __CLASS__.":".__METHOD__.":");
                redirect('login/index/' . ci()->request_type . "/" . $requester, 'auto', 307);
                die;
            }
        } else {
            return true;
        }
    }

    /**
     * @return bool
     */
    public function is_producer()
    {
        $user_data = $this->ci->session->userdata('logged_in');
        return (@$user_data['department'] == 'Reiseproduktion') ? true : false;
//        return ($user_data['department'] == 'Entwicklung/ Web')? true :false;
//        return ($user_data['department'] == 'Sales & Marketing')? true :false;
//        return ($user_data['department'] == 'DHBW-Studentin')? true :false;
    }

    /**
     * @return mixed
     */
    public function get_displayname_from_session()
    {
        return $_SESSION['logged_in']['displayname'];
    }

    function serviceping($host, $port=389, $timeout=1)
    {
        $op = fsockopen($host, $port, $errno, $errstr, $timeout);
        if (!$op)
            return 0; //DC is N/A
        else {
            fclose($op); //explicitly close open socket connection
            return 1; //DC is up & running, we can safely connect with ldap_connect
        }
    }
}