<?php

/**
 * Description of Content
 *
 * @author sebra
 */
class Content_lib extends Super_lib
{

    public $edit_box_defaultParams = array(
        "title" => "Content anlegen",
    );

    public function edit_box($id, &$data, $params = array())
    {
        if (!$id || $id == 0) {
            $item = $this->content_model->get_empty_item();
            $item->id = 0;
        } elseif (is_numeric($id)) {
            $item = $this->content_model->get_item_by_id($id);
        } else {
            $this->message("Content mit ID: $id ist ungültig.");
        }
        if (!$item) {
            $this->message("Content mit ID: $id nicht gefunden.");
        }
        $data['box_edit']['item'] = $item;
        $params = array_merge($this->edit_box_defaultParams, $params);

        $data['box_edit']['title'] = $params['title'];
        $data['box_edit']['form']['target'] = "cms/content/save";
        $data['box_edit']['form']['backend_fields'] = $this->content_model->backend_fields;
        $data['box_edit']['itemtitle'] = "Inhalt";
        $data['box_edit']['modelname'] = $this->content_model->table;
        return $this->get_view("boxes/edit", $data);
    }

}
