<?php

MY_Controller::get_instance()->load->library("super_lib");

/**
 * Description of Element_lib
 *
 * @author sebra
 */
class Element_lib extends Super_lib
{

    /**
     * @deprecated since 2016-12-14 use Element_lib::show_elements2()
     * @param array $element_items
     * @param null $element_types
     * @param Item $content_item
     * @return string returns view of element(s)
     */
    public function show_elements($element_items, $element_types = null, $content_item = null): string {
        return self::show_elements2($element_items, $content_item);
    }

    /**
     * @param array $element_items
     * @param Item $content_item
     * @return string returns view of element(s)
     */
    public static function show_elements2($element_items, $content_item = null): string {
        $output = array();
        $i = 0;
        foreach ($element_items as $k => $element) {
            $output[] = self::show_single_element($i, $element, $content_item);
        }
        if (is_object($content_item) && $content_item->content_type_key == "topichead") {
            return ci()->get_view("cms/element/list_views/topichead_view", ["elements" => $output]);
        } else {
            return ci()->get_view("cms/element/list_views/default_view", ["elements" => $output]);
        }
    }

    public static function show_single_element(int &$i, stdClass $element, Item $content_item = null): string {
        $output = [];
        $i++;
        $data['content_item'] = $content_item;
        $data['content_id'] = $content_item->id;
        $data['item'] = $item = $element;
        $data['input_id'] = $item_id = "data_" . ci()->element_model->table . "_" . $item->id . "_text";
        $data['box_element'] = [
//                'dom_class' => "well well-sm cms_element_item element_id_$item->id ui-state-default",
            'dom_class' => "well well-sm cms_element_item element_id_$item->id " . self::css_viewport($item),
            'dom_id' => 'element_id_' . $item->id,
            'dom_title' => "Element ID: $item->id",
            'dom_data' => ['input_id' => $item_id],
        ];
        /* todo: use $element->element_type_key instead of elemtent_type_name */
        $element_type_key = mb_strtolower($element->element_type_key);
        if (in_array($element_type_key, array("text", "comment", "link", "teasertext", "alternativ_text")))
            $output[] = ci()->get_view("cms/element/element_text_view", $data);
        elseif (in_array($element_type_key, array("subheading", "subheadingsmall", "subheadingsmallspecial")))
            $output[] = ci()->get_view("cms/element/element_input_view", $data);
        elseif (in_array($element_type_key, array("package_price", "hotel_title")))
            $output[] = ci()->get_view("cms/element/element_input_view", $data);
        elseif (in_array($element_type_key, array("picture")))
            $output[] = ci()->get_view("cms/element/element_picture_view", $data);
        elseif (in_array($element_type_key, array("old_picture")))
            $output[] = ci()->get_view("cms/element/element_old_picture_view", $data);
        elseif (in_array($element_type_key, array("gallery")))
            $output[] = ci()->get_view("cms/element/element_gallery_view", $data);
        elseif (in_array($element_type_key, array("text_ck", "infobox")))
            $output[] = ci()->get_view("cms/element/cke1_text_view", $data);
        elseif (in_array($element_type_key, array("video")))
            $output[] = ci()->get_view("cms/element/element_video-modal_view", $data);
        elseif (in_array($element_type_key, array("inline_video")))
            $output[] = ci()->get_view("cms/element/element_inline-video_view", $data);
        elseif (in_array($element_type_key, array("link_intern")))
            $output[] = ci()->get_view("cms/element/element_link_view", $data);
        elseif (in_array($element_type_key, array("pricelabel")))
            $output[] = ci()->get_view("cms/element/element_pricelabel_view", $data);
        elseif (in_array($element_type_key, array("subheadingsmall")))
            $output[] = ci()->get_view("cms/element/element_input_view", $data);
        elseif (in_array($element_type_key, array("service_ez", "service_an", "kontingent_of", "kontingent_is")))
            $output[] = ci()->get_view("cms/element/element_input_view", $data);
        else {
            $output[] = ci()->get_view("cms/element/element_text_view", $data);
            ci()->message('$element->element_type_key' . var_export($element->element_type_key, 1) . "' / '"
                . var_export($element_type_key, 1) . " is undefined!"
                . "<br/> or key: " . var_export($element_type_key, 1) . " is undefined!"
                , "warning");
        }
        # layout-helper
        if (is_object($content_item))
            if ($content_item->content_type_key == "topichead") {
                //                if ($i == 2){
                //                    $output[] = "<div style='width: 250px;height: 200px;float: left;'> ## Producerbild ## </div>";
                //                }
                //                if ($i == 3){
                //                    $output[] = "<div style='background-color: #f5f5f5;'> ## Karte ## </div>";
                //                }
                //                if ($i == 4){
                //                    $output[] = "<div> ## Pakete ## </div>";
                //                }
            }
        return implode("", $output);
    }

    public function show_add_element($content_id, $element_types, $sort = null) {
        if (!$element_types) {
            $element_types = ci()->element_model()->get_types_items();
            $content_item = ci()->content_model()->get_item_by_id($content_id);
            foreach ($element_types as $key => $value) {
                if (!empty($value->context)) {
                    if (stristr($value->context, $content_item->content_type_key) === FALSE)
                        unset($element_types[$key]);
                }
            }
        }
        $data['element_types'] = $element_types;
        $data['content_id'] = $content_id;
        $data['sort'] = $sort;
        $output[] = ci()->get_view("cms/element/add_element_view", $data);
        return implode("", $output);
    }

    /**
     * @param Item $item
     * @return string css-class for single element view
     */
    public static function css_viewport($item) {
        $css_class = "";
        if (is_null($item->mobile))
            $item->mobile = 1;
        if (is_null($item->desktop))
            $item->desktop = 1;
        if (is_null($item->show_smr))
            $item->show_smr = 1;
        if (is_null($item->show_cw))
            $item->show_cw = 1;
        $css_class .= ($item->mobile) ? " viewport_mobile " : "";
        $css_class .= ($item->desktop) ? " viewport_desktop " : "";
        $css_class .= ($item->show_smr) ? " viewport_smr " : "";
        $css_class .= ($item->show_cw) ? " viewport_cw " : "";
        return $css_class;
    }
}
