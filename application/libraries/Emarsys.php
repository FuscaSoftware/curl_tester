<?php

/**
 * emarsys
 *
 * @see http://documentation.emarsys.com/resource/developers/getting-started/authentication/php-sample/
 * @see https://api.emarsys.net/api-demo/
 *
 */
class Emarsys
{
    private $_secret;
    private $_suiteApiUrl;
    private $_username;

    const EMAIL = 'email';
    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const TITLE = 'title';

    const DELETE = 'DELETE';
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';

    const CREATING_A_CONTACT = 'creating_a_contact';
    const SIGN_OUT_FROM_EMAIL = 'sign_out_from_email';
    const SIGN_OUT_FROM_FORM = 'sign_out_from_form';
    const TRIGGERING_AN_EXTERNAL_EVENT_350 = 'triggering_an_external_event_350'; // Newsletter
    const TRIGGERING_AN_EXTERNAL_EVENT_3209 = 'triggering_an_external_event_3209'; // Gladbach
    const TRIGGERING_AN_EXTERNAL_EVENT_3210 = 'triggering_an_external_event_3210'; // Gladbach und Newsletter
    const TRIGGERING_AN_EXTERNAL_EVENT_3937 = 'triggering_an_external_event_3937'; // Paket-Verlängerungs-Info
    const TRIGGERING_AN_EXTERNAL_EVENT_4112 = 'triggering_an_external_event_4112'; // Paket-Überarbeitung-Info
    const CREATING_A_EXTENSION_REMIND_CONTACT = 'creating_a_extension_remind_contact';
    const CREATING_A_UEBERARBEITUNG_REMIND_CONTACT = 'creating_a_ueberarbeitung_remind_contact';


    const UPDATING_A_CONTACT = 'updating_a_contact';

    /**
     * SuiteApi constructor.
     */
    public function __construct()
    {
        $this->_secret = 'cgf4ACPZ6dI4WuBk0bpp';
        $this->_suiteApiUrl = 'https://api.emarsys.net/api/v2/';
        $this->_username = 'spar_mit_reisen002';
    }


    /**
     * @param $operation
     * @param $form_data
     * @return string
     */
    private function prepare_data($operation, $form_data)
    {
        switch ($operation) {

            case self::CREATING_A_CONTACT:
                // https://api.emarsys.net/api/v2/contact
                $arr = array(
                    1 => $form_data[self::FIRSTNAME],
                    2 => $form_data[self::LASTNAME],
                    3 => $form_data[self::EMAIL],
                    4494 => md5($form_data[self::EMAIL])
                );
                if (isset($form_data[self::TITLE])) {
                    switch ($form_data[self::TITLE]) {
                        case 'Herr':
                            $arr[46] = 1;
                            break;
                        case 'Frau':
                            $arr[46] = 2;
                            break;
                    }
                }

                $requestType = self::PUT;
                $endPoint = 'contact/?create_if_not_exists=1';
                break;

            case self::TRIGGERING_AN_EXTERNAL_EVENT_350:
                //https://api.emarsys.net/api/v2/event/350/trigger';
                $arr = array(
                    'key_id' => 3,
                    'external_id' => $form_data[self::EMAIL],
                );

                $requestType = self::POST;
                $endPoint = 'event/350/trigger';

                break;
            case self::TRIGGERING_AN_EXTERNAL_EVENT_3209:
                //https://api.emarsys.net/api/v2/event/3209/trigger';
                $arr = array(
                    'key_id' => 3,
                    'external_id' => $form_data[self::EMAIL],
                );

                $requestType = self::POST;
                $endPoint = 'event/3209/trigger';

                break;
            case self::TRIGGERING_AN_EXTERNAL_EVENT_3210:
                //https://api.emarsys.net/api/v2/event/3210/trigger';
                $arr = array(
                    'key_id' => 3,
                    'external_id' => $form_data[self::EMAIL],
                );

                $requestType = self::POST;
                $endPoint = 'event/3210/trigger';

                break;
            case self::TRIGGERING_AN_EXTERNAL_EVENT_3937:
                //https://api.emarsys.net/api/v2/event/3937/trigger';
                $arr = array(
                    'key_id' => 3,
                    'external_id' => $form_data[self::EMAIL],
                );

                $requestType = self::POST;
                $endPoint = 'event/3937/trigger';

                break;
            case self::TRIGGERING_AN_EXTERNAL_EVENT_4112:
                //https://api.emarsys.net/api/v2/event/4112/trigger';
                $arr = array(
                    'key_id' => 3,
                    'external_id' => $form_data[self::EMAIL],
                );

                $requestType = self::POST;
                $endPoint = 'event/4112/trigger';

                break;
            case self::CREATING_A_EXTENSION_REMIND_CONTACT:
                // https://api.emarsys.net/api/v2/contact
                $arr = array(
                    17062 => $form_data['package_number'],
                    17071 => $form_data['hotel'],
                    17073 => $form_data['ort'],
                    17074 => $form_data['package_valid_until'],
                    3 => $form_data[self::EMAIL],
                    4494 => md5($form_data[self::EMAIL])
                );

                $requestType = self::PUT;
                $endPoint = 'contact/?create_if_not_exists=1';
                break;
            case self::CREATING_A_UEBERARBEITUNG_REMIND_CONTACT:
                // https://api.emarsys.net/api/v2/contact
                $arr = array(
                    17062 => $form_data['package_number'],
                    17071 => $form_data['hotel'],
                    17073 => $form_data['ort'],
                    3 => $form_data[self::EMAIL],
                    4494 => md5($form_data[self::EMAIL])
                );

                $requestType = self::PUT;
                $endPoint = 'contact/?create_if_not_exists=1';
                break;
            case self::UPDATING_A_CONTACT:
                //https://api.emarsys.net/api/v2/contact'
                $arr = array(
                    31 => 1,
                    4494 => $form_data['token'],
                    'key_id' => 4494
                );

                if (isset($form_data['gladbach']) && isset($form_data['newsletter'])) {
                    $arr[14951] = 'Ja';
                    $arr[6185] = 'Ja';
                } else if (isset($form_data['gladbach']))
                    $arr[14951] = 'Ja';
                else
                    $arr[6185] = 'Ja';

                $requestType = self::PUT;
                $endPoint = 'contact';
                break;

            case self::SIGN_OUT_FROM_EMAIL:
                //https://api.emarsys.net/api/v2/contact'
                $arr = array(
                    31 => 2,
//                    4494 => $form_data['token'],
//                    'key_id' => 4494
                    3 => $form_data['email'],
                    'key_id' => 3
                );
                $requestType = self::PUT;
                $endPoint = 'contact';
                break;

            case self::SIGN_OUT_FROM_FORM:
                //https://api.emarsys.net/api/v2/contact'
                $arr = array(
                    31 => 2,
                    3 => $form_data[self::EMAIL],
                );
                $requestType = self::PUT;
                $endPoint = 'contact';
                break;

            default:
                die('Operation ist unbekannt');
                break;
        }
        return array($requestType, $endPoint, json_encode($arr));
    }

    /**
     * @param $operation
     * @param $form_data
     * @throws Exception
     */
    public function send($operation, $form_data)
    {
        list ($requestType, $endPoint, $requestBody) = self::prepare_data($operation, $form_data);

        if (!in_array($requestType, array('GET', 'POST', 'PUT', 'DELETE'))) {
            throw new Exception('Send first parameter must be "GET", "POST", "PUT" or "DELETE"');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($requestType) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
        }

        curl_setopt($ch, CURLOPT_HEADER, true);

        $requestUri = $this->_suiteApiUrl . $endPoint;
        curl_setopt($ch, CURLOPT_URL, $requestUri);

        /**
         * We add X-WSSE header for authentication.
         * Always use random 'nonce' for increased security.
         * timestamp: the current date/time in UTC format encoded as
         *   an ISO 8601 date string like '2010-12-31T15:30:59+00:00' or '2010-12-31T15:30:59Z'
         * passwordDigest looks sg like 'MDBhOTMwZGE0OTMxMjJlODAyNmE1ZWJhNTdmOTkxOWU4YzNjNWZkMw=='
         */

        $nonce = 'd36e316282959a9ed4c89851497a717f';
        $timestamp = gmdate("c");
        $passwordDigest = base64_encode(sha1($nonce . $timestamp . $this->_secret, false));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-WSSE: UsernameToken ' . 'Username="' . $this->_username . '", ' . 'PasswordDigest="' . $passwordDigest . '", ' . 'Nonce="' . $nonce . '", ' . 'Created="' . $timestamp . '"',
            'Content-type: application/json;charset=\"utf-8\"',
        ));
        $response = curl_exec($ch);
        curl_close($ch);
//        return $response;
    }

    public function send_ueberarbeitung_finish_mail($package_item) {
        $user_to_send_mail = ci()->zombie_reporting_model()->db()->select('*')
            ->from('package_ueberarbeitung_mails')
            ->where('package_number', $package_item->package_number)
            ->where('sent is null')
            ->get()->result_array();

        $hotel_data = ci()->hotel_model()->get_items_by_knot2($package_item->id, 'package')->first();

        if ($user_to_send_mail) {
            foreach ($user_to_send_mail as $user) {
                $form_data['email'] = $user['mail'];
                $form_data['package_number'] = $package_item->package_number;
                $form_data['ort'] = $hotel_data->address;
                $form_data['hotel'] = ($hotel_data->label) ? $hotel_data->label : $hotel_data->name;
                $emarsysObj = new Emarsys();
                $emarsysObj->send(Emarsys::CREATING_A_UEBERARBEITUNG_REMIND_CONTACT, $form_data);
                $emarsysObj->send(Emarsys::TRIGGERING_AN_EXTERNAL_EVENT_4112, $form_data);
                ci()->zombie_reporting_model()->db()->update('package_ueberarbeitung_mails', ['sent' => date('Y-m-d H:i:s')], "id = {$user['id']}");
            }
        }
    }

    public function send_extension_reminder_to_customers($package_number = null) {
        if (!$package_number) {
            $extended_packages = ci()->zombie_reporting_model()->db()->select('package_number, package_valid_until')
                ->from('package_history')
                ->where('action', 'valid_change')
                ->where('DATE_FORMAT(action_date, "%Y-%m-%d") = CURDATE()')
                ->get()->result_array();

            $new_or_reactivated_packages = ci()->zombie_reporting_model()->db()->select('package_number, package_valid_until')
                ->from('package_history')
                ->where('action', 'online')
                ->where('mail_sent', 1)
                ->where('DATE_FORMAT(action_date, "%Y-%m-%d") = CURDATE() - interval 1 day')
                ->get()->result_array();

            $extended_packages = array_merge($extended_packages, $new_or_reactivated_packages);
        } else {
            $extended_packages[] = ['package_number' => $package_number];
        }

        foreach ($extended_packages as $package) {
            $reminders = ci()->zombie_reporting_model()->db()->select('id, mail')
                ->from('package_extended_reminder_customers')
                ->where('package_number', $package['package_number'])
                ->where('sent is null')
                ->get()->result_array();

            if ($reminders) {
                $package_data = ci()->package_model()->get_item_by_field('package_number', $package['package_number']);
                $hotel_data = ci()->hotel_model()->get_items_by_knot2($package_data->id, 'package')->first();
                if (!@$package['package_valid_until']) {
                    $valid_until = @ci()->package_model()->db()->query("SELECT max(valid_until) as max_valid
from hazelnut_paket_saison
where package_id = " . $package_data->id)->result_object()[0]->max_valid;
                    $package['package_valid_until'] = $valid_until;
                }

                $data['packages_with_reminders'][] = $package;
                foreach ($reminders as $remind) {
                    $form_data['email'] = $remind['mail'];
                    $form_data['package_number'] = $package['package_number'];
                    $form_data['package_valid_until'] = date('d.m.Y', strtotime($package['package_valid_until']));
                    $form_data['ort'] = $hotel_data->address;
                    $form_data['hotel'] = ($hotel_data->label) ? $hotel_data->label : $hotel_data->name;
                    $emarsysObj = new Emarsys();
                    $emarsysObj->send(Emarsys::CREATING_A_EXTENSION_REMIND_CONTACT, $form_data);
                    $emarsysObj->send(Emarsys::TRIGGERING_AN_EXTERNAL_EVENT_3937, $form_data);
                    ci()->zombie_reporting_model()->db()->update('package_extended_reminder_customers', ['sent' => date('Y-m-d H:i:s')], "id = {$remind['id']}");
                }
            }
        }

        if (!@$data)
            $data['message'] = "keine Mails versendet!";

        return $data;
    }
}