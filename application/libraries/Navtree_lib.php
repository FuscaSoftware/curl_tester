<?php
/**
 * User: sbraun
 * Date: 01.11.17
 * Time: 10:18
 */
ci()->load->library("Super_lib");
class Navtree_lib extends Super_lib
{
    public function tree_root() {
        return new Collection([
            (object)["text" => "Reisethemen", 'id' => "topic_root", 'icon' => "fa fa-folder"],
            (object)["text" => "Reiseziele", 'id' => "location_root", 'icon' => "fa fa-folder"],
        ]);
    }

    public function tree_location_root() {
        $location_select = "location.id, title as text, 'location|country' as object_type, 'fa fa-flag' as icon";
        $items = $this->location_model()->get_items3(['type' => "country", "order_by" => 'title'], $location_select);
        return $items;
    }

    public function tree_topic($topic_parent_id) {
        $topic_select = "topic.id, title as text, 'topic|reisethemen' as object_type, 'fa fa-sitemap' as icon";
        $parent_id = ($topic_parent_id)? $topic_parent_id : 12;
        $items = $this->topic_model()->get_items3(
            ['class' => "reisethemen", "order_by" => 'title,sort', 'parent_id' => $parent_id],
            $topic_select
        );
        return $items;
    }

    public function tree_cities($country_location_id) {
//        $parent_item = $this->location_model()->get_item_by_id($country_location_id);
        $location_select = "location.id, title as text, 'location|city' as object_type, 'fa fa-map-marker' as icon";
        # get childs
        if (1 || $child_type == 'location') {
            $hierarchy_table = $this->location_model()->table2;
            $table = $this->location_model()->table;
            $this->location_model()->db()
                ->select($location_select)
                ->order_by("title")
                ->where('type', "city")
                ->where('location2_id', $country_location_id)
                ->where('relation_type', 'is_in')
                ->join($hierarchy_table, "$hierarchy_table.location_id = $table.id");
            $items = $this->location_model()->get_items3([
//            'type' => "country"
            ], "location.*, title as text");
        }
        return $items;
    }

    public function prepare_packages(&$package_items) {
        if (!empty($package_items) && count($package_items) > 0)
            foreach ($package_items as $item) {
                $item->id = 'package/edit/' . $item->id;
                $item->text = $item->package_number . " " . $item->title;
                $item->object_type = 'Pack';
                $item->icon = "fa fa-shopping-bag";
            }
    }

    public function packages_by_hotel($object_id) {
        $hotel_id = explode('|', $object_id)[3];
        $items = ci()->package_model()->get_items_by_knot2($hotel_id, 'hotel');
        $this->prepare_packages($items);
        return $items;
    }

    public function packages_by_topic($object_id) {
        $topic_id = explode('|', $object_id)[3];
        $items = ci()->package_model()->get_package_items_sorted_by_score_to_object3("topic", $topic_id);
        $this->prepare_packages($items);
//        }
        return $items;
    }

    public function hotel_by_city($object_id) {
        $hotel_select = "hotel.id as id, hotel.name as text, 'hotel' as object_type, 'fa fa-bed' as icon";
        $items = ci()->hotel_model()->get_hotel_by_location($object_id, ['select' => $hotel_select]);
        foreach ($items as $item)
            $item->id = "hotel/open/" . $item->id;
        return $items;
    }

    public function content_by_city($object_id) {
        $location_id = explode("|", $object_id)[3];
        $dogtag = ci()->dogtag_model()->get_items_by_knot2($location_id, "location")->first();
        if (@$dogtag) {
//            ci()->topic_model()->db()->where('page_linked', 1);
            $topic = ci()->topic_model()->get_items_by_knot2($dogtag->id, "dogtag", ['where' => ['page_linked' => 1]])->first();
            if ($topic) {
                ci()->content_model()->db()->order_by("title");
                $contents = ci()->content_model()->get_items_by_knot2($topic->id, "topic");
            } else { # fallback to region
                $location1 = ci()->location_model()->get_item_by_id($location_id);
                $locations2 = ci()->location_model()->get_items_by_fields(['type' => "region", 'title' => $location1->title]);
                if ($locations2) {
                    $location2 = $locations2[0];
                    $dogtag = ci()->dogtag_model()->get_items_by_knot2($location2->id, "location")->first();
                    ci()->topic_model()->db()->where('page_linked', 1);
                    $topic = ci()->topic_model()->get_items_by_knot2($dogtag->id, "dogtag", ['where' => ['page_linked' => 1]])->first();
                    if ($topic) {
                        ci()->content_model()->db()->order_by("title");
                        $contents = ci()->content_model()->get_items_by_knot2($topic->id, "topic");
                    }
                }
            }
            if (count(@$contents) > 0) {
                foreach ($contents as $content) {
                    $content->id = 'element/edit/' . $content->id;
                    $content->text = $content->title;
                    $content->object_type = 'content|edit';
                    $content->icon = "fa fa-newspaper-o";
                }
                return $contents;
            }
        }
        return new Collection();
    }
}