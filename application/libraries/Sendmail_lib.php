<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Sendmail_lib
{
    /**
     * Sendmail_lib constructor.
     */
    public function __construct() {
        ci()->load->library(array('email'));
    }

    /**
     * Sendet E-Mail vom angegebenen Typ
     *
     * @param  $mail_type
     * @param  $object_id
     * @param  $email_address
     * @param  $old_data
     * @param  $new_data
     */
    public function sendMail($mail_type, $object_id, $email_address, $old_data = null, $new_data = null) {

        $mail_config['mailtype'] = 'html';
        $mail_config['protocol'] = 'smtp';
        $mail_config['smtp_host'] = 'mail.spar-mit.com';
        $mail_config['smtp_user'] = 'terrine@spar-mit.com';
        $mail_config['smtp_pass'] = 'ImAp,,1234%';
        $mailData = array();

        if (ci()->config->item('server_name') != "localhost") {
            if ($mail_type == 'test') {
                $mail_subject = 'Testmail von Spar mit! Reisen';
                $mailData['header'] = 'Testmail';
                $mailData['content'] = 'Dies ist eine Testmail';
                $mail_message = ci()->get_view('mail/mail_view', array('mailData' => $mailData));
                ci()->email->initialize($mail_config);
                ci()->email->from('kontakt@spar-mit.com', 'Spar mit! Reisen');
                ci()->email->to('marcoheine@me.com');
                ci()->email->to('m.heine@spar-mit.com');
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($mail_type == 'remark') {
                $mail_config['mailtype'] = 'text';
                $mail_subject = 'CMS: Wiedervorlage heute fällig';
                $remark_item = ci()->remark_model()->get_item_by_id($object_id);
                if ($remark_item->object_type != "element") {
                    $type = $remark_item->object_type;
                    $id = $remark_item->object_id;
                } else {
                    $content_element_knot = ci()->content_element_knots_model()->get_items_by_field('element_id', $remark_item->object_id)[0];
                    $type = 'content';
                    $id = $content_element_knot->content_id;
                }

                if ($type != "reiseunterlagen")
                    $object_link = "https://cms.spar-mit.com/cms/$type/view/$id";
                else
                    $object_link = "https://cms.spar-mit.com/tools/pdf_creator/edit/$id";

                $mail_message = ci()->get_view('mail/remark_mail', array('remarkText' => $remark_item->text, 'object_link' => $object_link));
                ci()->email->initialize($mail_config);
                ci()->email->from('kontakt@spar-mit.com', 'CMS - Spar mit! Reisen');
                ci()->email->to($email_address, 'CMS - Spar mit! Reisen');
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($mail_type == 'price_change' || $mail_type == 'leadimage_change' || $mail_type == 'hotel_name_change' || $mail_type == 'leistungen_change') {
                $package_item = ci()->package_model()->get_item_by_id($object_id);
                $data['package_item'] = $package_item;
                $data['old_data'] = $old_data;
                $data['new_data'] = $new_data;

                if ($mail_type == 'price_change')
                    $subject = "PREISÄNDERUNG";
                else if ($mail_type == 'leadimage_change')
                    $subject = "FÜHRUNGSBILD-ÄNDERUNG";
                else if ($mail_type == 'hotel_name_change')
                    $subject = "HOTELNAMEN-ÄNDERUNG";
                else if ($mail_type == 'leistungen_change')
                    $subject = "PAKET-LEISTUNGEN-ÄNDERUNG";

                $data['subject'] = $subject;

                $mail_config['mailtype'] = 'html';

                if ($mail_type == 'hotel_name_change')
                    $mail_subject = $subject . ': "' . $old_data . '" wird zu "' . $new_data . '"';
                else
                    $mail_subject = $subject . ': Paket ' . $package_item->package_number . ' | ' . $package_item->name . ' | ' . $package_item->title;

                $mail_message = ci()->get_view('mail/package_change_mail', $data);
                ci()->email->initialize($mail_config);
                ci()->email->from('webundprint@spar-mit.com', 'webundprint@spar-mit.com');
                foreach ($email_address as $email) {
                    $addresses[] = $email['mail'];
                }
                ci()->email->to(implode(', ', $addresses));
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($mail_type == 'topic_publish_control') {
                $mail_subject = 'Topic-Control-Bericht ' . date("d.m.Y");
                $mail_config['mailtype'] = 'html';
                $mailData = $old_data;

                $mail_message = ci()->get_view('tools/topics_control_tool_view', $mailData);
                ci()->email->initialize($mail_config);
                ci()->email->from('webundprint@spar-mit.com', 'webundprint@spar-mit.com');
                ci()->email->to($email_address);
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($mail_type == 'extension_reminder') {
                $package_item = ci()->package_model()->get_item_by_id($object_id);
                $hotel_item = ci()->hotel_model()->get_items_by_knot2($package_item->id, 'package')->first();
                $producer_item = $email_address;
                $email_address = $producer_item->username . '@spar-mit.com';

                $mail_subject = 'Paket ' . $package_item->package_number . ' läuft in 30 Tagen aus!';
                $mail_config['mailtype'] = 'html';

                $mail_message = ci()->get_view('mail/package_extension_reminder_mail', ['package_item' => $package_item, 'producer_item' => $producer_item, 'hotel_item' => $hotel_item, 'expire_date' => $old_data]);
                ci()->email->initialize($mail_config);
                ci()->email->from('webundprint@spar-mit.com', 'webundprint@spar-mit.com');
                ci()->email->to($email_address);
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($mail_type == 'booking_payment_percent_change') {
                $email_address = $email_address . '@spar-mit.com';

                $mail_subject = "Zahlungsmodalidäten für Paket $object_id geändert";
                $mail_config['mailtype'] = 'html';

                $database_names = ['all', 'only_full', 'only_part'];
                $names_for_user = ['Teil- und Komplettzahlung möglich', 'nur Komplettzahlung möglich', 'nur Teilzahlung möglich'];

                if (!$old_data)
                    $old_data = "all";

                $old_data = str_replace($database_names, $names_for_user, $old_data);
                $new_data = str_replace($database_names, $names_for_user, $new_data);
                $mail_message = ci()->get_view('mail/package_payment_percent_change_mail', ['package_number' => $object_id, 'old_data' => $old_data, 'new_data' => $new_data]);

                ci()->email->initialize($mail_config);
                ci()->email->from('buchhaltung@spar-mit.com', 'buchhaltung@spar-mit.com');
                ci()->email->to([$email_address, 'd.franke@spar-mit.com']);
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            }
        }
    }

    public function send_standard_mail($text, $subject, $mail_to, $mail_from = 'kontakt@spar-mit.com') {
            $mail_config['mailtype'] = 'text';
            $mail_config['protocol'] = 'smtp';
            $mail_config['smtp_host'] = 'mail.spar-mit.com';
            $mail_config['smtp_user'] = 'terrine@spar-mit.com';
            $mail_config['smtp_pass'] = 'ImAp,,1234%';

        ci()->email->initialize($mail_config);
        ci()->email->from($mail_from);
        ci()->email->to([$mail_to]);
        ci()->email->subject($subject);
        ci()->email->message($text);
        ci()->email->send();
    }
}
