<?php

class Pdf_lib extends Super_lib
{

    public function create_package_pdf($package_item) {
        $source_path = "https://cms.spar-mit.com/tools/pdf_creator/infoblatt_pdf/$package_item->package_number?login=cronjob";
        exec("xvfb-run wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm  $source_path /var/www/vhosts/cms.spar-mit.com/httpdocs/pdfs/infoblaetter/infoblatt_$package_item->package_number.pdf 2>&1", $output);
        $name = "infoblatt_$package_item->package_number.pdf";
        redirect(site_url('pdfs/infoblaetter/' . $name));
    }

    public function get_data_for_pdf($package_item) {

        ci()->pdf_editor_model()->db()->where('package_id', $package_item->id);
        $edited_pdf_data = ci()->pdf_editor_model()->get_items3();

        foreach ($edited_pdf_data as $edited_data) {
            if ($edited_data->field_type == "inklusivleistung")
                $inklusivleistungen[] = $edited_data;
            else if ($edited_data->field_type == "hotel_service")
                $hotel_service[] = $edited_data;
            else if ($edited_data->field_type == "nebenkosten")
                $nebenkosten[] = $edited_data;
            else if ($edited_data->field_type == "hotel_picture")
                ${$edited_data->field_type."_".$edited_data->sort} = $edited_data;
            else
                ${$edited_data->field_type} = $edited_data;
        }

        $data['margin_text_block'] = @$margin_text_block->text;
        $data['picture_layout'] = @$picture_layout->text;

        $package_data = ci()->api_model()->fill_package(json_decode(json_encode($package_item), true));
        $data['hotel_name'] = $package_data['hotel'][0]['name'];
        $stars = substr($package_data['hotel'][0]['stars'][0]['value'], 0, 1);

        $data['stars'] = (@$package_data['hotel'][0]['stars'][0]['text']) ? $stars . '*' . $package_data['hotel'][0]['stars'][0]['text'] : $stars . '*';

        $dogtags = ci()->dogtag_model()->get_items_by_package($package_item->id, "package+hotel");
        foreach ($dogtags as $dogtag) {
            if ($dogtag->subclass == 'country') {
                $location = ci()->location_model()->get_items_by_knot2($dogtag->id, 'dogtag')->first();
                $data['country'] = @$location->title;
            } else if ($dogtag->subclass == 'city') {
                $location = ci()->location_model()->get_items_by_knot2($dogtag->id, 'dogtag')->first();
                $data['city'] = @$location->title;
            } else if ($dogtag->subclass == 'region') {
                $location = ci()->location_model()->get_items_by_knot2($dogtag->id, 'dogtag')->first();
                if (@$location->title)
                    $data['region'] = @$location->title;
            }
        }

        if (!@$main_picture) {
            $gallery = ci()->gallery_model()->get_items_by_knot2($package_item->id, 'package')->first();
            $data['main_picture'] = @ci()->picture_model()->get_items_by_knot2($gallery->id, 'gallery')[0];
        } else {
            $data['main_picture'] = ci()->picture_model()->get_item_by_id($main_picture->text);
            $data['main_picture']->crop = $main_picture->crop;
        }

        $contents = ci()->content_model()->get_items_by_knot2($package_item->id, 'package');
        foreach ($contents as $content) {
            if ($content->content_type_key == 'pricedate' || ($content->content_type_key == 'leistungenshort' && !@$inklusivleistungen)) {
                $elements = ci()->element_model()->get_items_by_knot2($content->id, 'content');
                foreach ($elements as $element) {
                    if ($content->content_type_key == 'pricedate') {
                        $keywords = ['Parkplatz', 'Parkplätze', 'Kurtaxe'];
                        $contains_keyword = false;
                        foreach ($keywords as $keyword) {
                            if (strpos($element->text, $keyword) !== false) {
                                $contains_keyword = true;
                            }
                        }
                        if ($contains_keyword) {
                            $data['nebenkosten'][] = $element->text;
                        }
                    } else if ($content->content_type_key == 'leistungenshort') {
                        if ($element->desktop == 1 && !@$data['inklusivleistungen']) {
                            $data['inklusivleistungen'] = $element->text;
                        }
                    }
                }
            }
        }

        if (@$inklusivleistungen) {
            $data['inklusivleistungen'] = "<ul>";
            foreach ($inklusivleistungen as $inklusivleistung) {
                $data['inklusivleistungen'] .= $inklusivleistung->text;
            }
            $data['inklusivleistungen'] .= "</ul>";
        }

        if (@$nebenkosten) {
            $data['nebenkosten'] = "<table><tbody>";
            foreach ($nebenkosten as $nebenkost) {
                $data['nebenkosten'] .= $nebenkost->text;
            }
            $data['nebenkosten'] .= "</tbody></table>";
        }

        $hotel = ci()->hotel_model()->get_items_by_knot2($package_item->id, 'package')->first();

        $data['hotel_location'] = ci()->location_model()->get_items_by_knot2($hotel->id, 'hotel')->first();
        $data['hotel_phone'] = @ci()->object_attribute_model()->get_items_by_fields(['object_type' => 'hotel', 'object_id' => $hotel->id, 'attribute_type' => 'customerphone'])[0]->text;

        $hotel_content = ci()->content_model()->get_items_by_knot2($hotel->id, 'hotel');
        foreach ($hotel_content as $content) {
            if ($content->content_type_key == "hotelkompakt") {
                ci()->element_model()->db()->where('element_type.key', 'gallery');
                $gallery_element = ci()->element_model()->get_items_by_knot2($content->id, 'content')->first();
                if ($gallery_element) {
                    $hotel_gallery = ci()->gallery_model()->get_items_by_knot2(@$gallery_element->id, 'element')->first();
                    $hotel_pictures = ci()->picture_model()->get_items_by_knot2(@$hotel_gallery->id, 'gallery');
                    $i = 0;
                    foreach ($hotel_pictures as $key => $picture) {
                        if (@!${"hotel_picture_" . $i})
                            $data['hotel_picture_' . $i] = $picture;
                        else {
                            $data['hotel_picture_' . $i] = ci()->picture_model()->get_item_by_id(${"hotel_picture_" . $i}->text);
                            $data['hotel_picture_' . $i]->crop = ${"hotel_picture_" . $i}->crop;
                        }
                        $i = $i + 10;
                    }
                }

                ci()->element_model()->db()->where_in('element_type.key', ['text', 'text_ck']);
                $elements = ci()->element_model()->get_items_by_knot2($content->id, 'content');

                $keywords = ['Anreise', 'Abreise', 'Parkplatz', 'Parkplätze', 'Restaurant', 'Bar', 'Hotelbar', 'Terrasse', 'Sauna'];
                foreach ($elements as $element) {
                    $contains_keyword = false;
                    foreach ($keywords as $keyword) {
                        if (strpos($element->text, $keyword) !== false) {
                            $contains_keyword = true;
                        }
                    }
                    if ($contains_keyword) {
                        $data['hotel_kompakt_elements'][] = $element;
                    }
                }
            }
        }

        if (@$hotel_service) {
            $data['hotel_kompakt_elements'] = [];
            foreach ($hotel_service as $hotel_s) {
                $data['hotel_kompakt_elements'][] = $hotel_s;
            }
        }

        return $data;
    }
}
?>

