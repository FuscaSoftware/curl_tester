<?php
/**
 * User: sbraun
 * Date: 2019-04-28
 * Time: 14:26
 */

/**
 * Class ProductOrderConfig Wrapper Class
 */
class ProductOrderConfig extends ProductOrderConfig_01
{

}

abstract class ProductOrderConfig_01
{
    public function __construct(array $product_order_config)
    {
        if (count($product_order_config) < 1) {
            throw new Exception("No Data to load Product-Config.");
        } else if (count($product_order_config == 1)) {
            if ($product_order_config['product_id'] || $product_order_config['package_id'])
                $this->config_item = ci()->product_order_config_model()->get_item_by_id($product_order_config['product_id'] ?? $product_order_config['package_id']);
            elseif ($product_order_config['package_number'])
                $this->config_item = ci()->product_order_config_model()->get_item_by("package_number", $product_order_config['package_number']);
        }
    }
}
