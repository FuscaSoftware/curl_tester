<?php
/**
 * User: sbraun
 * Date: 11.07.18
 * Time: 14:22
 */
setlocale(LC_TIME, 'de_DE');
ini_set('date.timezone', 'Europe/Berlin');
date_default_timezone_set('Europe/Berlin');

use de\fusca\hazelnut\gui\components;

class Booking_lib
{
    /** @var DateTime $anreise */
    private $anreise;
    /** @var DateTime $abreise */
    private $abreise;
    /** @var string $choosen_arrangement_key */
    private $choosen_arrangement_key;
    /** @var array $booking_config */
    public $booking_config;
    /** @var array $validation_error */
    public $validation_error = [];
    /** @var bool $debug_mode */
    public $debug_mode = false;

    public function __construct() {
//        date_default_timezone_set('UTC');
        $this->timezone = new DateTimeZone('Europe/Berlin');
//        $this->read_config();
        if (isset($_GET['debug']))
            $_SESSION['debug_mode'] = (int)$_GET['debug'];
        if (isset($_SESSION['debug_mode']))
            $this->debug_mode = $_SESSION['debug_mode'];
        if (isset($_GET['kill_session']) && $_GET['kill_session']) {
            unset($_SESSION['BookingLib']);
            header("Location: " . $_SERVER['SCRIPT_NAME']);
            die;
        }
    }

    public function set_date($anreise = '', $abreise = '') {
        $this->anreise = new DateTime($anreise, $this->timezone);
        $this->abreise = new DateTime($abreise, $this->timezone);
        if (is_a($this->anreise, DateTime::class) && is_a($this->abreise, DateTime::class))
            return true;
        else
            return false;
    }

    public function get_dates($format = 'object') {
        $nights = $this->get_night_count();
        $one_day_interval = new DateInterval('P1D');
        $one_day_interval->d = 1;
        if ($nights > 0) {
            $dates = [];
            $dates[] = $obj = clone $this->anreise;
            for ($i = 1; $i <= $nights; $i++) {
                $obj = clone $obj;
                $dates[] = $obj->add($one_day_interval);
            }
        } else {
            $dates = [
                $this->anreise,
                $this->abreise,
            ];
        }
        if ($format == 'array') {
            $arr = [];
            foreach ($dates as $date) {
                $arr[] = $date->format('Y-m-d');
            }
            return $arr;
        }
        return $dates;
    }

    /**
     * @param array  $times  [from => 2018-01-01, to => 2018-01-13]
     *
     * @param string $format 'array'|'object'
     *
     * @return array
     * @throws Exception
     */
    public function get_dates_for($times, $format = 'array') {
        $r = null;
//        var_dump([66 => $times]);
        if (is_array($times) && is_array(@$times[0]) && key_exists('from', $times[0]) && key_exists('to', $times[0])) {
            $dates = [];
            foreach ($times as $time_span) {
                $from = new DateTime($time_span['from'], $this->timezone);
                $to = new DateTime($time_span['to'], $this->timezone);
                $diff = $from->diff($to);
                $nights = (int)$diff->format('%a');
                $one_day_interval = new DateInterval('P1D');
                $one_day_interval->d = 1;
                if ($nights > 0) {
                    $dates[] = $obj = clone $from;
                    for ($i = 1; $i <= $nights; $i++) {
                        $obj = clone $obj;
                        $dates[] = $obj->add($one_day_interval);
                    }
                }
            }
        } elseif (is_array($times)) {
            $dates = [];
            foreach ($times as $date) {
                $obj = new DateTime($date, $this->timezone);
                $dates[] = $obj;
            }
        }
        if ($dates && $format == 'array')
            foreach ($dates as $date)
                $r[] = $date->format('Y-m-d');
        else
            $r = $dates;
        return $r;
    }

    public function set_arrangement($choosen_arrangement_key) {
        $this->choosen_arrangement_key = $choosen_arrangement_key;
    }


    public function read_config() {
        include dirname(__FILE__, 2) . "/_config/booking_config.php";
        $this->booking_config = $booking_config;
    }

    /**
     * step 1 finish
     */
    public function check_product() {
        if ($booking_data = @$_SESSION['BookingLib']) {
            if (!@$booking_data['persons'])
                $this->validation_error[] = 'persons';
            if (!@$booking_data['anreise'])
                $this->validation_error[] = 'anreise';
            if (!@$booking_data['abreise'])
                $this->validation_error[] = 'abreise';
            if (!@$booking_data['arrangement'])
                $this->validation_error[] = 'arrangement';
            if (!@$booking_data['rooms'])
                $this->validation_error[] = 'rooms';
            if (@$booking_data['childs'])
                foreach (@$booking_data['child'] as $k => $child_age)
                    if (!$child_age)
                        $this->validation_error[] = 'field_child_' . ($k + 1);
            if (empty($this->validation_error))
                return true;
        }
        return false;
    }

    public function check_contact() {
        if ($booking_data = @$_SESSION['BookingLib']) {
            if (!@$booking_data['contact']['vorname'])
                $this->validation_error[] = 'contact_vorname';
            if (!@$booking_data['contact']['nachname'])
                $this->validation_error[] = 'contact_nachname';
            if (!@$booking_data['contact']['strasse'])
                $this->validation_error[] = 'contact_strasse';
            if (!@$booking_data['contact']['plz'])
                $this->validation_error[] = 'contact_plz';
            if (!@$booking_data['contact']['ort'])
                $this->validation_error[] = 'contact_ort';
            if (!@$booking_data['contact']['land'])
                $this->validation_error[] = 'contact_land';
            if (!@$booking_data['contact']['email'])
                $this->validation_error[] = 'contact_email';
            if (!@$booking_data['contact']['telefon'])
                $this->validation_error[] = 'contact_telefon';
            if (empty($this->validation_error))
                return true;
        }
        return false;
    }

    public function check_order() {
        if ($booking_data = @$_SESSION['BookingLib']) {
            if (@$booking_data['confirm']['agb'])
                return true;
            else
                $this->validation_error[] = 'confirm_agb';
        }
        return false;
    }

    /**
     * @return int
     */
    public function get_night_count(): int {
        if (!is_null($this->anreise) && !is_null($this->abreise)) {
            $diff = $this->abreise->diff($this->anreise);
            return (int)$diff->format('%a');
        }
        return null;
    }

    public function get_weekend_adds($persons = 1) {
        $booking_dates = $this->get_dates();
        # remove day of abreise
        $booking_dates = array_slice($booking_dates, 0, count($booking_dates) - 1, true);
//        var_dump($dates);
//        die;
        $add_days = [];
        $costs = [];
        foreach ($booking_dates as $date) {
            $dayOfWeek = $date->format('w');
            if (in_array($dayOfWeek, [5, 6])) {# 5 and 6 Fr & Sa
                $add_days[] = [$date, $dayOfWeek, $date->format('D')];
                $costs[] = $this->booking_config['weekend_add_price']['price'];
            }
        }
//        return ['days' => $add_days, 'price' => array_sum($costs)];
        $nights = count($add_days);
        $label = $nights . (($nights > 1) ? " x " : " x ") . $this->booking_config['weekend_add_price']['title'];
        $price = array_sum($costs) * $persons;
        $return = ($nights > 0) ? [
            'label' => $label,
            'price' => $price,
            'subtext' => "(EUR $costs[0] pro Person / Tag)",
        ] : ['price' => 0,];
//        return ['days' => $add_days, 'price' => array_sum($costs)];
        return $return;
    }

    public function get_saison_adds($persons = 1) {
        $booking_dates = $this->get_dates('array');
        $booking_dates_without_last_day = array_slice($booking_dates, 0, count($booking_dates) - 1, true);
        $saison_dates = $this->get_saisons_by_date($booking_dates_without_last_day);


//        var_dump($booking_dates);
//        var_dump($saison_dates);
        $add_days = [];
        $saison_add_prices = [];
        foreach ($saison_dates as $day => $saisons_for_day) {
            foreach ($saisons_for_day as $k => $saison_info) {
                $saison_add_prices[$saison_info['key']][] = $saison_info['price'];
//                $saison_add_prices[$saison_info['key']][$day] = $saison_info['price'];
            }
//            var_dump($saison_dates);
//            var_dump($day, $saison, $saison_keys);
//            var_dump($saison);
//            die;
        }
//        var_dump($saison_add_prices);
//        var_dump($persons);
//        die;
        $return = [];
//        foreach ($saison_add_prices as $saison_key => $saison_add_price_by_season) {
        foreach ($saison_add_prices as $saison_key => $saison_add_price) {
//            foreach ($saison_add_price_by_season as $k => $saison_add_price) {
//            var_dump($saison_key);
//            var_dump($k);
//            var_dump($saison_add_price);
//                var_dump($saison_add_price_by_season);
            $price = array_sum($saison_add_price) * $persons;
            $nights = count($saison_add_prices[$saison_key]);
            $label = $nights . (($nights > 1) ? " Nächte " : " Nacht ") . $this->booking_config['season_add_price'][$saison_key]['title'];
//            var_dump($saison_add_price);
            $return[] = [
                'label' => $label,
                'price' => $price,
                'subtext' => "(EUR $saison_add_price[0] pro Person / Tag)",
            ];
//            }
        }
//        var_dump($return);
//        die;
//        return ['days' => $add_days, 'price' => array_sum($costs)];
        return $return;
    }

    public function get_extra_adds($persons, $nights) {
        $extras = [];
        foreach ($this->booking_config['extras'] as $k => $v) {
            $quantity = @$_SESSION['BookingLib']['extras'][$k];
            if ($quantity && $v['active']) {
                if ($v['per_day'] && $v['per_item']) {
                    $extra = $v;
                    $extra['label'] = "{$quantity} x " . $v['title'];
                    $extra['subtext'] = "(EUR " . $v['price_add'] . " pro Tag)";
                    $extra['price'] = $quantity * $nights * $v['price_add'];
                    $extras[] = $extra;
                }
            }
        }
        return $extras;
    }

    /**
     * @param array $booking_dates
     *
     * @return array
     * @throws Exception
     */
    public function get_saisons_by_date($booking_dates) {
        $arr = [];
        foreach ($this->booking_config['season_add_price'] as $k_saison => $saison) {
            $saison_dates = $this->get_dates_for($saison['times'], 'array');
//            var_dump($saison['times'], $saison_dates);
//            die;
            foreach ($saison_dates as $k => $saison_date) {
//                var_dump([214 => $saison_date, $k_saison, $saison]);
                if ((is_array($booking_dates) && in_array($saison_date, $booking_dates))
                    || empty($booking_dates)) {
                    $saison['key'] = $k_saison;
//                    var_dump($saison_date, $k_saison, $saison);
//                    die;
                    $arr[$saison_date][$k_saison] = $saison;
                }
            }
        }
//        var_dump([
//            218 => $arr,
//            'booking_dates' => $booking_dates,
//        ]);
        return $arr;
    }


    public function get_arrangement_keys(): array {
        return array_keys($this->booking_config['arrangements']);
    }

    public function get_room_keys(): array {
        return array_keys($this->booking_config['rooms']);
    }

    public function get_arrangement_config(string $chosen_arrangement_key): stdClass {
        $chosen_arrangement_config = $this->booking_config['arrangements'][$chosen_arrangement_key];
        $arrangement_parent = @$chosen_arrangement_config['extends'];
        if ($arrangement_parent) {
            $chosen_arrangement_config = array_merge(
                $this->booking_config['arrangements'][$arrangement_parent],
                $chosen_arrangement_config
            );
        }
        $chosen_arrangement_config['key'] = $chosen_arrangement_key;
        $chosen_arrangement_config['price_prefix'] = (@$chosen_arrangement_config['skip_season_add_price']) ? 'nur' : "ab";
        return (object)$chosen_arrangement_config;
    }

    public function get_room_config(string $room_key): stdClass {
        $chosen_arrangement_config = $this->booking_config['rooms'][$room_key];
        $arrangement_parent = @$chosen_arrangement_config['extends'];
        if ($arrangement_parent) {
            $chosen_arrangement_config = array_merge(
                $this->booking_config['rooms'][$arrangement_parent],
                $chosen_arrangement_config
            );
        }
        $chosen_arrangement_config['key'] = $room_key;
        $chosen_arrangement_config['price_prefix'] = (@$chosen_arrangement_config['skip_season_add_price']) ? 'nur' : "ab";
        return (object)$chosen_arrangement_config;
    }

    public function booking_result() {
        $booking = $this;
        $rooms = [];
        if (is_array($_SESSION['BookingLib']['rooms']))
            foreach ($_SESSION['BookingLib']['rooms'] as $room_key => $booked_rooms) {
                if ($booked_rooms > 0) {
                    $room = $this->get_room_config($room_key);
                    $room->quantity = $booked_rooms;
                    $rooms_text[] = $room->text = $booked_rooms . "x " . $room->title;
                    $rooms[] = $room;
                }
            }
        else {
            $room = $this->get_room_config($_SESSION['BookingLib']['rooms']);
            $room->quantity = 1;
            $rooms_text[] = $room->text = $room->title;
            $rooms[] = $room;
        }

        $arrangement = $this->get_arrangement_config($_SESSION['BookingLib']['arrangement']);

        if (@$_SESSION['BookingLib']['childs']) {
            $childs = ['a' => 0, 'b' => 0, 'c' => 0];
            foreach ($_SESSION['BookingLib']['child'] as $child) {
                if ($child >= $this->booking_config['children']['a']['from'] && $child <= $this->booking_config['children']['a']['to'])
                    $childs['a']++;
                if ($child >= $this->booking_config['children']['b']['from'] && $child <= $this->booking_config['children']['b']['to'])
                    $childs['b']++;
                if ($child >= $this->booking_config['children']['c']['from'] && $child <= $this->booking_config['children']['c']['to'])
                    $childs['c']++;
                $plural_singular = ($child > 1) ? "Jahre" : "Jahr";
                $child_texts[] = "1 Kind " . $child . " " . $plural_singular;
            }
        }
//        foreach ($childs as $k => $child_group) {
//            $child_texts[] = "$child_group " . (($child_group > 1)? "Kinder" : "Kind") . "  ";
//        }

        $adults = $_SESSION['BookingLib']['persons'];
        $persons_float = array_sum([
            1 * $adults,
            (@$childs['a']) ? $childs['a'] * (100 + $this->booking_config['children']['a']['price_factor']) / 100 : 0,
            (@$childs['b']) ? $childs['b'] * (100 + $this->booking_config['children']['b']['price_factor']) / 100 : 0,
            (@$childs['c']) ? $childs['c'] * (100 + $this->booking_config['children']['c']['price_factor']) / 100 : 0,
        ]);
//        var_dump($persons_float);

        $data1 = [
            'arrangement' => $arrangement,
            'rooms' => $rooms,
            'rooms_text' => $rooms_text,
            'nights' => $booking->get_night_count(),
            'adults' => $adults,
            'childs' => @$childs,
            'child_texts' => @$child_texts,
            'dates' => $booking->get_dates('array'),
            'weekend_adds' => (!@$arrangement->skip_weekend_add_price) ? $booking->get_weekend_adds($persons_float) : ['price' => 0],
            'saison_adds' => (!@$arrangement->skip_season_prices) ? $booking->get_saison_adds($persons_float) : [],
            'anreise_lang' => strftime('%A, %e. %B %Y', $booking->anreise->getTimestamp()),
            'abreise_lang' => strftime('%A, %e. %B %Y', $booking->abreise->getTimestamp()),
//            'dates_info' => [
//                $booking->anreise->getOffset(),
////                $booking->abreise->getOffset(),
//                $booking->anreise->getTimezone()->getName(),
//                $booking->anreise->format('Y-m-d H:i:s'),
//                strftime('%A, %e. %B %Y %H:%M', $booking->anreise->getTimestamp()),
//                strftime('%A, %e. %B %Y %H:%M', $booking->anreise->getTimestamp() + $booking->abreise->getOffset()),
//                strftime('%A, %e. %B %Y %H:%M', time()),
//                strftime('%z, %Z', time()),
//                (time()),
//            ],
        ];

        $data1['extra_adds'] = $booking->get_extra_adds($data1['adults'], $data1['nights']);
        $arrangement_price_pp = $arrangement->price / 1;
        $arrangement_price = $arrangement_price_pp * $data1['adults'];
        $extra_nights = max($data1['nights'] - $arrangement->duration, 0);
        $extra_night_price_pp = $extra_nights * ($this->booking_config['additional_night'] + $rooms[0]->add_price);
        $data2 = [
            # arrangement
            $arrangement_price,
            ($data1['adults'] % 2) ? ($arrangement_price_pp * 2 * $rooms[0]->single_room_factor / 100) - $arrangement_price_pp : 0,
            (@$childs['a']) ? $arrangement_price_pp * $childs['a'] * (100 + $this->booking_config['children']['a']['price_factor']) / 100 : 0,
            (@$childs['b']) ? $arrangement_price_pp * $childs['b'] * (100 + $this->booking_config['children']['b']['price_factor']) / 100 : 0,
            (@$childs['c']) ? $arrangement_price_pp * $childs['c'] * (100 + $this->booking_config['children']['c']['price_factor']) / 100 : 0,
            # roomprice
            $rooms[0]->add_price * $persons_float * ($arrangement->duration),
            # extra_nights
            $data1['adults'] * $extra_night_price_pp,
            ($data1['adults'] % 2) ? ($extra_night_price_pp * 2 * $rooms[0]->single_room_factor / 100) - $extra_night_price_pp : 0,
            (@$childs['a']) ? $extra_night_price_pp * $childs['a'] * (100 + $this->booking_config['children']['a']['price_factor']) / 100 : 0,
            (@$childs['b']) ? $extra_night_price_pp * $childs['b'] * (100 + $this->booking_config['children']['b']['price_factor']) / 100 : 0,
            (@$childs['c']) ? $extra_night_price_pp * $childs['c'] * (100 + $this->booking_config['children']['c']['price_factor']) / 100 : 0,
        ];

//        if ($arrangement->skip_weekend_add_price)
//            unset($data1['weekend_adds']);

//        if ($arrangement->skip_season_prices)
//            unset($data1['saison_adds']);

        foreach (@$data1['saison_adds'] as $saison_add)
            $saison_sum_part[] = $saison_add['price'];
//        foreach ($data1['weekend_adds'] as $weekend_add)
        $weekend_add_part[] = $data1['weekend_adds']['price'];
        foreach ($data1['extra_adds'] as $extra_add)
            $extra_add_part[] = $extra_add['price'];

        $data3 = [
            array_sum($data2),
            array_sum(@$saison_sum_part),
            array_sum(@$weekend_add_part),
            (@$extra_add_part) ? array_sum(@$extra_add_part) : 0,
        ];
        $final_sum = array_sum($data3);
        # calculation finished!
        # prepare output
        $data1['total_price'] = number_format($final_sum, 2, ',', '.');

        return [$data1, $data2, $data3, $final_sum];
    }

    public function send_order($booking_result) {
        if ($booking_data = @$_SESSION['BookingLib']) {

            include_once __DIR__ . '/Helper.php';
            $helper = new Helper();

            $to = "info@fini-resort-badenweiler.de, post@prinke.de, m.finck@spar-mit.com";
            $client_mail = $booking_data['contact']['email'];

            $mail_data = array_merge($booking_result[0], [
                'booking_result' => $booking_result,
                'booking_contact' => $booking_data['contact']
            ]);

            if (1) {
                $subject = "Neue Online-Buchung - {$booking_data['contact']['nachname']} - {$booking_result[0]['anreise_lang']}";
                $mail_template = "Vorlage-Mail ans Hotel.php";
                #$mail_internal = $this->mail($to, $subject, null, $mail_template, $mail_data);
                $mail_internal = $helper->send_mail($client_mail, ['info@fini-resort-badenweiler.de', 'h.prinke@fini-resort-badenweiler.de', 'm.finck@spar-mit.com', 'marcoheine@me.com'], $subject, $this->parse_template($mail_template, $mail_data));
            }

            if (1) {
                $to = $booking_data['contact']['email'];
                $subject = "Ihre Fini-Resort Badenweiler-Buchung";
                $mail_template = "Vorlage-Mail an den Gast.php";
                #$mail_client = $this->mail($to, $subject, null, $mail_template, $mail_data);
                $mail_client = $helper->send_mail('info@fini-resort-badenweiler.de', $client_mail, $subject, $this->parse_template($mail_template, $mail_data));
            }
        }
        $r = ['mail_internal' => $mail_internal, 'mail_client' => $mail_client];
//        var_dump($r);
//        die;
        return $r;
    }

    public function mail($to, $subject, $from, $mail_template, $mail_data) {
//        $to = 'h.prinke@spar-mit.com';
//        $to = 's.braun@spar-mit.com';
        $headers = [];
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=utf-8";
        if ($from)
            $headers[] = "From: $from";
        else
            $headers[] = "From: Fini-Resort Badenweiler <info@fini-resort-badenweiler.de>";
        $headers[] = "Errors-to: info@fini-resort-badenweiler.de";
//        $header[] = 'Cc: h.prinke@spar-mit.com';
//        $headers[] = 'Bcc: semabra@gmail.com, h.prinke@spar-mit.com, s.braun@spar-mit.com, marcoheine@me.com';
        $headers[] = 'Bcc: semabra@gmail.com, s.braun@spar-mit.com';

//        $body = "Anrede: $anrede\n\nNachname: $nachname\n\nVorname: $vorname\n\nLand: $land\n\nTelefon: $telefon\n\nBemerkungen: $message";
        $mail_body = $this->parse_template($mail_template, $mail_data);

//        echo $mail_body;


// If there are no errors, send the email
        if (1) {
            if ($success = mail($to, $subject, $mail_body, implode("\r\n", $headers))) {
                $error = false;
            } else {
                $error = '<div class="alert alert-danger">Es gab einen Fehler beim Senden der Nachricht. Bitte versuchen Sie es erneut oder rufen uns an, danke!</div>';
            }
        }
        return ['success' => $success, 'error' => $error];
    }

    public function parse_template($template_file, $data) {
        $booking = $this;
        ob_start();
        extract($data);
        include dirname(__FILE__, 2) . '/mails/' . $template_file;
        return ob_get_clean();
    }


    public function calculate_order_sums(array $input_data = []): array {
        $input_data ?: $this->input_data;

        $sql = "select Paket.PaketID, PaketPreis.* from PaketPreis inner join Paket ON Paket.PaketID = PaketPreis.PaketID where PaketNr = \"$this->package_number\"";

        ci()->loader()->model("fake/Zombie_fake_model");
        $saison_prices = ci()->zombie_fake_model()->db()->query($sql)->result_object();

        $sql = "select * from  Artikel where PaketID = \"184751\"";
        $articles = ci()->zombie_fake_model()->db()->query($sql)->result_object();

        # demo
        $package_config = (object)[];
        $package_config->room_type = "apartment";
        $package_config->room_limits = [
            'adults' => 2,
            'children' => 4,
        ];
        $package_config->insurance_price = 24.0;
        $package_config->base_price = 369.0;
        $package_config->base_nights = 5;


        #
        $values = [];
        #
        if ($calendar_string = $input_data['Calendar'] ?? null) {
            if (strlen($calendar_string) === 23)
                $dates = explode(' - ', $calendar_string);
            elseif (strlen($calendar_string) === 21)
                $dates = explode('-', $calendar_string);
            else {
                $dates = false;
                $values['error'] = true;
            }

            if (count($dates) == 2)
                $date_set = $this->set_date($dates[0], $dates[1]);
            else {
                $values['error'] = true;
            }
        } else {
            $values['comment'] = "Bitte wählen Sie zu erst ein Reisedatum!";
            $values['error'] = true;
        }

        if ($package_config->room_type == 'apartment') {
            $values['no_of_fewo'] = $base_price_factor = ($input_data['Erwachsene'] ?? 0) ? (int)round($input_data['Erwachsene'] / $package_config->room_limits['adults']) : 1;
        }
//        $values['nights'] = $this->get_dates_for($times, 'object');
        if ($date_set ?? 0) {
            $values['dates'] = $dates2 = $this->get_dates('array');
            $values['nights'] = ($dates2) ? $this->get_night_count() : null;

            if ($values['nights'] > $package_config->base_nights) {
                $values['error'] = true;
            } else if ($values['nights'] < $package_config->base_nights) {
                $values['error'] = true;
            }
        }


        $values['price'] = array_sum([
            $package_config->base_price * $base_price_factor,
            ($input_data['insurance'] ?? 0) ? $package_config->insurance_price : 0,
        ]);
        return $values;
    }

    public function get_order_sum_cardBody(array $values = null): components\CardBody {
        $values ?: $this->calculate_order_sums();
        ci()->loader()->helper('sb/gui');
        $cardBody = new components\CardBody(function (components\CardBody $cardBody) use ($values) {
            $cardBody->setTitle("Ajax Summe");
            $cardBody->setId("sum");
            $no_of_fewo = $values['no_of_fewo'];
            if ($no_of_fewo)
                $word = ($no_of_fewo > 0)? "Ferienwohnungen" : "Ferienwohnung";
            $amount = ($values['price']) ?? 0;
            if (isset($amount) && isset($word)) {
                $lines = [
                    "$no_of_fewo $word",
                    "$amount €",
                    $values['comment'] ?? null,
                ];
                $cardBody->setText(implode("<br>", $lines));
            }
        });
        return $cardBody;
    }

}