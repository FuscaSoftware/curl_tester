<?php

ci()->load->library('super_lib');

/**
 * Description of Bootstrap_lib
 *
 * @author sebra
 */
class Google_analytics_lib extends Super_lib
{


    public function index()
    {
        require_once 'vendor/autoload.php';

        $analytics = $this->initializeAnalytics();
        $profile = $this->getFirstProfileId($analytics);
        $results = $this->getResults($analytics, $profile);
        $this->printResults($results);
    }

    public function pageviewsPackage($slug)
    {
        require_once 'vendor/autoload.php';

        $analytics = $this->initializeAnalytics();
        $profile = $this->getFirstProfileId($analytics);

        // old Desktop and mobile
        $optParams['filters'] = 'ga:pagePath==/' . $slug .'/,ga:pagePath==/paket/' . $slug;
        $optParams['dimensions'] = 'ga:date';

        $results = $analytics->data_ga->get(
            'ga:' . $profile,
            '30daysAgo',
            'today',
            'ga:uniquePageviews',
            $optParams);

        $rows = $results->getRows();

        foreach ($rows as $row) {
            $data['pageviews30'][] = array($row[1]);
            $row[0] = date_format(date_create($row[0]), 'd.m.Y');
            $data['dates30'][] = array($row[0]);
        }

        $optParams['dimensions'] = 'ga:isoYearIsoWeek';

        $results = $analytics->data_ga->get(
            'ga:' . $profile,
            '365daysAgo',
            'today',
            'ga:uniquePageviews',
            $optParams);

        $rows = $results->getRows();

        foreach ($rows as $row) {
            $data['pageviews365'][] = array($row[1]);
            $year = substr($row[0], 0, 4);
            $week = substr($row[0], 4, 2);
            $data['dates365'][] = array('KW ' . $week . '/' .$year);
        }

        return $data;
    }

    public function pageviewsPackages()
    {
        require_once 'vendor/autoload.php';

        $analytics = $this->initializeAnalytics();
        $profile = $this->getFirstProfileId($analytics);
        // old Desktop and mobile
        $optParams['filters'] = "ga:pagePath=@/paket/";
        $optParams['dimensions'] = 'ga:pagePath';
        $optParams['sort'] = '-ga:uniquePageviews';
        $optParams['max-results'] = 10000;

        $results = $analytics->data_ga->get(
            'ga:' . $profile,
            '90daysAgo',
            'today',
            'ga:uniquePageviews',
            $optParams);

        $analytics_data = $results->getRows();

        $packages = ci()->package_model()->get_items3(['brand' => 'smr']);

        foreach ($packages as $package) {
            $data[$package->id] = 0;
            foreach ($analytics_data as $a_d) {
                if ($a_d[0] == "/paket/$package->name" || strpos($a_d[0], "paket/$package->name?")) {
                    $data[$package->id] = $data[$package->id] + $a_d[1];
                }
            }
        }

        return $data;
    }

    public function pageviewsTopic($slug)
    {
        require_once 'vendor/autoload.php';

        $analytics = $this->initializeAnalytics();

        $profile = $this->getFirstProfileId($analytics);

        $optParams['filters'] = 'ga:pagePath==/' . $slug .',ga:pagePath=@'.@$slug.'?';
        $optParams['dimensions'] = 'ga:date';

        $results = $analytics->data_ga->get(
            'ga:' . $profile,
            '30daysAgo',
            'today',
            'ga:uniquePageviews',
            $optParams);

        $rows = $results->getRows();

        foreach ($rows as $row) {
            $data['pageviews30'][] = array($row[1]);
            $row[0] = date_format(date_create($row[0]), 'd.m.Y');
            $data['dates30'][] = array($row[0]);
        }

        $optParams['dimensions'] = 'ga:isoYearIsoWeek';

        $results = $analytics->data_ga->get(
            'ga:' . $profile,
            '365daysAgo',
            'today',
            'ga:uniquePageviews',
            $optParams);

        $rows = $results->getRows();

        foreach ($rows as $row) {
            $data['pageviews365'][] = array($row[1]);
            $year = substr($row[0], 0, 4);
            $week = substr($row[0], 4, 2);
            $data['dates365'][] = array('KW ' . $week . '/' .$year);
            $weeks[] = $row[0];
        }

        $optParams['dimensions'] = 'ga:isoYearIsoWeek, ga:medium';
        $optParams['segment'] = 'gaid::-5';

        $results = $analytics->data_ga->get(
            'ga:' . $profile,
            '365daysAgo',
            'today',
            'ga:entrances',
            $optParams);

        $rows = $results->getRows();

        foreach ($weeks as $week) {
            $date_found = false;
            foreach ($rows as $row) {
                if ($week == $row[0]) {
                    $date_found = true;
                    $data['organic30'][] = [$row[2]];
                    break;
                }
            }
            if (!$date_found) {
                $data['organic30'][] = ['0'];
            }
        }

        // Keywords von der Search Console
        $search_console = $this->initializeSearchConsole();

        $search = new Google_Service_Webmasters_SearchAnalyticsQueryRequest;
        $search->setStartDate( date('Y-m-d', strtotime('-31 days')) );
        $search->setEndDate( date('Y-m-d', strtotime('-1 day')) );
        $search->setDimensions( ['query'] );
        $search->setAggregationType( 'auto' );
        $search->setRowLimit(20);
        // Set filter
        $filter = new Google_Service_Webmasters_ApiDimensionFilter;
        $filter->setDimension('page');
        $filter->setOperator('equals');
        $filter->setExpression("https://www.spar-mit.com/$slug");
        // Set filter group
        $filterGroup = new Google_Service_Webmasters_ApiDimensionFilterGroup;
        $filterGroup->setFilters(array($filter));
        $filterGroup->setGroupType('and');
        $search->setDimensionFilterGroups(array($filterGroup));

        $data['organic_keywords'] = $search_console->searchanalytics->query( 'https://www.spar-mit.com', $search )->getRows();

        return $data;
    }

    public function get_search_console_data_for_keyword($keyword, $slug, $start_date, $end_date) {

        $keyword = mb_strtolower($keyword);

        // Keywords von der Search Console
        require_once 'vendor/autoload.php';
        $search_console = $this->initializeSearchConsole();

        $search = new Google_Service_Webmasters_SearchAnalyticsQueryRequest;
        $search->setStartDate($start_date);
        $search->setEndDate($end_date);
        $search->setDimensions( ['query'] );
        $search->setAggregationType( 'auto' );
        $search->setRowLimit(1);
        // Set filter
        $filterpage = new Google_Service_Webmasters_ApiDimensionFilter;
        $filterpage->setDimension('page');
        $filterpage->setOperator('equals');
        $filterpage->setExpression("https://www.spar-mit.com/$slug");
        $filterkeyword = new Google_Service_Webmasters_ApiDimensionFilter;
        $filterkeyword->setDimension('query');
        $filterkeyword->setOperator('equals');
        $filterkeyword->setExpression("$keyword");
        // Set filter group
        $filterGroup = new Google_Service_Webmasters_ApiDimensionFilterGroup;
        $filterGroup->setFilters(array($filterpage,$filterkeyword));
        $filterGroup->setGroupType('and');
        $search->setDimensionFilterGroups(array($filterGroup));

        return $search_console->searchanalytics->query( 'https://www.spar-mit.com', $search )->getRows();
    }

    public function initializeAnalytics()
    {
        // Creates and returns the Analytics Reporting service object.

        // Use the developers console and download your service account
        // credentials in JSON format. Place them in this directory or
        // change the key file location if necessary.
        $KEY_FILE_LOCATION = 'assets/google_analytics/SMR-API-d89f6fdfc7a0.json';

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_Analytics($client);

        return $analytics;
    }

    public function initializeSearchConsole()
    {
        // Creates and returns the Analytics Reporting service object.

        // Use the developers console and download your service account
        // credentials in JSON format. Place them in this directory or
        // change the key file location if necessary.
        $KEY_FILE_LOCATION = 'assets/google_analytics/SMR-API-d89f6fdfc7a0.json';

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Hello Search Console Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/webmasters.readonly']);
        $searchconsole = new Google_Service_Webmasters($client);

        return $searchconsole;
    }

    public function getFirstProfileId($analytics) {
        // Get the user's first view (profile) ID.

        // Get the list of accounts for the authorized user.
        $accounts = $analytics->management_accounts->listManagementAccounts();

        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                // Get the list of views (profiles) for the authorized user.
                $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();

                    // Return the first view (profile) ID.
                    return $items[0]->getId();

                } else {
                    throw new Exception('No views (profiles) found for this user.');
                }
            } else {
                throw new Exception('No properties found for this user.');
            }
        } else {
            throw new Exception('No accounts found for this user.');
        }
    }

    public function getResults($analytics, $profileId) {
        // Calls the Core Reporting API and queries for the number of sessions
        // for the last seven days.

        return $analytics->data_ga->get(
            'ga:' . $profileId,
            '7daysAgo',
            'today',
            'ga:sessions');
    }

    public function printResults($results) {
        // Parses the response from the Core Reporting API and prints
        // the profile name and total sessions.
        if (count($results->getRows()) > 0) {

            // Get the profile name.
            $profileName = $results->getProfileInfo()->getProfileName();

            // Get the entry for the first entry in the first row.
            $rows = $results->getRows();
            $sessions = $rows[0][0];

            // Print the results.
            print "First view (profile) found: $profileName\n";
            print "Total sessions: $sessions\n";
        } else {
            print "No results found.\n";
        }
    }

}
