<?php

class Encryption_lib
{
    function encrypt($plaintext, $password) {
        $method = "AES-256-CBC";
        $key = hash('sha256', $password, true);
        $iv = openssl_random_pseudo_bytes(16);

        $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
        $hash = hash_hmac('sha256', $ciphertext . $iv, $key, true);

        return base64_encode($iv . $hash . $ciphertext);
    }

    function decrypt($encryptedText, $password) {
        if (empty($encryptedText))
            return;
        $encryptedText = base64_decode($encryptedText);

        $method = "AES-256-CBC";
        $iv = substr($encryptedText, 0, 16);
        $hash = substr($encryptedText, 16, 32);
        $ciphertext = substr($encryptedText, 48);
        $key = hash('sha256', $password, true);

        if (!hash_equals(hash_hmac('sha256', $ciphertext . $iv, $key, true), $hash)) return null;

        return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
    }
/*
    function sample() {
        $s = "Eine Katze kommt selten allein!";
        $pass = "eumel";
        $crypted = encrypt($s, $pass);
        echo "Encrypted: " . $crypted . "</br>\n";
        #Encrypted: yGGp0OdRa9MyhbKv1Fp6og97dBo1uglwU31+I1hAEz8bdL27U2CRdUKCtNNVcq9qanKbVvCF8g/nLP3Ael6yG9QCoUmag7/Nzvrldg4eLTY=
        $decrypted = decrypt($crypted, $pass);
        echo "Decrypted again: " . $decrypted . "<br>\n";
        $decrypted_false = decrypt($crypted, $pass_false);#with wrong passworod
        if ($decrypted_false == nulL)
            echo "Wrong passphrase!<br>\n";
    }
    */
}