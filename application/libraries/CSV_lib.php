<?php

class Csv_lib
{

    /**
     * Must be greater than the longest line (in characters) to be found in the CSV file (allowing for trailing line-end
     * characters). Otherwise the line is split in chunks of length characters, unless the split would occur inside an
     * enclosure.
     *
     * Omitting this parameter (or setting it to 0 in PHP 5.1.0 and later) the maximum line length is not limited,
     * which is slightly slower.
     */
    public $length = 4096;

    /**
     * The optional delimiter parameter sets the field delimiter (one character only).
     */
    public $delimiter = '|';

    /**
     * The optional enclosure parameter sets the field enclosure character (one character only).
     */
    public $enclosure = '"';

    /**
     * @param string $path_to_file
     * @return array $tmp
     */
    function parse_file($path_to_file)
    {
        $tmp = array();
        if (($handle = fopen($path_to_file, "r")) !== false) {
            while (($row_data = fgetcsv($handle, $this->length, $this->delimiter)) !== false) {
                $tmp[] = $row_data;
            }
            fclose($handle);
        }
        return $tmp;
    }

    public function convert_to_csv($csv_rows, $filename, $delimiter = "|",  $bom = true) {
        if (isset($bom)) {
            $codierung = 'utf-8-bom';
        } else {
            $codierung = 'utf-8';
        }

        // Open the output stream
        $file = fopen('php://output', 'w');

        // Start output buffering (to capture stream contents)
        ob_start();
        foreach ($csv_rows as $csv_row) {
            $keys = array_keys($csv_row);
            fputcsv($file, $keys, $delimiter);
            break;
        }

        foreach ($csv_rows as $csv_row) {
            fputcsv($file, $csv_row, $delimiter);
        }

        // Get the contents of the output buffer
        $string = ob_get_clean();

        header('Content-Encoding: UTF-8');
        header("Content-type: text/csv; charset=UTF-8");
        header("Content-Disposition: attachment; filename=" . $filename . "-" . $codierung . "-" . date('Ymd') . ".csv");
        header("Pragma: no-cache");
        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');

        if (!is_null($bom)) {
            echo "\xEF\xBB\xBF";
        }

        exit($string);
    }
}