<?php

MY_Controller::get_instance()->load->library("super_lib");
MY_Controller::get_instance()->load->library("bootstrap_lib");

/**
 * Description of Bootstrap_lib
 *
 * @author sebra
 */
class Bs4 extends Bootstrap_lib
{
    public function datatable($view_data = []) {
        return ci()->get_view('datatables/table',$view_data);
    }

}

class Bootstrap4_Message_Object extends Bootstrap_Message_Object
{

}
