<?php

MY_Controller::get_instance()->load->library("super_lib");

/**
 * Description of Model_helper_lib
 *
 * @author sebra
 */
class Model_helper_lib extends Super_lib
{

    /**
     * @param object[] $items
     * @param string $key like 'id'
     * @return array e.g.: ['1','2','3']
     */
    public function get_flat_items(array $items, string $key = "id"): array {
        $arr = [];
        if (!empty($items)) {
            if (is_object($items[0])) {
                foreach ($items as $k => $item) {
                    if (property_exists($items[$k], $key))
                        $arr[] = $items[$k]->{$key};
                    else {
                        $this->ci->message(
                            "\$item (" . get_class($item) . ") does not have a property like '$key'. <br/>only: " . implode(", ", array_keys((array)$item)),
                            "danger");
                        break;
                    }
                }
            } elseif (is_array($items[0])) {
                foreach ($items as $k => $item) {
                    if (array_key_exists($key, $items[$k]))
                        $arr[] = $items[$k][$key];
                    else {
                        $this->ci->message(
//                            var_export($item,1).
                            "\$item (" . get_class($item) . ") does not have a property like '$key'. <br/>only: " . implode(", ", array_keys((array)$item)),
                            "danger");
                        break;
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * @param object[] $items
     * @param string $key like 'id'
     * @return string e.g.: '1','2','3' or 0 if $items is empty
     */
    public function get_flat_items_as_csv(array $items, string $key = "id"): string {
        if (!empty($items)) {
            if (is_object($items[0])) {
                $values = $this->get_flat_items($items, $key);
                if (!empty($values))
                    return "'" . implode("','", $values) . "'";
            } elseif (is_array($items[0])) {
                return "'".implode("','", array_column($items, $key))."'";
            }
        } else
            return "0";#have to be 0 and not «'0'» so you can test for empty/bool
    }

    public static function get_tree(array $items, object $root_item = null) {
        $tree_array = array();
        #children for
//        $id = 0;
        if (is_null($root_item))
            $root_item = (object)array(
                "id" => 0,
                'text' => 'Navigation/Seiten/Themen/Kategorien',
                'tree_level' => 0,
                'state' => array('selected' => false, "opened" => true, "disabled" => true),
                "icon" => "jstree-file",
            );

        $sorted_items = array();
        foreach ($items as $item) {
            $sorted_items[$item->parent_id][] = $item;
        }
        self::tree_children_for($sorted_items, $root_item);

//        var_dump($root_item);
//        self::tree_children_for($items, $root_item);
        $tree_array[] = $root_item;
//        die;
        return $tree_array;
    }

    private static function tree_children_for(&$items_by_parent_id, &$parent_item) {
        foreach ($items_by_parent_id as $parent_id => $child_items) {
            if ($parent_item->id == $parent_id) {
                $parent_item->children = $child_items;
                foreach ($parent_item->children as $child_item) {
                    $child_item->tree_level = $parent_item->tree_level + 1;
                    self::tree_children_for($items_by_parent_id, $child_item);
                }
            } elseif (!isset($parent_item->children)) {
                $parent_item->children = false;
            }
        }

//        if (!is_object($parent_item))
//            die(__FILE__.":".__LINE__);
//        for ($i = 0; $i < count($items); $i++){
//            var_dump($parent_item);
//            if ($items[$i]->parent_id == $parent_item->id){
//                $parent_item->children[] = $items[$i];
//            } else
//                self::tree_children_for($items, $items[$i]);
//
////            unset($items[$i]);
//        }
    }

    public function get_keyless_items (array $items, array $keys = []):array {
        $arr = [];
        if (!empty($items)) {
            if (is_object($items[0])) {
                foreach ($items as $k => $item) {
                    if (empty($keys))
                        $arr[] = array_values((array)$item);
                    else {#check for key in keys
                        $arr2 = null;
                        foreach ($keys as $key)
                            $arr2[] = $item->$key;
                        $arr[] = $arr2;
                    }
                }
            } elseif (is_array($items[0])) {
                foreach ($items as $k => $item) {
                    if (empty($keys))
                        $arr[] = array_values((array)$item);
                    else {#check for key in keys
                        $arr2 = null;
                        foreach ($keys as $key)
                            $arr2[] = $item[$key];
                        $arr[] = $arr2;
                    }
                }
            }
        }
        return $arr;
    }

    public function parseSearchString(string $searchString): string {
        $searchString = str_replace("*", "%", $searchString);
        $searchString = str_replace("?", "_", $searchString);
        return addslashes($searchString);
    }
}
