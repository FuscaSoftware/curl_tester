<?php
    $item_info = "ID: ".$item->id."&#10;Erstellt: ".@$item->created."&#10;Bearbeitet: ".@$item->modified."&#10;Bearbeitet von: ".@$item->modifier."";
    $modelname = $box_index['modelname'];
    $icon = Bootstrap_lib::get_object_icon($box_index['modelname']);
    $item_icon = (!empty($icon))? '<i class="fa '.$icon.'"></i>' : "";

?>

<?php #$this->table->add_row($item);
//get_instance()->dump($box_index);
?>

<tr id="<?= $box_index['modelname'] ?>_<?=$item->id?>"
    class="<?= $box_index['modelname'] ?>_<?=$item->id?> <?= ($box_index['active_id'] == $item->id)? "active " : "" ?> row_<?= $box_index['modelname']?> row_<?= $box_index['modelname']."_".$item->id ?>"
    data-sort="<?= @$item->sort ?>"
>
<!--    list_row_view-->
    <? if (@$box_index['sortable']): ?>
        <td class="dragzone text-muted" title="ID: <?=$item->id?>">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </td>
    <? endif; ?>
    <?php if ($list_fields): ?>
        <?php foreach ($list_fields as $field): ?>
            <?php if ($field != "action"): ?>
                <?php if (in_array($field, array("title","name","lastname"))):##TODO ?>
                    <td class="td_linked_label">
                    <? if (isset($item->content_linked_with_element)){ ?>
                        <a href="<?= site_url('cms/element/edit/' . $item->content_linked_with_element->id); ?>" class="link_detail_view" title="Ansehen:&#10;<?= $item_info ?>">
                            <?php if (in_array($field, array("name","lastname"))):?><?= $item_icon ?> <?= $item->content_linked_with_element->name ?><?php endif; ?>
                            <?php if (in_array($field, array("title"))):?><?= $item->content_linked_with_element->title ?><?php endif; ?>
                        </a>
                        <? } else { ?>
                        <a href="<?= site_url($box_index['controllername'] . '/view/' . $item->id); ?>" class="link_detail_view" title="Ansehen:&#10;<?= $item_info ?>">
                            <?php if (in_array($field, array("name","lastname"))):?><?= $item_icon ?><?php endif; ?>
                            <?= $item->$field ?>
                        </a>
                       <? } ?>
                    </td>
                <?php elseif (in_array($field, array("old_link"))): ?>
                    <td style="max-width: 20%;width: 20%;">
                        <?php
                        ?><a href="<?=$item->$field?>" target="_blank"><i class="fa fa-external-link"></i> <?=$item->$field?></a>
                        <?php
                        ?>
                    </td>
                <?php elseif (in_array($field, array("new_mobile_link", "new_desktop_link"))): ?>
                    <td style="max-width: 20%;width: 20%;">
                        <?php
                        ?><a href="//<?= HOST_FRONTEND."/".$item->$field ?>" target="_blank"><i class="fa fa-external-link"></i> <?= HOST_FRONTEND."/".$item->$field ?></a>
                        <?php
                        ?>
                    </td>
                <?php elseif (in_array($field, array("__preview"))): ?>
                    <td style="max-width: 20%;width: 20%;">
                        <?php
                        ?><a href="<?=$this->media_lib->url($item)?>" target="_blank"><img style="max-width:200px;" src="<?=ci()->media_lib()->url($item)?> ?>" /></a>
                        <?php
                        ?>
                    </td>
                <?php elseif (in_array($field, array("score"))): ?>
                    <td>
                        <?= number_format($item->$field,3) ?>
                    </td>
                <?php elseif (in_array($field, array("tags"))): ?>
                    <td>
                        <?php
                        foreach ($item->tag_items as $tag_item):
                            echo $tag_item->category_name."/".$tag_item->name."<br/>";
                        endforeach;
                        ?>
                    </td>
                <?php elseif (in_array($field, array("Objekte")) ): ?>
                    <td>
                        <?php if (1 && !empty($item->package)): ?>
                            <?php foreach ($item->package as $package): ?>
                                <?= ci()->get_view('boxes/object_button/object_button_view', ['object_type' => "package", 'object' => $package, 'data_placement' => 'left']) ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (1 && !empty($item->hotel)): ?>
                            <?php foreach ($item->hotel as $hotel): ?>
                                <?= ci()->get_view('boxes/object_button/object_button_view', ['object_type' => "hotel", 'object' => $hotel, 'data_placement' => 'left']) ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php elseif (in_array($field, array("Elemente"))): ?>
                    <td class="col-xs-1">
                        <a href="<?= site_url("cms/element" . '/edit/' . $item->id); ?>" class="btn btn-default btn-sm btn-block" title="Elemente bearbeiten">
                            <i class="fa fa-file-text-o"></i> <?= ($item->$field > 0)? $item->$field : "0 !" ?>
                        </a>
                    </td>
                <?php elseif (in_array($field, array("scorable"))): ?>
                    <td data-field="<?=$field?>">
                        <?= ($item->$field)? "Ja" : "Nein" ?>
                    </td>
                <?php elseif (in_array($field, array("online"))): ?>
                    <td data-field="<?=$field?>">
                        <?= ($item->$field)? "<a class='btn btn-sm btn-default' href='" . site_url("export/publisher/publish_$modelname/" . $item->id) . "' onclick='return ajax_submit(this)'><i style=\"color: green\" class=\"fa fa-circle\" aria-hidden=\"true\"></i> Online <i class=\"fa fa-refresh\" aria-hidden=\"true\"></i></a>" : "<i style=\"color: red\" class=\"fa fa-times\" aria-hidden=\"true\"></i> Offline" ?>
                    </td>
                <?php elseif (in_array($field, array("brand"))): ?>
                    <td data-field="<?=$field?>">
                        <?= ($item->$field == "SMR")? "<img src=". site_url('img/smr.svg')." style='width: 40px'>" : "<img src=". site_url('img/cw.svg')." style='width: 40px'>" ?>
                    </td>
                <?php elseif (in_array($field, array("sort"))): ?>
                    <td data-field="<?=$field?>">
                        <? if (isset($item->knot_item) || isset($item->knot_item) || isset($item->knot_item)){ ?>
                            <form action="<?=site_url("cms/knot/save_knot/".$item->knot_item->get_knot_key())?>" onsubmit="return ajax_submit(this);">
                                <input title="Sort (10,20,30) (Enter zum Speichern!)" placeholder="00" size="5" maxlength="5" name="data[knot][<?=$item->knot_item->get_knot_key()?>][sort]" value="<?=html_escape($item->knot_item->sort)?>"/>
                            </form>
                        <? } ?>
                    </td>
                <?php elseif (in_array($field, array("page_linked")) && @$item->content_type_key == 'topichead'): ?>
                    <td data-field="<?=$field?>">
                        <form action="<?=site_url("cms/knot/save_knot/".$item->knot_item->get_knot_key())?>" onchange="return ajax_submit(this);">
                        <?= ci()->bootstrap_lib()->show_field2(array(
                            "id" => "data[$modelname][$item->id][$field]",
                            "name" => "data[knot][{$item->knot_item->get_knot_key()}][page_linked]",
                            "field_info" => array("type" => "checkbox"),
                            "value" => intval($item->$field),
                        )) ?>
                        </form>
                    </td>
                <?php elseif (in_array($field, array("stars"))): ?>
                    <td data-field="<?=$field?>">
                        <?= ($stars = @ci()->hotel_attribute_model()->get_stars($item->id)) && !empty($stars)? $stars[0]->value : "BITTE STERNE ANGEBEN!" ?>
                    </td>
                <?php elseif (!in_array($field, array("id","created","modified"))): ?>
                    <td>
                        <?= $item->$field ?>
                    </td>
                <?php endif; ?>
            <?php else: ?>
                <td class="text-right">
                    <?= ci()->get_view($action_view, array("item"=>$item,"box_index" => $box_index,"offerer_id" => @$offerer_id)) ?>
                </td>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <td>
            <?= $item->id ?>
        </td>
        <td>
            <?= $item->title; ?>
        </td>
        <td>
            <?php // echo $item->text; ?>
        </td>
    <?php endif; ?>
</tr>