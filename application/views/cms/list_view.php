<?php
$bootstrap_lib = ci()->bootstrap_lib();
if ($box_index['modelname']){
//    ci()->loader()->model($box_index['modelname']."_model");
    $model = ci()->any_model($box_index['modelname']);
}
if (isset($box_index['list_fields'])) {
    $list_fields = $box_index['list_fields'];
} else {
	$list_fields = @$model->list_fields;
}
if (!isset($box_index['list_fields_2D'])) {
    $list_fields_2D = (@$model->list_fields_2D)? $model->list_fields_2D : $model->get_fields_2D($list_fields);
    $model->apply_schema($list_fields_2D);
} else {
    $list_fields_2D = $box_index['list_fields_2D'];
}
if (!isset($items) && isset($box_index['items']))
	$items = $box_index['items'];

if (isset($box_index['action_view']))
	$action_view = $box_index['action_view'];
else
	$action_view = $box_index['controllername']."/actions_view";

if (isset($box_index['list_action_view']))
	$list_action_view = $box_index['list_action_view'];
else
	$list_action_view = "cms"/*$box_index['controllername']*/."/list_actions_view";

if (isset($box_index['list_row_view']))
    $list_row_view = $box_index['list_row_view'];
else
    $list_row_view = "cms/list_row_view";

$table_view = (isset($box_index['table_view']))? $box_index['table_view'] : "boxes/table";

$is_sortable = (@$box_index['sortable'])? true : false;
$sortable_tbody_class = (@$is_sortable)? "sortable" : "";

//	var_dump($items);
//	var_dump($list_fields);
//	var_dump($box_index['modelname']);
?>
<div class="panel panel-default box_index box_<?=$box_index['modelname']?>_index controller_<?= ci()->get_controller_name() ?>" data-action="<?= @$box_index['box_index_action'] ?>">
    <?= ($is_sortable)? ci()->get_view("boxes/tbody_sortable_js") : "" ?>
    <input type="hidden" name="data[box_index][controller_name]" value="<?= ci()->get_controller_name() ?>"/>
    <?#= (in_array($box_index['modelname'], array('picture')))? ci()->get_view("cms/picture/search_view", array("box_search" => @$box_search))  : "" ?>
	<?php if (@$box_index['title'] !== false): ?>
        <div class="panel-heading" style="cursor: pointer;" onclick="toggleList(this)">
		    <h2 class="panel-title">
            <i class="fa fa-angle-right toggle" style="width:1em;"></i>
            <?= $box_index['title'] ?>
                <? if(@$box_edit['item']->package_number) { ?> zu <?= $box_edit['item']->package_number ?><? }?>
            <?php if (@$box_index['box_index_action']):?>
                <a href="javascript:void(0);"
                   onmouseover="$(this).children('i').addClass('fa-spin');"
                   onmouseout="$(this).children('i').removeClass('fa-spin');"
                   onclick="ajax_submit('.box_<?=$box_index['modelname']?>_index.controller_<?= ci()->get_controller_name() ?>')">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
            </h2>
        </div>
        <?php endif; ?>
    <?= (empty($items) && !@$box_index['lazy'])? ci()->get_view("boxes/empty_list_view",[] ) : "" ?>
    <?= ci()->get_view($table_view, [
            "items" => $items,
            "show_table" => @$show_table,
            "list_fields_2D" => $list_fields_2D,
            "box_index" => $box_index,
            "model" => $model,
            "list_row_view" => $list_row_view,
        ]
    ); ?>
    <? if (!@$box_index['hide_actions']): ?>
        <div class="panel-footer">
        <?=	ci()->get_view(
            $list_action_view,
            array(
                "CI" => ci(),#stupid
                "box_index" => $box_index,
                "offerer_id"=> @$offerer_id,#deprecated
                )) ?>
        </div>
    <? endif; ?>
    <?php
        if (0){
            foreach ($items as $item):
                //var_dump($item);
                //$this->table->add_row((array) $item);
                $this->table->add_row($item);
            endforeach;

            echo $this->table->generate();
        }
    ?>
</div>
<p><?= @$box_index['title_comment'] ?></p>
