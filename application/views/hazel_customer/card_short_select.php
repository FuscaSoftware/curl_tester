<?php
/**
 * User: sbraun
 * Date: 24.08.18
 * Time: 16:10
 */
$title = (count($customers) > 0) ? "Kunden zur Auswahl" : '';
//$disabled = ($customer->id > 0) ? 'disabled="disabled"' : "";
//
//$fields = [
//    [
//        'label' => "Vorname",
//        'name' => "first_name",
//        'disabled' => $disabled,
//        'value' => $customer->first_name,
//        'placeholder' => "Vorname",
//    ],
//    [
//        'label' => "Nachname",
//        'name' => "last_name",
//        'disabled' => $disabled,
//        'value' => $customer->last_name,
//        'placeholder' => "Nachname",
//    ],
//    [
//        'label' => "Adresse",
//        'name' => "address",
//        'disabled' => $disabled,
//        'value' => "$customer->address // $customer->zip $customer->city/$customer->country_code",
//        'placeholder' => "Adresse",
//    ],
//];
$multiple = (1)? "multiple" : '';
?>
<div class="card bg-light">
    <script>
        function choose_customer(select) {
            var url = '<?= site_url('hazel/customer/choose/booking') ?>';
            var val = $(select).val();
            if (val.length > 1)
                alert("Bitte nur einen Kunden auswählen!");
            else if (val.length === 1)
                ajax_data2(url, {customer_id: val[0]});
        }
    </script>
    <form class="form-horizontal">
<!--        <h5 class="card-title m-b-0">--><?//= $title ?><!--</h5>-->
        <div class="card-body">
            <div class="form-group">
                <label for="customer_select"><?= $title ?></label>
                <select <?= $multiple ?> class="form-control" name="customer_select" onchange="choose_customer(this);">
                    <? foreach ($customers as $customer) { ?>
                        <option value="<?= $customer->id ?>"><?= "$customer->name // $customer->first_name // $customer->last_name // $customer->city // $customer->address" ?></option>
                    <? } ?>
                </select>
            </div>
        </div>
        <!--        <div class="border-top">-->
        <!--            <div class="card-body">-->
        <!--                <button type="button" class="btn btn-primary">Submit</button>-->
        <!--            </div>-->
        <!--        </div>-->
    </form>
</div>