<?php
/**
 * User: sbraun
 * Date: 24.08.18
 * Time: 16:10
 */
$title = ($customer->id == 0) ? "Neuer Kunde" : "{$customer->name} (KN: {$customer->id})";
$disabled = ($customer->id > 0) ? 'disabled="disabled"' : "";

$fields = [
    [
        'label' => "Vorname",
        'name' => "first_name",
        'disabled' => $disabled,
        'value' => $customer->first_name,
        'placeholder' => "Vorname",
    ],
    [
        'label' => "Nachname",
        'name' => "last_name",
        'disabled' => $disabled,
        'value' => $customer->last_name,
        'placeholder' => "Nachname",
    ],
    [
        'label' => "Adresse",
        'name' => "address",
        'disabled' => $disabled,
        'value' => "$customer->address // $customer->zip $customer->city/$customer->country_code",
        'placeholder' => "Adresse",
    ],
];
?>
<div class="card bg-light">
    <form class="form-horizontal">
        <h5 class="card-title m-b-0"><?= $title ?> <i class="fa fas fa-edit" onclick="edit_customer('<?= $customer->id ?>')"></i></h5>
        <div class="card-body">
            <? foreach ($fields as $field) { ?>
                <?= ci()->get_view('components/form_field', ['field' => $field]) ?>
                <!--                --><? // ci()->dump($i) ?>
            <? } ?>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Bemerkung</label>
                <div class="col-sm-9">
                    <textarea class="form-control"></textarea>
                </div>
            </div>
            <button type="button" class="btn btn-primary">Speichern</button>
        </div>
        <!--        <div class="border-top">-->
        <!--            <div class="card-body">-->
        <!--                <button type="button" class="btn btn-primary">Submit</button>-->
        <!--            </div>-->
        <!--        </div>-->
    </form>
</div>