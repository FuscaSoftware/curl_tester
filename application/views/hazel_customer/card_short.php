<?php
/**
 * User: sbraun
 * Date: 24.08.18
 * Time: 16:10
 */
$title = ($customer->id == 0)? "Neuer Kunde" : "{$customer->name} (KN: {$customer->id})";

?>
<div class="card bg-light">
    <form class="form-horizontal">
        <div class="card-body">
            <h5 class="card-title m-b-0"><?= $title ?></h5>
            <pre><? var_dump($customer) ?></pre>
            <div class="form-group row">
                <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="fname" placeholder="First Name Here">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="lname" placeholder="Last Name Here">
                </div>
            </div>
            <div class="form-group row">
                <label for="pw" class="col-sm-3 text-right control-label col-form-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="pw" placeholder="Password Here">
                </div>
            </div>
            <div class="form-group row">
                <label for="email1" class="col-sm-3 text-right control-label col-form-label">Company</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email1" placeholder="Company Name Here">
                </div>
            </div>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Contact No</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="cono1" placeholder="Contact No Here">
                </div>
            </div>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Message</label>
                <div class="col-sm-9">
                    <textarea class="form-control"></textarea>
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-primary">Speichern</button>
<!--        <div class="border-top">-->
<!--            <div class="card-body">-->
<!--                <button type="button" class="btn btn-primary">Submit</button>-->
<!--            </div>-->
<!--        </div>-->
    </form>
</div>