<?php
/**
 * User: sbraun
 * Date: 24.08.18
 * Time: 15:51
 */
$attributes = [
    'name' => 'data[customer_lookup]',
    'value' => '',
    'placeholder' => 'Kundennr./Name',
    'class' => 'form-control',
    "aria-label" => "Kundennummer/Name",
    "aria-describedby" => "button-addon2",
    "required" => "required",
];
?>
<div class="row">
    <div class="col-md-4">
        <script>
            function customer_select_lookup(form) {
                return ajax_submit(form, function(){
                    $(form).find('button').val(0);
                });
            }
        </script>
        <form id="new-booking-customer-lookup-form"
              method="post"
              action="<?= site_url("hazel/customer/lookup") ?>"
              onsubmit="return customer_select_lookup(this);"
        >
            <div class="input-group mb-3">
                <?= ci()->bootstrap_lib()->show_field2($attributes) ?>
                <div class="input-group-append" id="button-addon4">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2" onclick="">
                        <i class="fa fa-search"></i>
                    </button>
                    <button class="btn btn-outline-secondary" type="submit" name="data[create_new_customer]" value="0" onclick="$(this).val(1);">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </form>
        <div class="col-md-12 hazel_container_customer_select">
        </div>
    </div>


    <div class="col-md-8 hazel_container_customer_card">
        <?#= ci()->get_view('hazel_customer/card_short', []) ?>
    </div>

</div>
