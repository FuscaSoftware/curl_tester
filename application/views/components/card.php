<?php
/**
 * User: sbraun
 * Date: 2019-03-14
 * Time: 15:02
 */

use \de\fusca\hazelnut\gui\components;

$icon = (isset($icon) && $icon == false) ? "" : ($icon ?? "mdi mdi-account-plus");
$i_tag = ($icon) ? "<i class=\"$icon\"></i>" : '';
?>
<?php if ($is_linked): ?>
    <a <?= ($href ?? false) ? 'href="' . $href . '"' : '' ?>>
<?php endif; ?>
    <div class="card <?= ($is_linked) ? 'card-hover' : '' ?>">
        <?php if ($is_linked): ?>
            <div class="box <?= $bg_color ?? '' ?> <?= ($text_center ?? false) ? 'text-center' : '' ?>">
                <h1 class="font-light text-white"><?= $i_tag ?? '' ?></h1>
                <h6 class="text-white"><?= $text ?></h6>
            </div>
        <?php else: ?>

        <?php endif; ?>
    </div>
<?php if ($is_linked): ?>
    </a>
<?php endif; ?>