<?php
/**
 * User: sbraun
 * Date: 29.09.18
 * Time: 00:14
 */
$checked = ($checked)? "checked=\"checked\"" : "";
$id = uniqid('switch_');
?>
<style>

</style>
<div class="onoffswitch">
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" <?= $checked ?>>
    <label class="onoffswitch-label" for="myonoffswitch">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>