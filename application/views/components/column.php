<?php
/**
 * User: sbraun
 * Date: 2019-03-14
 * Time: 16:05
 */
?>
<!-- Column -->
<div class="col-md-<?= $md ?? 6 ?> col-lg-<?= $lg ?? 2 ?> col-xlg-<?= $xlg ?? 3 ?>">
%s
</div>
<!-- Column -->
