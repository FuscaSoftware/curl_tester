<?php
/**
 * User: sbraun
 * Date: 17.09.18
 * Time: 15:45
 */
$field_default = [
    'Label' => 'Label',
    'name' => 'field_name',
    'placeholder' => 'Platzhalter',
    'disabled' => (0)? 'disabled="disabled"' : '',
    'value' => '',
    'cols' => [3,9],
];
if (isset($field))
    $field = array_merge($field_default, $field);
?>
<div class="form-group row">
    <label for="<?= $field['name'] ?>" class="col-sm-<?= $field['cols'][0] ?> text-right control-label col-form-label"><?= $field['label'] ?></label>
    <div class="col-sm-<?= $field['cols'][1] ?>">
        <input type="text" class="form-control" id="" name="<?= $field['name'] ?>" placeholder="<?= $field['placeholder'] ?>" <?= $field['disabled'] ?> value="<?= $field['value'] ?>">
    </div>
</div>