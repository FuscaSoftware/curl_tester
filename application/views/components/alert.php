<?php
/**
 * User: sbraun
 * Date: 2019-03-14
 * Time: 15:02
 */
use \de\fusca\hazelnut\gui\components;
//$icon = (isset($icon) && $icon == false)? "" : ($icon ?? "mdi mdi-account-plus");
//$i_tag = ($icon)? "<i class=\"$icon\"></i>" : '';
?>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <?php if($title ?? 0): ?><strong><?= $title ?></strong><?php endif; ?> <?= $text ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>