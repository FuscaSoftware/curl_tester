<?php
$attributes = [];
if ($id ?? false)
    $attributes[] = "id=\"$id\"";
if ($name ?? false)
    $attributes[] = "name=\"$name\"";
if ($action ?? false)
    $attributes[] = "action=\"$action\"";
if ($onSubmit ?? false)
    $attributes[] = "onSubmit=\"$onSubmit\"";

?>
<form <?= implode(" ",$attributes) ?>>
    %s
</form>
