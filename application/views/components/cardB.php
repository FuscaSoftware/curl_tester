<?php
/**
 * User: sbraun
 * Date: 2019-03-14
 * Time: 15:02
 */

use \de\fusca\hazelnut\gui\components;

$icon = (isset($icon) && $icon == false) ? "" : ($icon ?? "mdi mdi-account-plus");
$i_tag = ($icon) ? "<i class=\"$icon\"></i>" : '';
?>
<?php if ($is_linked): ?>
    <a <?= ($href ?? false) ? 'href="' . $href . '"' : '' ?>>
<?php endif; ?>
    <div class="card <?= ($is_linked) ? 'card-hover' : '' ?>">
        <?php if ($is_linked): ?>
            <div class="box <?= $bg_color ?? '' ?> <?= ($text_center ?? false) ? 'text-center' : '' ?>">
                <h1 class="font-light text-white"><?= $i_tag ?? '' ?></h1>
                <h6 class="text-white"><?= $text ?></h6>
            </div>
        <?php else: ?>
            
        <?php endif; ?>
    </div>
<?php if ($is_linked): ?>
    </a>
<?php endif; ?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title">To Do List</h4>
        <div class="todo-widget scrollable ps-container ps-theme-default ps-active-y" style="height:450px;" data-ps-id="cba74aa3-9db4-b6be-5f30-f5ae6d6f3abb">
            <ul class="list-task todo-list list-group m-b-0" data-role="tasklist">
                <li class="list-group-item todo-item" data-role="task">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label todo-label" for="customCheck">
                            <span class="todo-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span> <span class="badge badge-pill badge-danger float-right">Today</span>
                        </label>
                    </div>
                    <ul class="list-style-none assignedto">
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/1.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Steave"></li>
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/2.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Jessica"></li>
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/3.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Priyanka"></li>
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/4.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selina"></li>
                    </ul>
                </li>
                <li class="list-group-item todo-item" data-role="task">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                        <label class="custom-control-label todo-label" for="customCheck1">
                            <span class="todo-desc">Lorem Ipsum is simply dummy text of the printing</span><span class="badge badge-pill badge-primary float-right">1 week </span>
                        </label>
                    </div>
                    <div class="item-date"> 26 jun 2017</div>
                </li>
                <li class="list-group-item todo-item" data-role="task">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                        <label class="custom-control-label todo-label" for="customCheck2">
                            <span class="todo-desc">Give Purchase report to</span> <span class="badge badge-pill badge-info float-right">Yesterday</span>
                        </label>
                    </div>
                    <ul class="list-style-none assignedto">
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/3.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Priyanka"></li>
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/4.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selina"></li>
                    </ul>
                </li>
                <li class="list-group-item todo-item" data-role="task">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                        <label class="custom-control-label todo-label" for="customCheck3">
                            <span class="todo-desc">Lorem Ipsum is simply dummy text of the printing </span> <span class="badge badge-pill badge-warning float-right">2 weeks</span>
                        </label>
                    </div>
                    <div class="item-date"> 26 jun 2017</div>
                </li>
                <li class="list-group-item todo-item" data-role="task">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                        <label class="custom-control-label todo-label" for="customCheck4">
                            <span class="todo-desc">Give Purchase report to</span> <span class="badge badge-pill badge-info float-right">Yesterday</span>
                        </label>
                    </div>
                    <ul class="list-style-none assignedto">
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/3.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Priyanka"></li>
                        <li class="assignee"><img class="rounded-circle" width="40" src="../../assets/images/users/4.jpg" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selina"></li>
                    </ul>
                </li>
            </ul>
            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: -13px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 13px; height: 450px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 13px; height: 437px;"></div></div></div>
    </div>
</div>
