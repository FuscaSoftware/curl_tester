<?php
/**
 * User: sbraun
 * Date: 2019-03-14
 * Time: 15:00
 */
$attributes = [];
if ($style ?? false)
    $attributes[] = "style=\"$style\"";
?>
<div class="<?= $class ?? '' ?>" id="<?= $id ?? '' ?>" <?= implode(" ", $attributes) ?>>
    %s
</div>