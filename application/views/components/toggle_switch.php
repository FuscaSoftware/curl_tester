<?php
/**
 * User: sbraun
 * Date: 29.09.18
 * Time: 00:14
 */
$checked = ($checked)? "checked=\"checked\"" : "";
$id = uniqid('switch_');
?>
<style>

</style>
<div class="onoffswitch">
    <input type="checkbox" name="<?= $id ?>" class="onoffswitch-checkbox" id="<?= $id ?>" <?= $checked ?>>
    <label class="onoffswitch-label" for="<?= $id ?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>