<?php
/**
 * User: sbraun
 * Date: 24.08.18
 * Time: 12:25
 */
/**
 * bootstrap 4
 */
$folds = $folds ?? [
        [
            'label' => "Toggle, Open by default",
            'content' => "This box is opened by default, paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.",
            'opened' => true,
            'icon' => ["fa fa-arrow-right", "fa fa-times"],
        ],
        [
            'label' => "Toggle, Closed by default",
            'content' => "This box is now open",
            'opened' => false,
            'icon' => ["fa fa-arrow-right", "fa fa-times"],
        ],
    ];
$accordian_id = uniqid("accordian-");
?>
    <!-- toggle part -->
    <div id="<?= $accordian_id ?>">
        <div class="card">
            <?php foreach ($folds as $k => $fold) { ?>
                <?php
                $fold_icon = (isset($fold['opened']) && $fold['opened']) ?
                    $fold['icon'][0] ?? "fa fa-arrow-right" : $fold['icon'][1] ?? "fa fa-times";
                $fold_class = (isset($fold['opened']) && $fold['opened']) ? "show" : '';
                $fold_expanded = (isset($fold['opened']) && $fold['opened']) ? "true" : 'false';
                $fold_id = "Toggle-" . ($k + 1);
                ?>
                <a class="card-header link"
                   data-toggle="collapse" data-parent="#accordian-4" href="#<?= $fold_id ?>"
                   aria-expanded="<?= $fold_expanded ?>" aria-controls="<?= $fold_id ?>">
                    <i class="<?= $fold_icon ?>" aria-hidden="true"></i>
                    <span><?= $fold['label'] ?></span>
                </a>
                <div id="<?= $fold_id ?>" class="collapse <?= $fold_class ?> multi-collapse">
                    <? if (!@$fold['no_card_body']) { ?>
                    <div class="card-body widget-content">
                    <? } ?>
                        <?= $fold['content'] ?>
                    <? if (!@$fold['no_card_body']) { ?>
                    </div>
                    <? } ?>
                </div>
            <?php } ?>
        </div>
    </div>


<?php
/* old from sample */
//    <a class="card-header link border-top" data-toggle="collapse" data-parent="#accordian-4" href="#Toggle-2" aria-expanded="false" aria-controls="Toggle-2">
//            <i class="fa fa-times" aria-hidden="true"></i>
//            <span>Toggle, Closed by default</span>
//        </a>
//        <div id="Toggle-2" class="multi-collapse collapse" style="">
//            <div class="card-body widget-content">
//                This box is now open
//            </div>
//        </div>
//        <a class="card-header link collapsed border-top" data-toggle="collapse" data-parent="#accordian-4" href="#Toggle-3" aria-expanded="false" aria-controls="Toggle-3">
//            <i class="fa fa-times" aria-hidden="true"></i>
//            <span>Toggle, Closed by default</span>
//        </a>
//        <div id="Toggle-3" class="collapse multi-collapse">
//            <div class="card-body widget-content">
//                This box is now open
//            </div>
//        </div>
?>