<?php
/**
 * User: sbraun
 * Date: 2019-04-07
 * Time: 01:56
 */
$attr_id = ($id ?? false)? 'id=\"$id\"' : '';
?>
<div class="card-body <?= $addClass ?? '' ?>"<?= $attr_id ?>>
    <?php if ($title ?? 0) { ?><h5 class="card-title"><?= $title ?></h5><?php } ?>
    %s
</div>
