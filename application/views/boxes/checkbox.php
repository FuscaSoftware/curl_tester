<?php
/*
 * info: sourrounding label of the checkbox crashs the checkbox in chrome
 */
$id = uniqid("id_" . $id . "_");
/*
 * remove form-control class because it leads to strange shadow line
 */
$class = str_replace(["form-control"], [''], $class);
?>
<? if ($label_text_before) { ?>
    <label for="<?= $id ?>">
        <?= @$label_text_before ?>
    </label>
<? } ?>
<input id="<?= $id ?>_view" data-id="<?= $id ?>" device="<?= $device ?? '' ?>" class="<?= $class ?>" value="<?= $value ?>"
       type="checkbox" autocomplete="off"
    <?= ($value) ? 'checked="checked"' : '' ?>
       onchange="if($(this).prop('checked')){$($('[name=&quot;<?= $name ?>&quot;]')).val(1);}else{$($('[name=&quot;<?= $name ?>&quot;]')).val(0);}<?= $onchange ?>">
<? if ($label_text_after) { ?>
    <label for="<?= $id ?>">
        <?= @$label_text_after ?>
    </label>
<? } ?>
<input type="hidden" id="<?= $id ?>" device="<?= $device ?? '' ?>" name="<?= $name ?>" class="<?= $class ?>" value="<?= $value ?>">
