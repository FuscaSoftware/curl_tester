<?php
	$CI = ci();
    $id = $box_edit['item']->id;
	$item = $box_edit['item'];
//	$box_index['controllername'] = "cms/content";
	$class = @$box_edit['class'];
//	var_dump(array('edit6' => $item));
    $modelname = $box_edit['modelname'];
    $info = "ID: {$item->id}\nBearbeitet: " . @$item->modified . " \nBearbeiter: " . @$item->modifier
        . "\nErstellt: " . @$item->created . "\nErsteller: " . @$item->creator
        . "\nVeröffentlicht: $item->published";
    if (isset($item->meta_tag))
        $info .= "\nMeta-Title: «" . $item->meta_tag->text . "»";
?>

<div class="panel panel-default box_edit <?= $class ?>">
	<?php if (!@$box_edit['hide_header']): ?>
        <div class="panel-heading">
		    <h1 class="panel-title">
                <i class="fa <?= ci()->bootstrap_lib()->get_object_icon($modelname) ?>"></i>
                <?= $box_edit['title'] ?>
                <?php
                if ($box_edit['modelname'] == 'topic') { ?>
                    <? $color = (@$topic_published)? "green" : "red" ?>
                    <i style="color: <?= $color ?>" class="fa fa-circle" aria-hidden="true"></i>
                 <?php   } ?>
                <i style="float: right;" class="fa fa-info-circle popover-init" data-trigger="hover" data-toggle="popover" data-placement="left" data-html="true" data-content="<?= nl2br($info) ?>"></i>
                <a title="History ansehen" href="<?= site_url('user/user_history?object_type='.$modelname.'&object_id=' . $item->id) ?>" style="float: right; margin-right: 5px;"><i class="fa fa-calendar" aria-hidden="true"></i> </a>
            </h1>
            <script>//$('.box_edit h1 i').popover({});</script>
        </div>
	<?php endif; ?>
    <div class="panel-body">
    <?= $this->bootstrap_lib->show_messages() ?>
    <?#= validation_errors(); ?>
    <?= $this->bootstrap_lib->show_validation_errors(); ?>

    <?= form_open($box_edit['form']['target'], array("class"=>"box_edit_form #form-inline")) ?>
    <input type="hidden" name="request_type" value="<?=(ci()->request_type)? ci()->request_type : ""?>">
    <input type="hidden" name="data[<?=$box_edit['modelname']?>][<?= $box_edit['item']->id ?>][id]" value="<?= $box_edit['item']->id ?>">

    <table class="table table-hover table-condensed no-lines" style="">
    <?php #var_dump($box_edit['form']['backend_fields']); ?>
    <?php foreach ($box_edit['form']['backend_fields'] as $k => $fieldInfo): ?>
        <?php
            $field_table = (@$fieldInfo['table'])? $fieldInfo['table'] : $modelname;
            $field1_attributes = array(
                "name" => "data[$field_table][$id][$k]",
                "field_info" => @$fieldInfo,
                "value" => @$box_edit['item']->$k,
                "items" => (isset($fieldInfo['values']))? $fieldInfo['values'] : null,
                "class" => "form-control",
                "view" => (isset($fieldInfo['view']))? $fieldInfo['view'] : null,
                "placeholder" => (isset($fieldInfo['placeholder_value']))? $fieldInfo['placeholder_value'] : null,
            );
			$producer_marked = (@$fieldInfo['producer_marked']) ? '<span class="producer-marked">*</span>' : '';
        ?>

		<?php if (@$fieldInfo['type'] != "hidden"): ?>
            <tr>
<!--        <div class="well well-sm">-->
<!--            <div class="form-group">-->
            <td>
                <label for="<?=$field1_attributes['name']?>">
					<?= $producer_marked ?> <?= (isset($fieldInfo['COLUMN_COMMENT']) && !empty($fieldInfo['COLUMN_COMMENT']))? $fieldInfo['COLUMN_COMMENT'] : "(".ucfirst($k).")"?>
				</label>
            </td>
		<?php endif; ?>
            <td>
                <?= ci()->bootstrap_lib()->show_field2($field1_attributes) ?>
            </td>
		<?php if (@$fieldInfo['type'] != "hidden"): ?>
<!--            </div>-->
<!--        </div>-->
            </tr>
		<?php endif; ?>

    <?php endforeach; ?>
    </table>

	<?php
//	var_dump(@$item);
		if (isset($item->foreign_keys)):
//			echo "<!-- 48 -->";
			foreach ($item->foreign_keys as $k => $v):
		?>
			<?php
				$field2_attributes = array(
					"name" => "data[$modelname][$id][foreign_keys][$k]",
					"field_info" => array("type" => "hidden"),
					"value" => $v,
	//				"items" => (isset($fieldInfo['values']))? $fieldInfo['values'] : null,
	//				"class" => "form-control",
				);
			?>
			<?= $this->bootstrap_lib->show_field2($field2_attributes) ?>
		<?php endforeach; ?>
    <?php endif; ?>
    <?= (isset($box_edit['set_top_topic_left']))? $box_edit['set_top_topic_left'] : '' ?>
    <?= (isset($box_edit['remarks']))? $box_edit['remarks'] : '' ?>
    <?= (isset($box_edit['metatags']))? $box_edit['metatags'] : '' ?>
	<?php if (@$box_edit['todo_list']): ?>
		<div class="box_edit_todo_list_container well well-sm">
			<label>ToDo-Liste</label>
			<?= $box_edit['todo_list'] ?>
		</div>
	<?php endif; ?>
    </div>
    <?php if (!@$box_edit['hide_actions']): ?>
        <?= ci()->get_view("boxes/edit_actions_view", ['box_edit' => $box_edit, 'item' => $item]) ?>
    <?php endif; ?>
    <?= form_close() ?>
</div>