<?php
/**
 * User: sbraun
 * Date: 07.12.16
 * Time: 17:28
 *
 * requires:
 * $object_type
 * $object
 * optional:
 * $data_placement : left|right|top|bottom
 * $data_trigger : hover|click
 */


$a_dom_id = uniqid("id_");
ci()->load->library("Media_lib");

if ($object_type == 'product_feed')
    $object_type = 'package';
else if ($object_type == 'pdf_editor') {
    $object_type ='package';
    $url = site_url("tools/pdf_creator/edit/$object->id");
}

ci()->load->model('cms/' . $object_type . "_model");
$model = ci()->{$object_type . "_model"};
$data_placement = (empty($data_placement)) ? "left" : $data_placement;
$data_trigger = (empty($data_trigger)) ? "hover" : $data_trigger;
$icon = Bootstrap_lib::get_object_icon($object_type);
if (!@$url)
    $url = site_url("cms/". @$object_type . '/edit/' . @$object->id);

if ($object_type == "package" && @$object) {
    $field = "package_number";
    $field2 = "name";
    $class = @$object->brand;
    if (!isset($object->online))
        $object->online = ci()->publisher_model()->status_package($object);
    $status_color = ($object->online)? "green" : "red";
    $status = " <i style=\"color:{$status_color}\" class=\"fa fa-circle\" aria-hidden=\"true\"></i>";
    $short_label = $object->{$field} . " - " . $object->{$field2} . @$status;
    $object_label = "Paket-Nr. " . $object->{$field} . " - " . $object->{$field2};
    $pic_url = Media_lib::url($object->get_image(),128);
    $data_content = "<img style='width: 100%;' src='" . $pic_url . "'>";
    foreach ($object as $k => $v) {
        if (in_array($k, $model->list_fields) && is_string($v))
            $data_content .= "\n<br/><em>" . $k . ":</em> «" . html_escape($v) . "»";

    }

} elseif ($object_type == "hotel") {
    $field = "name";
    $object_label = $object->{$field};
    $short_label = $object->{$field};
    $data_content = "";
    foreach ($object as $k => $v) {
        if (in_array($k, $model->list_fields))
            $data_content .= "\n<br/><em>" . $k . ":</em> «" . html_escape($v) . "»";
    }
} elseif ($object_type == "content") {
    $field = "name";
    $type = "content_type_name";
    $object_label = $object->{$field};
    $short_label = "Name: " . $object->{$field} . " - Typ: " . @$object->{$type};
    $data_content = "";
    foreach ($object as $k => $v) {
        if (in_array($k, $model->list_fields))
            $data_content .= "\n<br/><em>" . $k . ":</em> «" . html_escape($v) . "»";
    }
} elseif ($object_type == "element") {
    $content = ci()->content_model()->get_item_by_id(@$object->content_id);
    $url = site_url("cms/element/edit/" . @$content->id);
    $field = "id";
    $type = "content_type_name";
    $object_label = "Element-ID: " . @$object->{$field};
    if ($content) {
    $short_label = "$content->name";
    $object->id = $object->content_id;
    $data_content = "$content->name";
    }
} elseif ($object_type == "notification") {
    $notification_type = ci()->notification_type_model()->get_item_by_field('id', $object->notification_type_id);
    $notification_object = ci()->any_model($object->object_type)->get_item_by_id($object->object_id);
    $notification_object_name = (@$notification_type->name) ? $notification_object->name : @$notification_object->title;
    $short_label = $object_label = "$notification_object_name: $notification_type->name";
    $url = site_url("cms/". @$object->object_type . '/edit/' . @$object->object_id);
    $data_content = "";
    foreach ($object as $k => $v) {
        if (in_array($k, $model->list_fields))
            $data_content .= "\n<br/><em>" . $k . ":</em> «" . html_escape($v) . "»";
    }
} elseif ($object_type == "picture") {
    $field = "title";
    $object_label = $object->{$field};
    $short_label = $object->{$field};
    $data_content = "<img style='max-width: 100%' src='" . Media_lib::url(($object), 256) . "'>";
    foreach ($object as $k => $v) {
        if (in_array($k, $model->list_fields))
            $data_content .= "\n<br/><em>" . $k . ":</em> «" . html_escape($v) . "»";
    }
} else {
    // var_dump($object); die;
    if ($object) {
        $field = "name";
        $object_label = @$object->{$field};
        $short_label = @$object->{$field};
        $data_content = "";

        foreach ($object as $k => $v) {
            if (in_array($k, $model->list_fields))
                $data_content .= "\n<br/><em>" . $k . ":</em> «" . html_escape($v) . "»";
        }
    } else {
        $data_content = "$object_type wurde gelöscht";
        $object_label = "nicht mehr vorhanden";
        $short_label = "nicht mehr vorhanden";
    }
}
?>
<a id="<?= $a_dom_id ?>" href="<?= $url ?>"
   class="btn btn-default btn-sm popover-init"
   title="<?= $object_label ?>"
    <?= (@$target_blank) ? "target='_blank'" : "" ?>
   role="button" data-toggle="popover" data-placement="<?= $data_placement ?>" data-trigger="<?= $data_trigger ?>"
   data-html="true" data-content="<?= @$data_content ?>"
>
    <i class="fa <?= $icon ?> <?= @$class ?>" aria-hidden="true"></i>
    <?= (@$show_short_label && @$short_label) ? $short_label : "" ?>
    <script>$('#<?=$a_dom_id?>').popover({});</script>
</a>
