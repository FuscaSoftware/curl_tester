<?php
/**
 * User: sbraun
 * Date: 17.10.17
 * Time: 14:49
 */
$modelname = $box_edit['modelname'];
$item_label = (isset($box_edit['itemtitle']))? $box_edit['itemtitle'] : ci()->any_model($modelname)->item_label;
$controllername = (isset($box_edit['controllername'])? $box_edit['controllername'] : ci()->get_full_controller_name());
$preview_host = ($item->brand == "CW")? HOST_FRONTEND_CW : HOST_FRONTEND;
$text = (@$item->online)? "aktualisieren" : "veröffentlichen";

?>
<?php if (!@$box_edit['hide_actions']): ?>
    <div class="panel-footer edit_box_actions">
        <button type="submit" name="submit" class="btn btn-success save_action">
            <i class="fa fa-floppy-o"></i> <?#= $item_label ?> speichern
        </button>
        <?php if ($item->id > 0): ?>
            <?php if ($controllername == "cms/content"): ?>
                <a href="<?= site_url("cms/element" . '/edit/' . $item->id); ?>" class="btn btn-default">
                    <i class="fa fa-edit"></i> Elemente bearbeiten
                </a>
                <a href="<?= site_url("cms/content" . '/clone_content/' . $item->id); ?>" onclick="return confirm('Sind Sie sicher?');" class="btn btn-default">
                    <i class="fa fa-copy"></i> Inhalt duplizieren
                </a>
                <?php if ($item->online):?>
                    <a class="btn btn-default" href="<?= site_url('export/publisher/publish_content/' . $item->id) ?>" onclick="return ajax_submit(this)"><i style="color: green" class="fa fa-circle" aria-hidden="true"></i> Veröffentlichung aktualisieren <i class="fa fa-refresh" aria-hidden="true"></i></a>
                <?php else: ?>
                    <a class="btn btn-default" href="<?= site_url('export/publisher/publish_content/' . $item->id) ?>" onclick="return ajax_submit(this)"><i style="color: red" class="fa fa-times" aria-hidden="true"></i> Veröffentlichen <i class="fa fa-globe" aria-hidden="true"></i></a>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($controllername == "cms/gallery"): ?>
                <a href="<?= site_url("cms/gallery" . '/clone_gallery/' . $item->id); ?>" onclick="return confirm('Sind Sie sicher?');" class="btn btn-default">
                    <i class="fa fa-copy"></i> Galerie duplizieren
                </a>
            <?php endif; ?>
            <?php if ($controllername == "cms/package"): ?>
                <a href="<?= '' . '//'.$preview_host.'/paket/' . $item->package_number ?>?cms_session_id=<?= session_id() ?>" class="btn btn-default" title="Desktop Vorschau ansehen"
                   target="_blank">
                    <i class="fa fa-desktop"></i> Vorschau
                </a>
                <a href="<?= '' . '//'.$preview_host.'/paket/' . $item->package_number ?>?cms_session_id=<?= session_id() ?>" class="btn btn-default" title="Mobile Vorschau ansehen"
                   target="_blank"
                   onclick="var prev_topic = window.open($(this).attr('href'), 'Paket Vorschau', 'width=320,height=568');return false;">
                    <i class="fa fa-mobile"></i> Vorschau
                </a>
            <?php endif; ?>
            <?php if ($controllername == "cms/topic"): ?>
                <a href="<?= '' . '//'.$preview_host."/$item->class/" . $item->url ?>?cms_session_id=<?= session_id() ?>" class="btn btn-default" title="Desktop Vorschau ansehen"
                   target="_blank"
                   onclick="var prev_topic = window.open($(this).attr('href'), 'Thema Vorschau', '');return false;">
                    <i class="fa fa-desktop"></i> Vorschau
                </a>
                <a href="<?= '' . '//'.$preview_host."/$item->class/" . $item->url ?>?cms_session_id=<?= session_id() ?>" class="btn btn-default" title="Mobile Vorschau ansehen"
                   target="_blank"
                   onclick="var prev_topic = window.open($(this).attr('href'), 'Thema Vorschau', 'width=320,height=568');return false;">
                    <i class="fa fa-mobile"></i> Vorschau
                </a>
            <?php endif; ?>
            <?php if ($controllername == "cms/hotel" && @$box_edit['hotel_topic_item']):
                ?>
                <a href="<?= '' . '//'.$preview_host."/{$box_edit['hotel_topic_item']->class}/" . $box_edit['hotel_topic_item']->url ?>?cms_session_id=<?= session_id() ?>" class="btn btn-default" title="Desktop Vorschau ansehen"
                   target="_blank"
                   onclick="var prev_topic = window.open($(this).attr('href'), 'Thema Vorschau', '');return false;">
                    <i class="fa fa-desktop"></i> Vorschau
                </a>
                <a href="<?= '' . '//'.$preview_host."/{$box_edit['hotel_topic_item']->class}/" . $box_edit['hotel_topic_item']->url ?>?cms_session_id=<?= session_id() ?>" class="btn btn-default" title="Mobile Vorschau ansehen"
                   target="_blank"
                   onclick="var prev_topic = window.open($(this).attr('href'), 'Thema Vorschau', 'width=320,height=568');return false;">
                    <i class="fa fa-mobile"></i> Vorschau
                </a>
            <?php endif; ?>
            <a href="<?= site_url($controllername . '/delete/' . $item->id); ?>" class="btn btn-default btn-delete">
                <i class="fa fa-trash-o"></i> Löschen
            </a>
            <?php if ($controllername == "cms/package"): ?>
                <div style="float: right;">
                    <?php if (@$item->cronjob_item) {
                    if (strpos($item->cronjob_item->path, 'unpublish') !== false)
                        $cronjob_text = "geunpublished";
                    else
                        $cronjob_text = "gepublished";
                        ?>
                            <a href="<?= site_url("cronjob/edit/" . $item->cronjob_item->id) ?>" class="btn btn-default">
                                <i class="fa fa-clock-o"></i> Paket wird am <?= $item->cronjob_item->next_execution ?> <?= $cronjob_text ?>.
                            </a>
                    <?php } ?>
                    <?php if ($item->published) { ?>
                        <span style="margin-right: 1rem;">Letzte Veröffentlichung: <?= date("d.m.Y H:i:s", strtotime($item->published)) ?></span>
                    <?php } ?>
                    <?php if (ci()->acl_lib->has_role('webmaster')) { ?>
                    <div class="btn-group-vertical" role="group" aria-label="Vertical button group">
                        <div class="btn-group" role="group">
                            <button id="btnGroupVerticalDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i style="color: <?php if ($package_published && $item->booking_status != 'ausgebucht' && $item->booking_status != 'Warteliste' && $item->booking_status != 'Ueberarbeitung' && $item->booking_status != 'Vorabinfo') { ?>green<?php } else if ($package_published) { ?>orange<?php } else { ?>red<?php } ?>" class="fa fa-circle" aria-hidden="true"></i> Veröffentlichung
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <?php if (!@$item->online) { ?>
                                    <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/publish_package/$item->id/1") ?>"
                                       onclick="if (katja_check()) { return ajax_submit(this); } else { return false };"
                                       class="btn btn-default publish_action"
                                       title="<?=$text?>" >
                                        <i class="fa fa-globe"></i> <?= $text ?>
                                    </a>
                                <?php } ?>
                                <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/publish_package/$item->id") ?>"
                                   onclick="if (katja_check()) { return ajax_submit(this); } else { return false };"
                                   class="btn btn-default publish_action"
                                   title="<?=$text?> ohne Mail" >
                                    <i class="fa fa-globe"></i> <?= $text ?> ohne Mail
                                </a>
                                <?php if (@$item->online) { ?>
                                    <? $text = "entfernen";?>
                                    <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/unpublish_package/$item->id/1") ?>"
                                       onclick="return ajax_submit(this);"
                                       class="btn btn-default"
                                       title="<?=$text?>" style="height: 34px;">
                                        <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                            <i class="fa fa-globe fa-stack-1x" style="font-size: 0.5em;"></i> <i class="fa fa-ban fa-stack-1x text-danger"></i>
                            </span> <?= $text ?>
                                    </a>
                                    <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/unpublish_package/$item->id") ?>"
                                       onclick="return ajax_submit(this);"
                                       class="btn btn-default"
                                       title="<?=$text?> ohne Mail" style="height: 34px;">
                                        <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                            <i class="fa fa-globe fa-stack-1x" style="font-size: 0.5em;"></i> <i class="fa fa-ban fa-stack-1x text-danger"></i>
                            </span> <?= $text ?> ohne Mail
                                    </a>
                                <?php } ?>
                                <?php if (@!$item->cronjob_item) { ?>
                                    <a data-href="javascript:void(0);" href="<?= site_url("cronjob/create?foreign_keys[object_type]=package&foreign_keys[object_id]=$item->id") ?>"
                                       onclick="return ajax_submit(this);" class="btn btn-default" title="Planen" style="height: 34px;">
                                        <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                           <i class="fa fa-clock-o"></i>
                            </span> Planen
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <script>
                        // Holger will, dass bei Holger vor Publish extra nochmal eine Benachrichtigung kommt.
                        function katja_check() {
                            <?php if (ci()->get_username() == "Katja Moosdorf") { ?>
                            return confirm("Ich bestätige hiermit, dass ich alle neu angelegten und veränderten Seitenelemente - Bilder wie Texte - angeschaut bzw. gelesen und kontrolliert habe. Alle Desktop-/Mobile-Button wurden von mir richtig gesetzt. Wenn ich dieses Paket veröffentliche und dabei zu obigen Fakten die Unwahrheit gesagt habe, sollen mir die Hand verfaulen, die die Maus führt, die Haare ausfallen und sich meine Zähne schwarz verfärben, so wahr mir der König der Löwen helfe!");
                            <?php } else { ?>
                                return true;
                            <?php } ?>
                        }
                    </script>
                    <?php } ?>
                </div>
            <?php endif; ?>
            <?php if ($controllername == "cms/topic"): ?>
                <div style="float: right;">
                    <?php if (@$item->cronjob_item) {
                        if (strpos($item->cronjob_item->path, 'unpublish') !== false)
                            $cronjob_text = "geunpublished";
                        else
                            $cronjob_text = "gepublished";
                        ?>
                        <a href="<?= site_url("cronjob/edit/" . $item->cronjob_item->cronjob_id) ?>" class="btn btn-default">
                            <i class="fa fa-clock-o"></i> Topic wird am <?= date("d.m.Y H:i", strtotime($item->cronjob_item->next_execution)); ?> Uhr <?= $cronjob_text ?>.
                        </a>
                    <?php } ?>
                    <?php
                    if (@$topic_published)
                        $text = "aktualisieren";
                    else
                        $text = "veröffentlichen";
                    ?>
                    <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/publish_topic/$item->id") ?>"
                       onclick="return ajax_submit(this);"
                       class="btn btn-default"
                       title="<?=$text?>" >
                        <i class="fa fa-globe"></i> <?= $text ?>
                    </a>
                    <?php if ($topic_published) { ?>
                        <? $text = "entfernen";?>
                        <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/unpublish_topic/$item->id") ?>"
                           onclick="return ajax_submit(this);"
                           class="btn btn-default"
                           title="<?=$text?>" style="height: 34px;">
                            <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                            <i class="fa fa-globe fa-stack-1x" style="font-size: 0.5em;"></i> <i class="fa fa-ban fa-stack-1x text-danger"></i>
                            </span> <?= $text ?>
                        </a>
                        <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/delete_topic_cache/$item->id") ?>"
                           onclick="return ajax_submit(this);" class="btn btn-default" title="Planen" style="height: 34px;">
                                        <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                           <i class="fa fa-ban"></i>
                            </span> Cache leeren
                        </a>
                    <?php } ?>
                    <?php if (@!$item->cronjob_item) { ?>
                        <a data-href="javascript:void(0);" href="<?= site_url("cronjob/create?foreign_keys[object_type]=topic&foreign_keys[object_id]=$item->id") ?>"
                           onclick="return ajax_submit(this);" class="btn btn-default" title="Planen" style="height: 34px;">
                                        <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                           <i class="fa fa-clock-o"></i>
                            </span> Planen
                        </a>
                    <?php } ?>
                </div>
            <?php endif; ?>
            <?php if ($controllername == "cms/hotel" && ci()->acl_lib->has_role('webmaster')): ?>
                 <div style="float: right;">
                     <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/publish_hotel_packages/$item->id") ?>"
                        onclick="return ajax_submit(this);"
                        class="btn btn-default"
                        title="<?=$text?>" >
                         <i class="fa fa-globe"></i> Alle Online-Pakete des Hotels republishen
                     </a>
                 </div>
            <?php endif; ?>
            <?php if ($controllername == "cms/staticpage"): ?>
                <div style="float: right;">
                    <?php if (@$item->cronjob_item) {
                        if (strpos($item->cronjob_item->path, 'unpublish') !== false)
                            $cronjob_text = "geunpublished";
                        else
                            $cronjob_text = "gepublished";
                        ?>
                        <a href="<?= site_url("cronjob/edit/" . $item->cronjob_item->cronjob_id) ?>" class="btn btn-default">
                            <i class="fa fa-clock-o"></i> Seite wird am <?= date("d.m.Y H:i", strtotime($item->cronjob_item->next_execution)); ?> Uhr <?= $cronjob_text ?>.
                        </a>
                    <?php }
                    if (@$staticpage_published)
                        $text = "aktualisieren";
                    else
                        $text = ci()->any_model($modelname)->item_label . " veröffentlichen";
                    ?>
                    <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/publish_staticpage/$item->id") ?>"
                       onclick="return ajax_submit(this);"
                       class="btn btn-default"
                       title="<?=$text?>" >
                        <i class="fa fa-globe"></i> <?= $text ?>
                    </a>
                    <?php if ($staticpage_published) { ?>
                        <? $text = "entfernen";?>
                        <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/unpublish_staticpage/$item->id") ?>"
                           onclick="return ajax_submit(this);"
                           class="btn btn-default"
                           title="<?=$text?>" style="height: 34px;">
                            <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                            <i class="fa fa-globe fa-stack-1x" style="font-size: 0.5em;"></i> <i class="fa fa-ban fa-stack-1x text-danger"></i>
                            </span> <?= $text ?>
                        </a>
                    <?php } ?>
                    <?php if (@!$item->cronjob_item) { ?>
                        <a data-href="javascript:void(0);" href="<?= site_url("cronjob/create?foreign_keys[object_type]=staticpage&foreign_keys[object_id]=$item->id") ?>"
                           onclick="return ajax_submit(this);" class="btn btn-default" title="Planen" style="height: 34px;">
                                        <span class="fa-stack fa-lg" style="width: 1em; height: 1em; line-height: 1em;">
                           <i class="fa fa-clock-o"></i>
                            </span> Planen
                        </a>
                    <?php } ?>
                </div>
            <?php endif; ?>
            <?php if ($controllername == "cms/redirect"): ?>
                <div style="float: right;">
                    <?php
                    if (@$redirect_published)
                        $text = ci()->any_model($modelname)->item_label . " Veröffentlichung aktualisieren";
                    else
                        $text = ci()->any_model($modelname)->item_label . " veröffentlichen";
                    ?>
                    <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/publish_redirect/$item->id") ?>"
                       onclick="return ajax_submit(this);"
                       class="btn btn-default"
                       title="<?=$text?>" >
                        <i class="fa fa-globe"></i> <?= $text ?>
                    </a>
                    <?php if ($redirect_published) { ?>
                        <? $text = "Veröffentlichung entfernen";?>
                        <a data-href="javascript:void(0);" href="<?= site_url("export/publisher/unpublish_redirect/$item->id") ?>"
                           onclick="return ajax_submit(this);"
                           class="btn btn-default"
                           title="<?=$text?>" >
                            <i class="fa fa-globe"></i> <?= $text ?>
                        </a>
                    <?php } ?>
                </div>
            <?php endif; # if redirect ?>
        <?php endif; # (item->id > 0) ?>
    </div>
<?php endif; ?>
