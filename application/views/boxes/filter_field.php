<?php
    $fieldname = $filter_field['fieldname'];
    $fieldvalue = (is_string(@$filter_field['fieldvalue']))? @$filter_field['fieldvalue'] : "";
    $modelname = $filter_field['modelname'];
    $default_params = array(
        "autocomplete_min_length" => 3,
    );
    $params = (is_array(@$filter_field['params']))? array_merge($default_params, $filter_field['params']) : $default_params;
?>

<script>
    $(function() {
        var availableTags = [
            "<?= implode('","',$filter_field[$fieldname]['autocomplete'] ) ?>"
        ];

        $('.controller_<?=$modelname?> .filter_<?=$fieldname?>').tagit({
            <?php if(empty($filter_field[$fieldname]['autocomplete'])) { ?>
            tagSource: function(search, showChoices) {
                $.ajax({
                    url: "<?= $filter_field[$fieldname]['autocomplete_url'] ?>",
                    data: search,
                    success: function(choices) {
                        showChoices(choices);
                    }
                });
            },
            <?php } else { ?>
            availableTags: availableTags,
            <? } ?>
            autocomplete: {
                delay: 0,
                minLength: <?= $params['autocomplete_min_length'] ?>
            },
            singleField: true,
            singleFieldDelimiter: ';',
            allowSpaces: true,
            caseSensitive: false
        });

        $('.controller_<?=$modelname?> .field_<?=$fieldname?> .tagit li input.ui-widget-content').on('keyup', function(event) {
            if(event.keyCode === 13) {
                $('.controller_<?=$modelname?> .field_<?=$fieldname?> form').submit();
            }
        });

    });

    <?php if(!empty($fieldvalue)) { ?>
        ajax_submit('table > thead');
    <?php } ?>
</script>

<div class="ui-widget">
    <input name="filter[<?=$modelname?>][<?=$fieldname?>]" class="filter_<?=$fieldname?>" value="<?= $fieldvalue ?>">
</div>