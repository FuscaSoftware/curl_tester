<?php
/**
 * User: sebra
 * Date: 18.08.16
 * Time: 15:44
 */
# Environment & short_keys
/** @var Bootstrap_lib $bootstrap_lib */
$bootstrap_lib = ci()->bootstrap_lib();
$modelname = $box_index['modelname'];
$dom_id = uniqid("id_");

# Configuration
if (!isset($box_index['show_empty_table']))
    $box_index['show_empty_table'] = false;
$show_table = ($box_index['show_empty_table'] || !empty($items))? true : false;
$foreign_keys = (@$box_index['foreign_keys'])? $box_index['foreign_keys'] : array();

$is_sortable = (@$box_index['sortable'])? true : false;
$sortable_tbody_class = (@$is_sortable)? "sortable" : "";
?>

<div id="<?=$dom_id?>" class="table-container">
    <div class="filter_field_container" style="display: none;">
        <?php foreach ($foreign_keys as $object_type => $object_id): ?>
            <input type='hidden' name='foreign_keys[<?=$object_type?>]' value='<?= $object_id ?>'/>
            <input type='hidden' name='<?=$object_type?>' value='<?= $object_id ?>'/>
        <?php endforeach; ?>
        <?= (@$box_index['cmssearch']) ? "<input type='hidden' name='cmssearch' value='{$box_index['cmssearch']}'>" : "" ?>
        <input type="radio" name="filter[<?=$modelname?>][order_by][field]" value="<?= (@$box_index['filter']['order_by']['field'])? $box_index['filter']['order_by']['field'] : ""?>" onchange="console.log($('input[name=\'filter[<?=$modelname?>][order_by]\']:checked').val());">
        <input type="radio" name="filter[<?=$modelname?>][order_by][direction]" value="<?= (@$box_index['filter']['order_by']['direction'])? $box_index['filter']['order_by']['direction'] : "asc"?>" onchange="console.log($('input[name=\'filter[<?=$modelname?>][order_by]\']:checked').val());">
    </div>
<table class="table table-hover table-striped table-condensed">
<?php if($show_table): ?>
    <?php if ($list_fields_2D && !@$box_index['hide_thead']): ?>
        <thead data-action="<?= @$box_index['filter']['action']?>" data-additional_fields="#<?=$dom_id?> .filter_field_container">
            <tr>
                <? if (@$box_index['sortable']): ?>
                    <th class=""></th>
                <? endif; ?>
                <?php foreach ($list_fields_2D as $k => $v): ?>
                    <?php if ((!in_array($list_fields_2D[$k], array("id","created","modified")) && (!in_array($k, array("id","created","modified")))) || ($box_index['modelname'] == 'content' && in_array($k, array("modified")))): ?>
                        <?php
                        $label = (isset($list_fields_2D[$k]['COLUMN_COMMENT']) && !empty(($list_fields_2D[$k]['COLUMN_COMMENT'])))?
                            $list_fields_2D[$k]['COLUMN_COMMENT']
                            : $k;

                        //TODO: SORTIERUNGSFUNKTION EINBAUEN
                        $filter_order_by_action = "return switch_order_by('#$dom_id', '$modelname', '$k');";
                        $filter_order_by_icon = (@$box_index['filter']['order_by']['direction'] == "desc")? "fa-sort-alpha-desc" : "fa-sort-alpha-asc";
                        $sort_button = (isset($box_index['filter'][$k]))
                            ? '<button class="filter-button btn btn-xs btn-link" onclick="'.$filter_order_by_action.'"><i class="fa '.$filter_order_by_icon.'"></i></button>'
                            : '';

                        $filter_onload = (@$box_index['load_all_filter'] || (isset($box_index['filter'][$k]) && is_string($box_index['filter'][$k])))?
                            "\$(function(){console.log(this);\$('table > thead tr button.filter_".$box_index['modelname']."_".$k."').click();});"
                            : "";
                        $filter_value = (@is_string($box_index['filter'][$k]))? $box_index['filter'][$k] : "";
                        $filter_button = (isset($box_index['filter'][$k]))? (
                            "<button
                             class=\"btn btn-xs btn-link filter_btn filter_".$box_index['modelname']."_".$k."\"
                            onclick=\"return ajax_submit(this)\" data-action='".site_url($box_index['controllername']."/get_filter/".$box_index['modelname']."/".$k)."'>
                            <input type='hidden' name='modelname' value='".$box_index['modelname']."'/>
                            <input type='hidden' name='fieldname' value='".$k."'/>
                            <input type='hidden' name='filter[".$box_index['modelname']."][".$k."]' value='".$filter_value."'/>
                            <i class=\"fa fa-filter\"></i>
                            <script>".$filter_onload."</script>
                            </button>"
                            )
                            : "";
                        ?>
                        <th class="field_<?= $k ?>">
                            <?= form_open('', array("onsubmit"=>"return ajax_submit('table > thead');","onload"=>"",)) ?>
                                <?php if($k != "action"): ?>
                                    <div class="text-uppercase">
                                        <?= $label ?>
                                        <div class="btn-group">
                                            <?= $sort_button ?>
                                            <?= $filter_button ?>
                                        </div>
                                    </div>
                                    <div class="filter"></div>
                                <?php endif; ?>
                            <?= form_close(); ?>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
        </thead>
    <?php endif; ?>
    <tbody class="tbody <?= @$sortable_tbody_class ?>">
    <?php foreach ($items as $item): ?>
        <?= $bootstrap_lib->show_item_row($item, $model, $list_fields_2D, $box_index['modelname'], $box_index['controllername'], @$box_index['foreign_keys'], @$box_index['active_id'], $list_row_view, $box_index) ?>
        <?php #= ci()->get_view($list_row_view, array("CI" => ci(), "box_index" => $box_index, "list_fields" =>$list_fields, "item" => $item, "action_view" => $action_view)) ?>
    <?php endforeach; ?>
    <?php if (@$modelname == "redirect" && isset($box_index['object1_item'])) { ?>
        <tr>
            <td>
                <form action="<?= site_url("cms/redirect/save") ?>" onsubmit="return ajax_submit(this);">
                    <i class="fa fa-plus-circle"></i> NEU:
                    <input title="(Enter zum Speichern!)" style="width: 100%;" class="form-control" placeholder="Keyword" name="data[redirect][<?= $box_index['object1_item']->id ?>][old_url]" value="">
                    <input type="hidden"  name="data[redirect][<?= $box_index['object1_item']->id ?>][new_controller]" value="<?= $box_index['object1_item']->class ?>">
                    <input type="hidden"  name="data[redirect][<?= $box_index['object1_item']->id ?>][new_slug]" value="<?= $box_index['object1_item']->url ?>">
                </form>
            </td>
        </tr>
    <?php } else if (isset($list_fields) && @$modelname != "element") { ?>
    <?php $select_url = site_url($box_index['controllername'] . "/index"); ?>
    <tr class="row_<?=$modelname?>_search" data-action="<?= $select_url ?><?= (isset($box_index['create_params'])) ? $box_index['create_params'] : "" ?>">
        <td colspan="<?= count($list_fields_2D)-3 ?>">
            <?= form_open($select_url.(isset($box_index['create_params'])) ? $box_index['create_params'] : "", ["onsubmit"=>"return ajax_submit('.row_{$modelname}_search');",]) ?>
            <input type="hidden" name="request_type" value="ajax">
            <div class="input-group input-group-sm col-xs-4">
                <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                <input type="text" class="form-control" name="data[filter][<?=$modelname?>][name]">
                <span class="input-group-btn">
                                <button class="btn btn-default btn-sm">
                                    <i class="fa fa-search"></i>
                                    <?= (1 && @ci()->any_model($box_index['modelname'])->item_label) ? @ci()->any_model($box_index['modelname'])->item_label : "Datensatz" ?> suchen
                                </button>
                            </span>
            </div>
            <?= form_close(); ?>
        </td>
    </tr>
    <?php } ?>
    </tbody>
    <?php if (@$box_index['pagination']){ ?>
    <tfoot  data-action="<?= @$box_index['pagination']['next_action'] ?>"
            data-additional_fields="#<?=$dom_id?> .filter_field_container"
            data-additional_fields2="#<?=$dom_id?> thead"
        >
        <tr>
            <td colspan="<?= count($list_fields_2D) ?>">
                <button class="btn btn-link btn-block load-more" href="javascript:void(0);" onclick="return ajax_submit('.box_index table tfoot');" title="Weitere Ergebnisse laden">
                    <i class="fa fa-chevron-down"></i>
                </button>
                <input type="hidden" name="filter[<?= $box_index['modelname'] ?>][skip]" value="<?= $box_index['pagination']['skip'] ?>">
            </td>
        </tr>
    </tfoot>
    <?php } ?>
<?php endif; ?>
</table>
</div>