<?php
/**
 * User: sebra
 * Date: 29.09.16
 * Time: 12:24
 */

if (isset($box_index['sort_url']) && $box_index['sort_url'])
    $sort_url = $box_index['sort_url'];
else
    $sort_url = site_url($box_index['controllername']."/sort/");
?>
<style>
    /*.sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }*/
    /*.sortable tr { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; height: 1.5em; }*/
    /*html>body .sortable tr { height: 1.5em; line-height: 1.2em; }*/
    .ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>
<script>
    $( function() {
        $(".box_<?=$box_index['modelname']?>_index .sortable").sortable({
            handle: '.dragzone',
            placeholder: 'ui-state-highlight',
            forcePlaceholderSize: true,
            update: function (event, ui) {
                var data = $(this).sortable('serialize');
                var url = '<?= $sort_url ?>';
                var jqxhr = $.getJSON(url, data)
                    .done(function (json) {
                        jsonToDom(json);
                        $("body").delay(1000).css("cursor", "default");
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        console.log("Request Failed: " + err);
                    });
            }
        });
    });
</script>
