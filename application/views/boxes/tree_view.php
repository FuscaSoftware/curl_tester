<?php
/**
 * User: sbraun
 * Date: 10.10.17
 * Time: 18:46
 */
$dom_id = uniqid("id_");
?>
<div class="tree_view">
    <style>
        html {
            /*margin: 0;*/
            /*padding: 0;*/
            /*font-size: 62.5%;*/
        }

        body {
            /*max-width: 800px;*/
            /*min-width: 300px;*/
            /*margin: 0 auto;*/
            /*padding: 20px 10px;*/
            /*font-size: 14px;*/
            /*font-size: 1.4em;*/
        }

        h1 {
            /*font-size: 1.8em;*/
        }

        .nav_tree {
            overflow: auto;
            /*border: 1px solid silver;*/
            min-height: 100px;
        }
    </style>
    <link rel="stylesheet" href="<?= site_url() ?>/css/jstree/themes/default/style.css"/>

    <!--    <button class="btn btn-default edit_button">Bearbeiten (neues Fenster)</button>-->
    <div id="<?= $dom_id ?>"
         class="nav_tree"
         data-action="<?= site_url("cms/topic/ajax_js_tree_operation") ?>"
         data-data=""></div>


    <script src="<?= site_url() ?>/js/jstree.js"></script>
    <script>
        var jstree_nav = {
            dom_id: "<?= $dom_id ?>",
            selector: '#' + "<?= $dom_id ?>"
        };
        function jstree_callback(operation, node, node_parent, node_position, more) {
            // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
            // in case of 'rename_node' node_position is filled with the new node name
//                return operation === 'rename_node' ? true : false;
            console.log(operation);
            console.log(node);
            console.log(node_parent);
            console.log(node_position);
            console.log(more);
            if (operation == "move_node") {
                if (more.dnd !== true) {
//                        var data = { operation : operation, node : node, node_parent : node_parent, node_position : node_position, more : more };
                    var params = {
                        request_type: "ajax",
                        date: new Date().valueOf(),
                        data: {
                            "operation": operation,
                            "node": node.id,
                            "node_parent": node_parent.id,
                            "node_position": node_position,
                            "core": more.core
                        }
                    };
//                        var data = {};
//                        $('#<?//=$dom_id?>//').data('data', data);
                    if (0)
                        ajax_data("<?=site_url("cms/topic/ajax_js_tree_operation/")?>", params);
//                        $.post("<?//=site_url("cms/topic/ajax_js_tree_operation/")?>//", params, null, 'json');
//                        jQuery.getJSON("<?////=site_url("cms/topic/ajax_js_tree_operation/")?>////", data, null, 'json');
//                        console.log(params);
//                        return true;
                } else {
                    /*console.log("Move without drop!");*/
                }
            }

        }
        function jstree_change(e, data) {
            if (data.selected.length) {
                console.log(e);
                console.log(data);
                var node = data.instance.get_node(data.selected[0]);
                jstree_nav.node = node;
//                $(':jstree').jstree(true).open_node(jstree_nav.node);
                if (data.event !== undefined) { //in case of not loaded by restoring the tree-state
                    if (node.original.object_type === "package") {
//                        var new_url = site_url + "cms/package/edit/" + node.original.id;
                        var new_url = site_url + "cms/" + node.original.id;
                        if (new_url !== window.location.href) {
                            $('div.main-content').html('');
                            window.location.href = new_url;
                        }
                    } else if (node.original.object_type === "hotel|edit") {
//                        var new_url = site_url + "cms/hotel/" + node.original.id;
                        var new_url = site_url + "cms/" + node.original.id;
                        if (new_url !== window.location.href) {
                            $('div.main-content').html('');
                            window.location.href = new_url;
                        }
                    } else if (node.original.object_type === 'content|edit') {
                        var new_url = site_url + "cms/" + node.original.id;
                        if (new_url !== window.location.href) {
                            window.location.href = new_url;
                        }
                    }
                }



                var topic_id = data.instance.get_node(data.selected[0]).id;
//                alert('The selected node is: ' + data.instance.get_node(data.selected[0]).id);
                if (topic_id > 0 && topic_id !== undefined && topic_id !== 'undefined') {
//                    $('iframe.topic_preview').attr('src', '<?//=site_url('cms/topic/view/')?>//' + topic_id + "?request_type=iframe");
//                    $('iframe.topic_preview').show();
                } else {
//                    $('iframe.topic_preview').hide();
                }
            }
        }
        function jstree_btnclick() {
//            var
            instance = $('#<?=$dom_id?>').jstree(true);
            var sel = instance.get_selected();
            if (!sel.length) {
                return false;
            }
//                console.log(sel);
            sel = sel[0];
            var topic_id = sel;
            var popup = window.open('<?=site_url('cms/topic/view/')?>' + topic_id + "?request_type=html");
            if (!popup)
                window.location.href = '<?=site_url('cms/topic/view/')?>' + topic_id + "?request_type=html";

//                instance.edit(sel);

//            var node = instance.get_node(data.selected[0]).id;
            console.log(instance);
            console.log(sel);
//            instance.deselect_all();
//            instance.select_node('1');
        }
        function open_path() {
//                $(':jstree').jstree(true).open_node(jstree_nav.node);
            var tree = $(':jstree').jstree(true);
            var n1 = tree.get_node('location_root');
            tree.open_node(n1);

        }
        var jstree_root_url = '<?= site_url('scope/get_navigation_tree_sub/0/') ?>';
        var jstree_children_url = '<?= site_url('scope/get_navigation_tree_sub/id/') ?>';
        var tree_data = {
                'url': function (node) {
                    console.log(124);
                    return node.id === '#' ? jstree_root_url : jstree_children_url;
                },
                'data': function (node) {
                    console.log(128);
                    return {
                        'id': (node.id === '#')? 0 : node.id,
                        'icon': node.icon,
                        'date': Date.valueOf().toString().substring(0,10),
                        'object_type': (node.original !== undefined && node.original.object_type !== undefined)?
                            node.original.object_type : ""
                    };
                }
        };


        var topic_root = <?=json_encode(Model_helper_lib::get_tree($items), 1)?>;
        // inline data demo
        $('#<?= $dom_id ?>').jstree({
//            no drag-n-drop is needed for navigation
//            state brings problems because of leaving the tree
//            "plugins": ["dnd", "state"],
            "plugins": ['state'],
            'core': {
//            "check_callback" : false,//If left as false all operations like create, rename, delete, move or copy are prevented.
//            "check_callback" : true,
                'check_callback': jstree_callback,
                'multiple': true,
                'data': tree_data
//            [
//                {
//                    "text": "Root node", "children": [
//                    {"text": "Child node 1"},
//                    {"text": "Child node 2"}
//                ]
//                }
//            ]
            }
        })
            .on("changed.jstree", jstree_change);
        jstree_nav.dom = $('#<?= $dom_id ?>');
        var instance = null;
//        $('.tree_view .edit_button').on("click", jstree_btnclick);
        // bind customize icon change function in jsTree open_node event.
        $(jstree_nav.selector).on('open_node.jstree', function(e, data){
            // avoid errors on "invalid" characters
            var el = document.getElementById(data.node.id);
            var icon = $(el).find('i.jstree-icon.jstree-themeicon').first();
//            var icon = $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first();
            if (icon.hasClass('fa-folder'))
                icon.removeClass('fa-folder').addClass('fa-folder-open');
        });

        // bind customize icon change function in jsTree close_node event.
        $(jstree_nav.selector).on('close_node.jstree', function(e, data){
            // avoid errors on "invalid" characters
            var el = document.getElementById(data.node.id);
            var icon = $(el).find('i.jstree-icon.jstree-themeicon').first();
//            var icon = $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first();
            if (icon.hasClass('fa-folder-open'))
                icon.removeClass('fa-folder-open').addClass('fa-folder');
        });

    </script>
<!--    <iframe class="topic_preview" src="" style="display:none; width: 100%; height: 600px;" frameborder="0"></iframe>-->
</div>