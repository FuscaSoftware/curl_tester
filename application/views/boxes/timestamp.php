<?php $id = "datepicker_" . md5(rand(1,999999)); ?>

<input id="<?= $id ?>" class="<?= $class ?> datepicker" type="text" name="<?= $name ?>" value="<?= $value ?>">
<script>
    $(function() {
        $('#<?= $id ?>.datepicker').flatpickr({
            locale: 'de',
            dateFormat: 'Y-m-d H:i:S',
            <?php if(isset($value)): ?>
            defaultDate: $('#<?= $id ?>.datepicker').val(),
            <?php endif; ?>
            allowInput: true
        });
    });
</script>