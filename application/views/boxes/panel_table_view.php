<?php
/**
 * User: sbraun
 * Date: 09.10.17
 * Time: 15:50
 */
extract($panel);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $title ?></h3>
    </div>
    <div class="panel-body">
        <?= $body ?>
    </div>
    <table class="table table-hover table-striped table-condensed">
        <? foreach ($items as $k => $item) { ?>
        <tr>
            <? foreach ($fields as $kF => $field) { ?>
            <td><?= $item->$field ?></td>
            <? } ?>
        </tr>
        <? } ?>
    </table>
    <div class="panel-footer">
        <?= $footer ?>
    </div>
</div>
