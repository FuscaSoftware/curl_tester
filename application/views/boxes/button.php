<?php
if (is_null($fieldname)) $fieldname = "name";
$disabled = (@$disabled) ? 'disabled="disabled"' : '';
?>
<?php #foreach ( $items as $item ): ?>
<?php $item = (is_array($item)) ? (object)$item : $item;
$icon = (@$icon)?: ci()->bootstrap_lib()->get_element_type_icon($item->id);
?>
<button class="btn btn-default btn-small" data-id="<?= $item->id ?>" type="button">
    <? if ($icon !== false): ?>
    <div><i class="fa <?= $icon ?>" aria-hidden="true"></i></div>
    <? endif; ?>
    <div><?= $item->$fieldname ?></div>

</button>
<?php #endforeach; ?>