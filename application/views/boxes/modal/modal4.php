<?php
/*
 * options:
 *
 * 'box_modal' => [
 *      reload_onclose => [true]|false
 *      modal_size  => modal-lg|modal-sm|null
 * ]
 */

//	call by $('#myModal').modal(options);
//	info/help: http://getbootstrap.com/javascript/#modals
$modal_title = $box_modal['modal_title'];
$modal_content = $box_modal['modal_content'];
$modal_footer_data = @$box_modal['modal']['footer']['data'];
$modal_footer_view = (@$box_modal['modal']['footer']['view']) ? @$box_modal['modal']['footer']['view'] : "boxes/modal/modal_footer4";
$modal_id = (@$box_modal['modal_id']) ? $box_modal['modal_id'] : "myModal_" . time();
$modal_size = $box_modal['modal']['size'] ?? $box_modal['modal_size'] ?? "";# modal-lg | modal-sm
$reload_onclose = $box_modal['modal']['reload_onclose'] ?? $box_modal['reload_onclose'] ?? true;#default true
$fade = (1)? 'fade' : '';
$options = [
    'reload_onclose' => $reload_onclose,
]
?>
<div class="modal <?= $fade ?>" id="<?= $modal_id ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $modal_id ?>_ModalLabel" data-show="true">
    <!-- Modal -->
    <div class="modal-dialog <?= $modal_size ?> draggable resizable" role="document">
        <div class="modal-content">
            <? if ($modal_title !== false): ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="<?= $modal_id ?>_ModalLabel"><?= $modal_title ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <? endif; ?>
            <div class="modal-body">
                <?= $modal_content ?>
            </div>
            <?php if (!strstr($modal_footer_view, "empty")): ?>
                <div class="modal-footer">
                    <?= get_instance()->get_view($modal_footer_view, $modal_footer_data) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <script src="<?= fe_source_with_timestamp('js_hazel/bs4.modal.js') ?>"></script>
    <script>
        $(function () {
            sb_modal_init('#<?= $modal_id ?>', '<?= json_encode($options) ?>');
            console.log('Modal loaded.');
        });
    </script>
    <style>
        .modal.fade {
            -webkit-transition: opacity 0.05s linear;
            -o-transition: opacity 0.05s linear;
            transition: opacity 0.05s linear;
        }
        .modal.fade .modal-dialog {
            -webkit-transition: -webkit-transform 0.3s ease-out;
            transition: -webkit-transform 0.3s ease-out;
            -o-transition: transform 0.3s ease-out;
            transition: transform 0.3s ease-out;
            transition: transform 0.3s ease-out, -webkit-transform 0.3s ease-out;
            /*-webkit-transform: translate(0, -25%);*/
            /*-ms-transform: translate(0, -25%);*/
            /*transform: translate(0, -25%);*/
        }

    </style>
</div>