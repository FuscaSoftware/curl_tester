<?php
//	call by $('#myModal').modal(options);
//	info/help: http://getbootstrap.com/javascript/#modals
$modal_title = $box_modal['modal_title'];
$modal_content = $box_modal['modal_content'];
$modal_footer_data = @$box_modal['modal']['footer']['data'];
$modal_footer_view = (@$box_modal['modal']['footer']['view'])? @$box_modal['modal']['footer']['view'] : "boxes/modal/modal_footer";
$modal_id = (@$box_modal['modal_id']) ? $box_modal['modal_id'] : "myModal_" . time();
$modal_size = (@$box_modal['modal']['size'])? $box_modal['modal']['size'] : "";# modal-lg | modal-sm
$reload_onclose = (isset($box_modal['modal']['reload_onclose']))? $box_modal['modal']['reload_onclose'] : true;#default true
?>
<style type="text/css">
    @media (min-width: 1024px) {
        .modal-lg {
            width: 1000px;
        }
    }
    th {
        max-width: 90px;
    }
</style>
<!-- Modal -->
<div class="modal fade draggable" id="<?= $modal_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-show="true">
	<div class="modal-dialog <?= $modal_size ?>" role="document">
		<div class="modal-content">
            <? if ($modal_title !== false): ?>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?= $modal_title ?></h4>
			</div>
            <? endif; ?>
			<div class="modal-body">
				<?= $modal_content ?>
			</div>
			<?php if (!strstr($modal_footer_view, "empty")): ?>
				<div class="modal-footer">
					<?= get_instance()->get_view($modal_footer_view, $modal_footer_data) ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<script>
		$(function () {
//			console.log('Modal loaded.');
//			$(".draggable").draggable();
//			$(".modal").modal();
			$('#<?= $modal_id ?>').modal();
			$('#<?= $modal_id ?>').on('hidden.bs.modal', function (e) {
				$("body").css("cursor", "progress");
				$('#<?= $modal_id ?>').remove();
				var error = $('.error-messages iframe').contents().find('body').html();
                if (!error && typeof noreload === "undefined")
				    <?= ($reload_onclose)? "location.reload();" : '$("body").css("cursor", "default");'; ?>
			});
			$('#<?= $modal_id ?> .box_edit button:submit').click(function(){
				$("body").css("cursor", "progress");
				ajax_submit("#<?= $modal_id ?> .box_edit_form");
				$('#<?= $modal_id ?>').modal('hide');
				$("body").css("cursor", "default");
				return false;
			});
			$('[data-toggle="tooltip"]').tooltip();
			$('#<?= $modal_id ?> .btn-delete').click(function(){return confirm('Sind Sie sicher?');});
		});
	</script>
</div>