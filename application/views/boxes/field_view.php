<?php
/**
 * User: sbraun
 * Date: 03.10.17
 * Time: 21:41
 */
$label = (!empty($field_info[$field]['COLUMN_COMMENT']))? $field_info[$field]['COLUMN_COMMENT'] : ucfirst($field);
$attr = [
    'field_info' => $field_info[$field],
//    'name' => "package[$field]",
    "name" => "data[$modelname][$item->id][$field]",
    'value' => $item->$field,
//    'class' => "form-control input-sm",
    'class' => "form-control input",
    'onchange' => "package_form(this, $item->id);",
    'onkeyup' => "package_form(this, $item->id);",
];
/*
 * 0: R= Row | D=Div
 * 1  C= Cell | I= Inline
 * 2  #=colspan
 */
$layout = (isset($layout))? strtoupper($layout) : "RC1";
?>
<? if($layout[0] == 'D') { ?>
<div class="form-group">
    <label class="control-label col-sm-2" for="package[<?= $field ?>]"><?= $label ?>:</label>
    <div class="col-sm-10"><?= ci()->bootstrap_lib()->show_field2($attr); ?></div>
</div>
<? } ?>
<? if ($layout[0] == 'R') { ?><tr class=""><? } ?>
    <? if ($layout[1] == 'C') { ?>
    <td <? if(is_numeric($layout[2])) { ?>colspan="<?= $layout[2] ?>"<? } ?>><label for="<?= $attr['name'] ?>"><?= $label ?>:</label></td>
    <td <? if(is_numeric($layout[2])) { ?>colspan="<?= $layout[2] ?>"<? } ?>><div class="form-group"><?= ci()->bootstrap_lib()->show_field2($attr); ?></div</td>
    <? } ?>
<? if ($layout[0] == 'R') { ?></tr><? } ?>