<?php
	if (is_null($fieldname))
	    $fieldname = "name";
    $disabled = (@$disabled)?'disabled="disabled"' : '';
?>
<select class="<?= @$class ?>" name="<?= @$name ?>" onchange="<?= @$onchange ?>" title="<?=@$title."&#10;&#10;".@$name."&#10;".$value?>" autocomplete="off" <?=$disabled?>>
	<?php foreach ( $items as $item ): ?>
		<?php $item = (is_array($item)) ? (object) $item : $item; ?>
		<option value="<?= $item->id ?>" <?= (is_numeric($value) && $value == $item->id || $value == @$item->$fieldname)? 'selected="selected"' : "" ?> >
			<?= $item->$fieldname ?>
		</option>
	<?php endforeach; ?>
</select>