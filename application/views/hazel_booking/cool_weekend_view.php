<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 16:50
 */
?>

<div class="row">
    <!-- Column -->
    <div class="col-md-12 col-lg-12 col-xlg-12">
        <iframe class="sb-maxheight" data-maxheightsub="140" style="width: 100%;min-height:300px;border:none;" src="https://tools.spar-mit.com/cgi-bin/cw_overview.pl?aktion=bookings"></iframe>
    </div>
</div>
<script>
    // use max height:
    var useMaxHeight = function () {
        $('.sb-maxheight').each(function () {
            var maxH = window.visualViewport.height - parseInt($(this).data('maxheightsub')) - $('body nav').outerHeight();
           $(this).css('height', Math.max(maxH, 250));
        });
    };
    useMaxHeight();
    $(window).resize(useMaxHeight);
</script>