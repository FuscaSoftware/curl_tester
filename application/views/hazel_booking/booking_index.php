<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 17:47
 */
?>
<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-2 col-xlg-3">
        <a href="<?= site_url('hazel/booking/create') ?>">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-cart-plus"></i></h1>
                    <h6 class="text-white">Neue Buchung</h6>
                </div>
            </div>
        </a>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-md-12 col-lg-12 col-xlg-12">
        <a href="<?= site_url('hazel/booking/list') ?>">
            <div class="card card-hover">
                <div class="box bg-danger text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-cart-outline"></i></h1>
                    <h6 class="text-white">Buchungen</h6>
                </div>
            </div>
        </a>
    </div>
</div>
