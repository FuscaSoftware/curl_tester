<?php
/**
 * User: sbraun
 * Date: 2019-04-05
 * Time: 16:25
 */
$package_number = "1578";
$config = '
{
"PackageConfig": {
"RoomDesc": "Kategorie Ferienwohnung",
"AllowRoomSelect": "no",
"AllowCalendar": "no",
"PriceWording": "5 Ü/FeWo ab",
"ChildName": "Kinder / Jugendliche",
"ChildDesc": "(0 - 18 J. in derselben FeWo)"
},
"Calendar": {
"0": "Bitte Reisetermin auswählen",
"1": "So., 07.04. - Fr., 12.04.2019",
"2": "So., 14.04. - Fr., 19.04.2019",
"3": "So., 28.04. - Fr., 03.05.2019",
"4": "So., 05.05. - Fr., 10.05.2019",
"5": "So., 12.05. - Fr., 17.05.2019",
"6": "So., 19.05. - Fr., 24.05.2019",
"7": "So., 02.06. - Fr., 07.06.2019",
"8": "So., 16.06. - Fr., 21.06.2019",
"9": "So., 23.06. - Fr., 28.06.2019",
"10": "So., 30.06. - Fr., 05.07.2019",
"11": "So., 07.07. - Fr., 12.07.2019",
"12": "So., 25.08. - Fr., 30.08.2019",
"13": "So., 01.09. - Fr., 06.09.2019",
"14": "So., 08.09. - Fr., 13.09.2019",
"15": "So., 15.09. - Fr., 20.09.2019",
"16": "So., 22.09. - Fr., 27.09.2019",
"17": "So., 29.09. - Fr., 04.10.2019",
"18": "So., 06.10. - Fr., 11.10.2019",
"19": "So., 13.10. - Fr., 18.10.2019",
"20": "So., 20.10. - Fr., 25.10.2019"
},
"AddOptions": {}
';


use \de\fusca\hazelnut\gui\components;


?>
<?php ob_start(); ?>
    <!--<form action="?packageNumber=1578#step3" method="post" name="bookingForm" id="bookingForm" autocomplete="on">-->
    <form action="<?= site_url('json/order/demo') ?>" method="post" name="bookingForm" id="bookingForm" autocomplete="on">
        <script>
            // function recalcInsurance(el) {
            //     $(this).
            // }
        </script>
        <div class="row">
            <div class="col-sm-6 tab-content">
                <!-- TabStep1 -->
                <div role="tabpanel" class="tab-pane active fade in show" id="step1">
                    <!-- Angaben Reisende -->


                    <div class="row">
                        <input class="form-control" id="packageNumber" type="hidden" name="data[packageNumber]" value="1578">
                        <input class="form-control" id="hotelName" type="hidden" name="data[hotelName]" value="Freizeit- und Ferienpark">
                        <div class="col-7 ">
                            <h3>Reisetermin</h3>
                            <div class="form-group">
                                <select class="form-control" name="data[Calendar]" id="Calendar" data-cip-id="jQuery342845639">
                                    <option value="">Bitte Reisetermin auswählen</option>
                                    <option value="07.04.2019 - 12.04.2019">So., 07.04. - Fr., 12.04.2019</option>
                                    <option value="14.04.2019 - 19.04.2019">So., 14.04. - Fr., 19.04.2019</option>
                                    <option value="28.04.2019 - 03.05.2019">So., 28.04. - Fr., 03.05.2019</option>
                                    <option value="05.05.2019 - 10.05.2019">So., 05.05. - Fr., 10.05.2019</option>
                                    <option value="12.05.2019 - 17.05.2019">So., 12.05. - Fr., 17.05.2019</option>
                                    <option value="19.05.2019 - 24.05.2019">So., 19.05. - Fr., 24.05.2019</option>
                                    <option value="02.06.2019 - 07.06.2019">So., 02.06. - Fr., 07.06.2019</option>
                                    <option value="16.06.2019 - 21.06.2019">So., 16.06. - Fr., 21.06.2019</option>
                                    <option value="23.06.2019 - 28.06.2019">So., 23.06. - Fr., 28.06.2019</option>
                                    <option value="30.06.2019 - 05.07.2019">So., 30.06. - Fr., 05.07.2019</option>
                                    <option value="07.07.2019 - 12.07.2019">So., 07.07. - Fr., 12.07.2019</option>
                                    <option value="25.08.2019 - 30.08.2019">So., 25.08. - Fr., 30.08.2019</option>
                                    <option value="01.09.2019 - 06.09.2019">So., 01.09. - Fr., 06.09.2019</option>
                                    <option value="08.09.2019 - 13.09.2019">So., 08.09. - Fr., 13.09.2019</option>
                                    <option value="15.09.2019 - 20.09.2019">So., 15.09. - Fr., 20.09.2019</option>
                                    <option value="22.09.2019 - 27.09.2019">So., 22.09. - Fr., 27.09.2019</option>
                                    <option value="29.09.2019 - 04.10.2019">So., 29.09. - Fr., 04.10.2019</option>
                                    <option value="06.10.2019 - 11.10.2019">So., 06.10. - Fr., 11.10.2019</option>
                                    <option value="13.10.2019 - 18.10.2019">So., 13.10. - Fr., 18.10.2019</option>
                                    <option value="20.10.2019 - 25.10.2019">So., 20.10. - Fr., 25.10.2019</option>
                                </select>
                            </div>
                        </div>
                        <input class="form-control" id="Anreise" type="hidden" name="data[Anreise]" value="">
                        <input class="form-control" id="Abreise" type="hidden" name="data[Abreise]" value="">
                        <div class="col-7">
                            <h3>Erwachsene</h3>
                            <div class="form-group text-center erwachsenewrapper">
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default bootstrap-touchspin-down" type="button" onclick="return ajax_submit('#bookingForm');">
                                            <i class="icon-minus"></i>
                                        </button>
                                    </span>
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="Erwachsene" type="text" value="2" name="data[Erwachsene]" maxlength="2" class="form-control" style="display: block; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;" data-cip-id="Erwachsene">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="icon-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-8 kinderwrapper">
                            <h3>Kinder / Jugendliche</h3>
                            <div class="form-group text-center">
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="icon-minus"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="Kinder" type="text" value="0" name="Kinder" maxlength="2" class="form-control" style="display: block;" data-cip-id="Kinder">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default bootstrap-touchspin-up" type="button">
                                            <i class="icon-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <sup style="color: #838383;top: .5em;">(0 - 18 J. in derselben FeWo)</sup>
                            <div id="KinderAlterAuswahl" class="row">
                            </div>
                        </div>
                        <div class="row"></div>
                        <!-- Desktop Calendar
    <div class="col-sm-7 hidden-xs desktopcalendar" data-toggle="modal" data-target="#calendarModalanreise">
    <div class="calendar_smr" id="mainCalendar"></div>
    </div> -->
                        <div class="col-8 ">
                            <h3 class="headline">Reisegutschein / Partner-ID / Aktionscode
                                <i class="info" data-toggle="modal" data-target="#reisegutscheinmodal"></i></h3>
                            <div class="form-group">
                                <input class="form-control" id="codeInput" type="text" placeholder="Code eingeben" name="data[gsCodes]" data-cip-id="codeInput">
                            </div>
                            <h3 class="headline">
                                Treuebonus <i class="info" data-toggle="modal" data-target="#treuebonus"> </i>
                            </h3>


                            <div class="form-group">
                                <div class="radio">
                                    <label><input checked="checked" value="0" type="radio" name="data[treuebonus]" id="treuebonus0" class="rc-hidden"><i class="radio radio-checked"></i><span id="0prozent">1. Reise</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" value="5" name="data[treuebonus]" id="treuebonus5" class="rc-hidden"><i class="radio"></i><span id="5prozent">2. Reise</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" value="10" name="data[treuebonus]" id="treuebonus10" class="rc-hidden"><i class="radio"></i><span id="10prozent">3. Reise oder mehr</span></label>
                                </div>
                            </div>

                            <?= GUI()->input_select_row(function (components\SelectRow $select) {
                                $select->title = "Treuebonus";
//                                $select->text = "Reise";
                                $select->optgroups = [
                                    $select->new_optgroup("Reisen", [
                                        $select->new_option(1, "1. Reise"),
                                        $select->new_option(1, "2. Reise"),
                                        $select->new_option(1, "3. Reise oder mehr"),
                                    ]),
                                ];
                            }) ?>


                            <h3 class="headline">Reiserücktrittskosten-Schutz
                                <i class="info" data-toggle="modal" data-target="#reiseruecktritt"> </i></h3>
                            <div class="featurebox-checkbox form-group">
                                <!--                        <label class="checkbox-inline">-->
                                <!--                            <input type="checkbox" value="0" name="data[insurance]" id="insurance" onchange="recalcInsurance(this);" class="rc-hidden">-->
                                <!--                            <i class="checkbox"></i><span id="insuranceLabel">€ 24 ,- gesamt</span>-->
                                <!--                        </label>-->
                                <? /*= ci()->bootstrap_lib()->show_field2([
                                    'field_info' => ['type' => 'checkbox'],
                                    'name' => "data[insurance]",
                                    'value' => "0",
                                    'id' => "insurance",
                                    'label_text_after' => "€ 24 ,- gesamt",
                                ])*/ ?>
                                <?= GUI()->checkbox(function (components\Checkbox $checkbox) {
                                    $checkbox->name = "data[insurance]";
                                    $checkbox->value = "0";
                                    $checkbox->id = "insurance";
                                    $checkbox->label_text_after = "€ 24 ,- gesamt";
                                }) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- TabStep2 -->
                <div role="tabpanel" class="tab-pane fade" id="step2">
                    <div id="standardForm">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="radio">
                                        <label><input checked="checked" value="Frau" type="radio" name="data[AnredeReisender]" id="AnredeReisender" class="rc-hidden"><i class="radio radio-checked"></i>
                                            Frau</label>
                                        <label><input value="Herr" type="radio" name="data[AnredeReisender]" id="AnredeReisender" class="rc-hidden"><i class="radio"></i>
                                            Herr</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Vorname *" name="data[VornameReisender]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte Vorname eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Name *" name="data[NachnameReisender]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte Nachname eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Straße, Nr. *" name="data[strasseReisender]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte Straße eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group required">
                                    <input id="plz" class="form-control" type="tel" placeholder="PLZ *" name="data[zip]" maxlength="8">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte Hausnummer eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Ort *" name="data[ortReisender]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte Ort eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" name="LandReisender">
                                        <option value="DE">Deutschland</option>
                                        <option value="AT">Österreich</option>
                                        <option value="CH">Schweiz</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group required">
                                    <input id="email" class="form-control" type="email" placeholder="E-Mail *" name="data[emailReisender]" maxlength="40">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte E-Mail-Adresse eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group required">
                                    <input id="number" class="form-control" type="tel" placeholder="Telefon *" name="data[phone]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                    <p class="error-message">Bitte Telefonnummer eingeben</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group travelgiftForm">
                                    <label class="checkbox-inline">
                                        <input id="Lieferanschrift" type="checkbox" name="data[Lieferanschrift]" value="1" class="rc-hidden"><i class="checkbox"></i><span>Abweichende Lieferanschrift</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div style="display: none;" id="LieferanschriftView">
                            <div class="col-sm-12">
                                <h3>Abweichende Lieferanschrift</h3>
                                <div class="form-group">
                                    <div class="radio">
                                        <label><input checked="checked" value="Frau" type="radio" name="data[anredeLieferanschrift]" id="anredeBeschenkter" class="rc-hidden"><i class="radio radio-checked"></i>
                                            Frau</label>
                                        <label><input value="Herr" type="radio" name="data[anredeBeschenkter]" id="anredeBeschenkter" class="rc-hidden"><i class="radio"></i>
                                            Herr</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Vorname *" name="data[vornameLieferanschrift]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Name *" name="data[nachnameLieferanschrift]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group required">
                                    <input class="form-control" type="text" placeholder="Straße, Nr. *" name="data[strasseLieferanschrift]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group required">
                                    <input class="form-control" type="tel" placeholder="PLZ *" name="data[zipLieferanschrift]" maxlength="8">
                                    <i class="icon-close form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group required">
                                    <input class="form-control" type="Text" placeholder="Ort *" name="data[ortLieferanschrift]" maxlength="30">
                                    <i class="icon-close form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" name="landLieferanschrift">
                                        <option value="DE">Deutschland</option>
                                        <option value="AT">Österreich</option>
                                        <option value="CH">Schweiz</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <h3 class="headline">Wünsche und Bemerkungen</h3>
                            <sub style="color: #838383;">z. B. Kinderbett, Spätanreise, Unverträglichkeiten, Hund</sub>


                            <div class="form-group">
                                <textarea type="text" name="bemerkungen" onkeyup="countChar(this)" class="form-control" style="resize:none" rows="5"></textarea>
                                <div id="charNum" class="lightencolor">320 / 0</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- TabStep 3 -->

                <div role="tabpanel" class="tab-pane fade" id="step3">
                    <div id="responsetext">
                    </div>
                    <div class="producer-part row">
                        <div class="col-xs-6 pull-right">
                            <img width="276" height="213" src="https://media.spar-mit.com/image-handler/media/dms/ccc509efe89af56d89b8e3b6ecaec08a59125bfe/w224/Reiseproducer%20Andr%C3%A9%20Broeders%20224x560.png">
                        </div>
                        <div class="col-xs-6">

                            <p>Ich wünsche Ihnen sonnige Urlaubstage in meiner Region!</p>
                            <br>
                            <p>Viele Grüße,</p>
                            <br>
                            <p>André Broeders</p>
                            <p class="italic">Reiseproducer Reiseproducer Overijssel</p>
                        </div>
                    </div>
                    <div class="beyondlinks">
                        <p><a href="https://www.spar-mit.com/">Zur Startseite</a></p>
                        <p><a href="https://www.spar-mit.com/reisegutschein.php">Reisegutschein bestellen</a></p>
                        <p><a href="https://www.spar-mit.com/service/newsletter">Für den Newsletter anmelden</a></p>
                    </div>
                </div>
            </div>

            <!-- PaketTeaser Desktop/ Mobile-Zusammenfassung -->
            <div class="col-md-4 col-sm-5 packageteaser packageteaser-mobile">
                <div class="hidden-xs">
                    <p class="packageteaser-bar">
                        <span class="packageteaser-packagenumber">Paket 1578</span>
                    </p>
                    <a href="https://spar-mit.com/paket/slagharen" target="_blank" class="paket-link">
                        <h4 class="packageteaser-title">Endlos-Vergnügen in Hollands tollstem Park</h4>
                        <p class="packageteaser-hotel">
                            Slagharen, Overijssel <br>
                            Freizeit- und Ferienpark<br>
                            5 Ü/FeWo ab € 369,00<br>
                        </p>
                    </a>
                    <span class="thumbnail">
                <a href="https://spar-mit.com/paket/slagharen" target="_blank" class="paket-link">
                    <img class="" src="//media.spar-mit.com/image-handler/media/dms/9bbf136f9004daf01cc629f7a88536f257285a0a/w384_0.075-0-0.851-0.638/Slagharen30_Kettenkarussel_Apollo.jpg" alt="Endlos-Vergnügen in Hollands tollstem Park">
                    </a><a class="btn btn-xs btn-primary" style="font-family: 'Asap'; font-size: 10px; z-index: 1; position: absolute; bottom: 5px; right: 5px; box-shadow: 0 0 5px 0 rgba(0,0,0,0.7);" href="https://spar-mit.com/paket/slagharen" target="_blank">Paketinformationen</a>

            </span>
                </div>
                <div class="fixedteaserpart">
                    <p>Wir buchen diese <strong>Spar mit!</strong>-Reise zu den angegebenen Konditionen
                        (eventuelle Saisonpreise, Ermäßigungen und Aufpreise siehe Paketinformationen).</p>
                    <div class="packageteaser-list">
                        <ul class="list-group">
                            <!--
                            <li class="list-group-item">Anreise<span class="text-right" id="TextAnreise">keine Angabe</span></li>

                            <li class="list-group-item">Abreise <span class="text-right" id="TextAbreise">keine Angabe</span></li>

                            <li class="list-group-item">Reisende<span class="text-right" id="AnzahlGast"><span>2 Erwachsene</span></span></li>

                            <li class="list-group-item leistung">Leistung<span class="text-right" id="leistungSummary">5 Ü, Extras</span></li>
                            -->
                            <!--<li class="list-group-item col-xs-4"></li>
                               <li class="list-group-item col-xs-8 text-right" id="additionalSummary"></li>-->
                            <li class="list-group-item" style="display:none;" id="ListadditionalWording1">
                                <span id="additionalWording1"></span><span class="text-right" style="display:none;" id="additionalSummary1"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording0">
                                <!-- Overnights --><span id="additionalWording0"></span><span style="display:none;" class="text-right" id="additionalSummary0"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording06">
                                <!-- SingleMitKind --><span id="additionalWording06"></span><span style="display:none;" class="text-right" id="additionalSummary06"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording01">
                                <!-- AufpreisEZ --><span id="additionalWording01"></span><span class="text-right" style="display:none;" id="additionalSummary01"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListeventWording">
                                <!-- EventPreis --><span id="eventWording"></span><span class="text-right" style="display:none;" id="eventSummary"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListeventWording2">
                                <!-- EventPreis --><span id="eventWording2"></span><span class="text-right" style="display:none;" id="eventSummary2"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListeventWording3">
                                <!-- EventPreis --><span id="eventWording3"></span><span class="text-right" style="display:none;" id="eventSummary3"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording05">
                                <!-- TagesZuschlag --><span id="additionalWording05"></span><span style="display:none;" class="text-right" id="additionalSummary05"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording02">
                                <!-- Mindernacht --><span id="additionalWording02"></span><span style="display:none;" class="text-right" id="additionalSummary02"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording04">
                                <!-- TageWeniger --><span id="additionalWording04"></span><span style="display:none;" class="text-right" id="additionalSummary04"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording03">
                                <!-- Nachlass3rd --><span id="additionalWording03"></span><span style="display:none;" class="text-right" id="additionalSummary03"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording31">
                                <span id="additionalWording31"></span><span class="text-right" style="display:none;" id="additionalSummary31"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWordingReisePreisA">
                                <span id="additionalWordingReisePreisA"></span><span class="text-right" style="display:none;" id="additionalSummaryReisePreisA"></span>
                            </li>

                            <li class="list-group-item bold" style="display:none;" id="ListadditionalWording2pre">
                                <span id="additionalWording2pre"></span><span class="text-right bold greenfont" style="display:none;" id="additionalSummary2pre"></span>
                            </li>
                            <li class="list-group-item bold greenfont" style="display:none;" id="ListadditionalWording2">
                                <span id="additionalWording2"></span><span class="text-right bold greenfont" style="display:none;" id="additionalSummary2"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWordingReisePreisB">
                                <span id="additionalWordingReisePreisB"></span><span class="text-right" style="display:none;" id="additionalSummaryReisePreisB"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording3">
                                <span id="additionalWording3"></span><span class="text-right" style="display:none;" id="additionalSummary3"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWording4">
                                <span id="additionalWording4"></span><span class="text-right" style="display:none;" id="additionalSummary4"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="ListadditionalWordingFini">
                                <span id="additionalWordingFini"></span><span class="text-right" style="display:none;" id="additionalSummaryFini"></span>
                            </li>

                            <li class="list-group-item" style="display:none;" id="PaymentWording">
                                <span id="additionalPaymentWording"></span><span class="text-right" style="display:none;" id="PaymentWordingSummary"></span>
                            </li>

                        </ul>
                    </div>

                    <div class="form-group required agb checkboxpart hide clearfix">
                        <span class="pull-right">
                        <i class="info" data-toggle="modal" data-target="#agbmodal"></i>
                        <label class="checkbox-inline">
                            <span>Wir akzeptieren die AGB</span>
                            <input type="checkbox" id="agb" name="data[agb]" class="rc-hidden"><i class="checkbox"></i>
                        </label>
                            <p class="error-message">Bitte AGB akzeptieren</p>
                    </span>
                    </div>

                    <div class="form-group checkboxpart newsletterpart hide clearfix">
                        <label class="checkbox-inline pull-right">
                            <span id="newsletterLabel">Newsletter jetzt bestellen</span>
                            <input type="checkbox" id="newsletter" name="data[newsletter]" class="rc-hidden"><i class="checkbox"></i>
                        </label>
                    </div>
                    <div class="form-group checkboxpart newsletterpart hide clearfix">
                        <label class="checkbox-inline pull-right">
                            <span id="newsletterLabel"><sup>(Abmeldung jederzeit möglich: <a href="https://www.spar-mit.com/datenschutz.php" target="_blank">Datenschutzerklärung</a>)</sup></span>
                        </label>
                    </div>

                    <ul class="pager wizard wizardbuttons">
                        <li class="previous hide">
                            <a class="btn btn-default" href="#step1" onclick="AllowEventRenew = 'yes';">zurück</a></li>
                        <!--                            <li class="btnstep1 waittonext"><span class="pull-right"><a id="submit_step2" class="btn btn-red -->
                        <!-- pull-right">weiter</a></span></li>-->
                        <li class="btnstep1 waittonext">
                            <span class="pull-right"><a id="submit_step2" class="btn btn-red pull-right">weiter</a></span>
                        </li>
                        <li class="btnstep2 waitingnext hide">
                            <span class="pull-right"><a class="btn btn-red pull-right">Buchung jetzt abschicken</a></span>
                        </li>
                        <li class="btnstep2 next gotonext hide">
                            <a id="submit_buchung" class="btn btn-red pull-right" type="submit" onclick="send();">Buchung
                                jetzt abschicken</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
<?php $plain = ob_get_clean(); ?>
    <pre>
    <? # var_dump($articles); ?>
    <? # var_dump($saison_prices); ?>
</pre>
<?= GUI()->dump(function (components\Dump $dump) use (&$articles) {
    $dump->setText($articles);
}) ?>
<?= GUI()->row(function (components\Row $row) use (&$plain) {
    return GUI()->col(function (components\Column $column) use (&$plain) {
        return GUI()->simpleCard(function (components\SimpleCard $simpleCard) use (&$plain) {
            $simpleCard->setTitle("Buchung");
            $simpleCard->setText($plain);
            /** @var Booking_lib $booking_lib */
            $booking_lib = ci()->booking_lib();
            $sumCardBody = ci()->booking_lib()->get_order_sum_cardBody();
            $simpleCard->addChild($sumCardBody);
        });
    });
}); ?>