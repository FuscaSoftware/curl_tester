<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 17:47
 */
$folds = [
    [
        'label' => "Kunde auswählen",
        'content' => ci()->get_view('hazel_customer/select_and_enter', []),
        'opened' => true,

    ],
    [
        'label' => "Buchungsdaten",
        'content' => ci()->get_view('hazel_booking/booking_data', []),
        'opened' => true,

    ],
//    [
//        'label' => "Demo",
//        'content' => ci()->get_view('hazel_booking/booking_demo', []),
//        'opened' => true,
//
//    ],
    [
        'label' => "Pakete / Beschreibung / Artikel / Leistungen",
//        'content' => ci()->get_view('datatables/samples/custom', []),
        'content' => ci()->get_view('hazel_booking/booking_articles', ['data' => $data]),
        'opened' => true,
//        'no_card_body' => true,
    ],
    [
        'label' => "Summe / Zahlung / Versicherung",
//        'content' => ci()->get_view('datatables/samples/custom', []),
        'content' => ci()->get_view('empty_view', []),
        'opened' => true,

    ],
    [
        'label' => "4",
//        'content' => ci()->get_view('datatables/samples/custom', []),
        'content' => ci()->get_view('hazel_booking/booking_actions', []),
        'opened' => true,

    ],
];
?>
<div class="row">

    <div class="col-md-12">
        <?= ci()->get_view('components/accordian', ['folds' => $folds, 'data' => $data]) ?>
    </div>

    <!--    --><? //= ci()->get_view('components/sample_form', ['folds' => $folds]) ?>

    <!--    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
    <!--    <script defer="defer" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>-->
</div>