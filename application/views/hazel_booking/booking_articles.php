<?php
/**
 * User: sbraun
 * Date: 23.11.18
 * Time: 17:35
 */
/** @var Bs4 $bs4 */
$bs4 = ($this->load->library('bs4')) ? ci()->bs4 : null;
$this->load->helper('sb_helper');
$booking_id = $booking_id ?? $data['booking_id'] ?? 0;
$dt_data = [
    'datatable_id' => uniqid('dt_booking_articles_'),
    'ajax_source' => "http://localhost/hazelnut/hazel/article/ajax_data_json/$booking_id?request_type=ajax",
    'js_file' => "js_hazel/datatables/booking_articles.js",
    'config_key' => 'booking_articles',
    'columns' => [
//            'booking_article.id' => 'id',
            'id' => 'id',
//            'booking_article.text' => 'text'
            'text' => 'text'
    ],
    'table_class' => 'stripe hover row-border',

];
?>
<div class="booking_articles">
    <style>
        .booking_articles .inner {
            overflow: auto;
            min-height: 200px;
        }
        .booking_articles table.dataTable thead,
        .booking_articles table.dataTable tfoot {
            display: none;
        }

        td.details-control {
            background: url('../resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.details td.details-control {
            background: url('../resources/details_close.png') no-repeat center center;
        }


    </style>
    <div class="inner">


<!--        <table id="example" class="display" cellspacing="0" width="100%">-->
<!--            <thead>-->
<!--            <tr>-->
<!--                <th></th>-->
<!--                <th>Paket</th>-->
<!--            </tr>-->
<!--            </thead>-->
<!--        </table>-->

        <?= $bs4->datatable($dt_data) ?>
    </div>

    <script>
        /*
        var editor; // use a global for the submit and return data rendering in the examples

        $(document).ready(function () {
            editor = new $.fn.dataTable.Editor({
                ajax: "http://localhost/hazelnut/hazel/customer/ajax_data_json/?request_type=ajax",
                table: "#example",
                fields: [{
                    label: "First name:",
                    name: "first_name"
                }, {
                    label: "Last name:",
                    name: "last_name"
                }, {
                    label: "Salary:",
                    name: "salary"
                }
                ]
            });

            // Activate an inline edit on click of a table cell
            $('#example').on('click', 'tbody td.editable', function (e) {
                editor.inline(this);
            });

            $('#example').DataTable({
                dom: 'Bfrtip',
                ajax: "http://localhost/hazelnut/hazel/customer/ajax_data_json/?request_type=ajax",
                columns: [
                    {
                        data: null,
                        defaultContent: '',
                        className: 'select-checkbox',
                        orderable: false
                    },
                    {data: 'first_name', className: 'editable'},
                    {data: 'last_name', className: 'editable'},
                    {data: 'position'},
                    {data: 'office'},
                    {data: 'start_date'},
                    {data: 'salary', render: $.fn.dataTable.render.number(',', '.', 0, '$'), className: 'editable'}
                ],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    {extend: 'create', editor: editor},
                    {extend: 'edit', editor: editor},
                    {extend: 'remove', editor: editor}
                ]
            });
        });
        */
    </script>

</div>
