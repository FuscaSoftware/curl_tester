<?php
/**
 * User: sbraun
 * Date: 01.10.18
 * Time: 17:11
 */
$datatable_id = $datatable_id ?? uniqid('dt_');
$js_file = $js_file ?? "js_hazel/datatables/customer.js";
$table_class = $table_class ?? "display";

$options = [
    'columns' => array_keys($columns),
    'ajax_source' => $ajax_source,
    'config_key' => $config_key ?? "datatable",
];
if (isset($initialSearchTerm))
    $options['initialSearchTerm'] = $initialSearchTerm;

if (!empty($data_attribs)) {
    $attribs_0 = [];
    foreach ($data_attribs as $k => $v) {
        $attribs_0[] = "data-{$k}={$v}";
    }
    $attribs = implode(" ", $attribs_0);
}
?>
<div class="hazel-table-container table-responsive" data-modal_id="<?= $modal_id ?? '' ?>" data-base_url="<?= site_url() ?>" <?= $attribs ?? '' ?>>
    <table id="<?= $datatable_id ?>" class="<?= $table_class ?>" style="width:100%">
        <thead>
        <tr>
            <?php foreach ($columns as $k => $column) { ?>
                <th class="<?= @$column['class'] ?>"><?= (!is_array($column)? $column : $column['label'])?></th>
            <? } ?>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <?php foreach ($columns as $k => $column) { ?>
                <th><?= (!is_array($column)? $column : $column['label'])?></th>
            <? } ?>
        </tr>
        </tfoot>
    </table>
    <!--    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
    <!--    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<!--    <script defer="defer" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>-->


    <!--    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>-->
    <!--    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>-->
    <!--    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>-->
    <!--    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>-->
    <script src="<?= fe_source_with_timestamp($js_file) ?>"></script>
    <script>
        var url = site_url + "handlebars/toggle_switch.mustache";
        register_template("toggle_switch", url);
        $(function () {
            var datatable_selector = '#<?= $datatable_id ?>';
            var datatable_options = <?= json_encode($options) ?>;
            sb_init_datatable[datatable_options['config_key']](datatable_selector, null, datatable_options);
            var modal_selector = <?= (!empty($modal_id)) ? "'#$modal_id'" : 'null' ?>;
            if (modal_selector) {
                $(modal_selector).on('hidden.bs.modal', function (e) {
                    delete dt[datatable_selector];
                });
            }
        });
    </script>
</div>