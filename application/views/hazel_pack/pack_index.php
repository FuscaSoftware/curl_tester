<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 17:47
 */
?>
<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-2 col-xlg-3">
        <a href="<?= site_url('hazel/pack/create') ?>">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-plus"></i></h1>
                    <h6 class="text-white">Neues Paket</h6>
                </div>
            </div>
        </a>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-md-12 col-lg-12 col-xlg-12">
        <a href="<?= site_url('hazel/pack/list') ?>">
            <div class="card card-hover">
                <div class="box bg-danger text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-shopping"></i></h1>
                    <h6 class="text-white">Pakete</h6>
                </div>
            </div>
        </a>
    </div>
</div>
