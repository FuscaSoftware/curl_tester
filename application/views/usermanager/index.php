<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 17:47
 */
?>
<div class="row">
    <!-- Column -->
<!--    <div class="col-md-6 col-lg-2 col-xlg-3">-->
<!--        <a href="--><?//= site_url('hazel/customer/create') ?><!--">-->
<!--            <div class="card card-hover">-->
<!--                <div class="box bg-cyan text-center">-->
<!--                    <h1 class="font-light text-white"><i class="mdi mdi-account-plus"></i></h1>-->
<!--                    <h6 class="text-white">Neuer Kunde</h6>-->
<!--                </div>-->
<!--            </div>-->
<!--        </a>-->
<!--    </div>-->
    <!-- Column -->
    <!-- Column -->
    <div class="col-md-4 col-lg-4 col-xlg-4">
        <a href="<?= site_url('user/list') ?>">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-account-multiple-outline"></i></h1>
                    <h6 class="text-white">User</h6>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-4 col-lg-4 col-xlg-4">
        <a href="<?= site_url('user/roles') ?>">
            <div class="card card-hover">
                <div class="box bg-danger text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-account-multiple-outline"></i></h1>
                    <h6 class="text-white">Rollen</h6>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-4 col-lg-4 col-xlg-4">
        <a href="<?= site_url('user/rights') ?>">
            <div class="card card-hover">
                <div class="box bg-orange text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-account-multiple-outline"></i></h1>
                    <h6 class="text-white">Rechte</h6>
                </div>
            </div>
        </a>
    </div>
</div>
