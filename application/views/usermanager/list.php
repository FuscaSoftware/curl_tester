<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 17:47
 */

/** @var Bs4 $bs4 */
$bs4 = ($this->load->library('bs4')) ? ci()->bs4 : null;
if (empty($dt_data)) {
    ci()->message('$dt_data is empty! Programmer was wrong!');
    ci()->dump($dt_data);
    $dt_data = [];
}


?>
<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-2 col-xlg-3">
        <a href="<?= site_url('user/index') ?>">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-account-multiple-outline"></i></h1>
                    <h6 class="text-white">Usermanagement</h6>
                </div>
            </div>
        </a>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-md-12 col-lg-12 col-xlg-12">
        <div class="card">
            <div class="card-body">
<!--                <h5 class="card-title">User</h5>-->
                <?= $bs4->datatable($dt_data) ?>
            </div>
        </div>
    </div>
</div>
