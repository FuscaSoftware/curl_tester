<?php
#<!--<h1><a href="https://github.com/abodelot/jquery.json-viewer">jQuery json-viewer</a></h1>-->

?>
<div class="json_viewer">
    <script src="<?= site_url() ?>assets/json-viewer/jquery.json-viewer.js"></script>
    <link href="<?= site_url() ?>assets/json-viewer/jquery.json-viewer.css" type="text/css" rel="stylesheet">

    <style type="text/css">
        div.json_viewer p.options label {
            margin-right: 10px;
        }

        div.json_viewer p.options input[type=checkbox] {
            vertical-align: middle;
        }

        div.json_viewer textarea#json-input {
            width: 100%;
            height: 200px;
        }

        div.json_viewer pre#json-renderer {
            border: 1px solid #aaa;
        }
    </style>

    <script>
        $(function () {
            function renderJson() {
                try {
                    var input = eval('(' + $('#json-input').val() + ')');
                } catch (error) {
                    return alert("Cannot eval JSON: " + error);
                }
                var options = {
                    collapsed: $('#collapsed').is(':checked'),
                    rootCollapsable: $('#root-collapsable').is(':checked'),
                    withQuotes: $('#with-quotes').is(':checked'),
                    withLinks: $('#with-links').is(':checked')
                };
                $('#json-renderer').jsonViewer(input, options);
            }

            // Generate on click
            $('#btn-json-viewer').click(renderJson);

            // Generate on option change
            $('p.options input[type=checkbox]').click(renderJson);

            // Display JSON sample on page load
            renderJson();
        });
    </script>
    <textarea id="json-input" autocomplete="off">
{ sample: 1 }</textarea>
    <p class="options">
        Options:
        <label title="Generate node as collapsed">
            <input type="checkbox" id="collapsed">Collapse nodes
        </label>
        <label title="Allow root element to be collasped">
            <input type="checkbox" id="root-collapsable" checked>Root collapsable
        </label>
        <label title="Surround keys with quotes">
            <input type="checkbox" id="with-quotes">Keys with quotes
        </label>
        <label title="Generate anchor tags for URL values">
            <input type="checkbox" id="with-links" checked>
            With Links
        </label>
    </p>
    <button id="btn-json-viewer" title="run jsonViewer()">Transform to HTML</button>
    <pre id="json-renderer"></pre>
</div>