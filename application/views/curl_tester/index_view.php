<?php

use de\fusca\hazelnut\gui\components;

# ci()->bootstrap_lib()->show_field2($field, $fieldInfo, $modelname, $value);
$id_card = uniqid("card_");
$space_id = $space_id ?? 1;
?>
<?= GUI()->row(function (components\Row $row) use ($id_card, $space_id) {
    return [
        GUI()->col(function (components\Column $column) use ($id_card, $space_id) {
            $column->setWide();
            return GUI()->simpleCard(function (components\SimpleCard $card) use ($id_card, $space_id) {
                $title = "Curl- & API-Tester";
                $card->id = $id_card;
                $card->addBody($title, function (components\CardBody $cardBody) use ($id_card, $space_id) {
                    return [
                        GUI()->form(function (components\Form $form) use ($id_card, $space_id) {
                            $form->id = "form_curl_tester";
                            $form->onSubmit = 'return ajax_submit(this);';
                            $form->action = site_url('tester/process');
                            return [
                                GUI()->input(function (components\Input $input) use ($id_card, $space_id) {
                                    $input->id = "input_space_id";
                                    $input->name = "data[space_id]";
//                                    $input->title = "URL";
                                    $input->value = $space_id;
                                    $input->type = 'hidden';
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_url";
                                    $input->name = "data[url]";
                                    $input->title = "URL";
                                    $input->value = ci()->cookie_data[$space_id]['url'] ?? "https://";
                                    $input->placeholder = "https://";
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_user";
                                    $input->name = "data[hta_user]";
                                    $input->title = ".htaccess Username";
                                    $input->value = ci()->cookie_data[$space_id]['hta_user'] ?? "";
                                    $input->placeholder = "user";
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_pw";
                                    $input->name = "data[hta_pw]";
                                    $input->title = ".htaccess Password";
                                    $input->value = ci()->cookie_data[$space_id]['hta_pw'] ?? "";
                                    $input->placeholder = "***";
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_get_params";
                                    $input->name = "data[get_params]";
                                    $input->title = "GET Parameter";
                                    $input->value = ci()->cookie_data[$space_id]['get_params'] ?? "";
                                    $input->placeholder = "a=1&b=2";
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_post_params";
                                    $input->name = "data[post_params]";
                                    $input->title = "POST Parameter";
                                    $input->value = ci()->cookie_data[$space_id]['post_params'] ?? "";
                                    $input->placeholder = "d=1&c=2";
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_access_token";
                                    $input->name = "data[access_token]";
                                    $input->title = "Access-Token";
                                    $input->value = ci()->cookie_data[$space_id]['access_token'] ?? "";
                                    $input->placeholder = "080042cad6356ad5dc0a720c18b53b8e53d4c274";
                                }),
                                GUI()->input_text_row(function (components\InputTextRow $input) use ($id_card, $space_id) {
                                    $input->id = "input_timeout";
                                    $input->name = "data[timeout]";
                                    $input->title = "timeout";
                                    $input->value = ci()->cookie_data[$space_id]['timeout'] ?? "";
                                    $input->placeholder = "800";
                                }),
                                GUI()->input_submit_row(function (components\InputSubmitRow $ibr) use ($id_card, $space_id) {
                                    $ibr->title = $ibr->value = "Request!";
                                    $ibr->inputSubmit->value = "Request!";
//                                        $ibr->button->onClick = "send_request();";
                                }),
//                                GUI()->row_form_group(function (components\RowFormGroup $row) {
//                                    $row->addChild("test");
//                                }),
                            ];
                        }),
                    ];
                });
            });
        }),
    ];
})
?>
    <script>
        function send_request() {
            ajax_submit('#form_curl_tester')
            data = {
                'url': $('#input_url').val()
            };
            console.log(data);
            return false;
        }
    </script>
<?= GUI()->row(function (components\Row $row) use ($id_card, $space_id) {
    return [
        GUI()->col(function (components\Column $column) use ($id_card, $space_id) {
            $column->setWide();
            return GUI()->simpleCard(function (components\SimpleCard $card) use ($id_card, $space_id) {
                $title = "Ausgabe:";
                $card->id = $id_card;
                $card->addBody($title, function (components\CardBody $cardBody) {
                    return [
                        GUI()->div(function (components\Div $div) {
                            $div->id = 'output';
                            return ci()->get_view('curl_tester/json_viewer_view');
                        }),
                    ];
                });
            });
        }),
    ];
}) ?>

<?php

# url

# htaccess

# username
# password

# get params

# post params

# fire

##

# result

# header info

# json output

# plain output
