<?php
/**
 * User: sbraun
 * Date: 04.07.18
 * Time: 17:47
 */

use \de\fusca\hazelnut\gui\components;

?>
<?= GUI()->row(
    function (components\Row $row) {
        return [
            GUI()->col(function (components\Column $col) {
                $col->setSmall();
                return GUI()->linkedCard(function (components\Card $card) {
                    $card->setText("Test");
                });
            }),
            GUI()->col(function (components\Column $col) {
                $col->setWide();
                return GUI()->linkedCard(function (components\Card $card) {
                    $card->setText("Hallo Armin");
                });
            }),
            GUI()->col(function (components\Column $col) {
                $col->setHalf();
                return GUI()->linkedCard(function (components\Card $card) {
                    $card->setText("Hallo Bassasam!");
                    $card->setBgColor(components\COLOR_DANGER);
                });
            }),
            GUI()->datatable(function (components\DataTable $dataTable) {
                $dataTable;
            }),
        ];
    })
?>
<pre>
<?
var_dump($data);
?>
</pre>
