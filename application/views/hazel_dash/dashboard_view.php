<?php
/**
 * User: sbraun
 * Date: 03.07.18
 * Time: 15:31
 */
use de\fusca\hazelnut\gui\components;
?>
<?= GUI()->row(function (components\Row $row) {
    return [
        GUI()->col(function (components\Column $column) {
            $column->setSmall();
            return GUI()->linkedCard(function(components\LinkedCard $linkedCard) {
                $linkedCard->setText("Dashboard");
                $linkedCard->setIcon("mdi mdi-view-dashboard");
                $linkedCard->setBgColor(components\COLOR_CYAN);
                $linkedCard->setLink(site_url());
            });
        }),
        GUI()->col(function (components\Column $column) {
            $column->setSmall();
            return GUI()->linkedCard(function(components\LinkedCard $linkedCard) {
                $linkedCard->setText("Buchungen");
                $linkedCard->setIcon("mdi mdi-cart-outline");
                $linkedCard->setBgColor(components\COLOR_WARNING);
                $linkedCard->setLink(site_url('hazel/booking'));

            });
        }),
        GUI()->col(function (components\Column $column) {
            $column->setSmall();
            return GUI()->linkedCard(function(components\LinkedCard $linkedCard) {
                $linkedCard->setText("Kunden");
                $linkedCard->setIcon("mdi mdi-account-multiple-outline");
                $linkedCard->setBgColor(components\COLOR_ORANGE);
                $linkedCard->setLink(site_url('hazel/customer'));
            });
        }),
        GUI()->col(function (components\Column $column) {
            $column->setSmall();
            return GUI()->linkedCard(function(components\LinkedCard $linkedCard) {
                $linkedCard->setText("Pakete");
                $linkedCard->setIcon("mdi mdi-shopping");
                $linkedCard->setBgColor(components\COLOR_DANGER);
                $linkedCard->setLink(site_url('hazel/pack'));
            });
        }),
    ];
}) ?>