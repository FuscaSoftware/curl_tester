<?php

/**
 * Description of Contact_model
 *
 * @author sebra
 */
class Customer_contact_model extends MY_Model
{

	public $table = "customer_contact";
//	public $item_label = "Kontakt/Ansprechpartner";
//	public $items_label = "Kontakte";
//	public $item_class = "CustomerBean";
//	public $full_controller_name = "hazel/customer";
	public $backend_fields = array(
		"id" => array("type" => "hidden"),
//		"lastname" => array(),
//		"firstname" => array(),
//		"company" => array(),
//		"position" => array(),
//		"phone1" => array(),
//		"phone2" => array(),
//		"email1" => array(),
//		"email2" => array(),
//		"address" => array(),
//		"zipcode" => array(),
//		"city" => array(),
	);

//	public $list_fields = array(
//		"id",
//		"lastname",
//		"firstname",
//		"company",
//		"position",
//		"action",
//	);
//	public $linked_by_knot = array("content","producer","offerer","location","package","hotel");

	public function __construct() {
		parent::__construct();
		$this->apply_schema($this->backend_fields);
//		var_dump($this->backend_fields);
	}


}

class CustomerContactBean extends Item
{
    public function get_full_name() {
        return "{$this->first_name} {$this->last_name}";
    }
}