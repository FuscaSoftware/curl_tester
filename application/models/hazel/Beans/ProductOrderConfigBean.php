<?php
/**
 * User: sbraun
 * Date: 2019-04-28
 * Time: 15:05
 */

namespace PrivateProductOrderConfig {
    abstract class ProductOrderConfigBean_2019 extends Item
    {

    }

    abstract class ProductOrderConfigBean_20190430 extends ProductOrderConfigBean_2019
    {

    }
}

namespace {
    /**
     * Class ProductOrderConfigBean Version Wrapper Class
     * Do not change! should always extend latest Version
     */
    final class ProductOrderConfigBean extends ProductOrderConfigBean_20190430
    {

    }
}