<?php

/**
 * Description of Contact_model
 *
 * @author sebra
 */
class Product_order_config_model extends MY_Model
{

    public $table = "product_order_config";
//	public $table2 = "customer_contact";
//	public $table3 = "customer_contact_phone";
//	public $item_label = "Paket";
//	public $items_label = "Pakete";
    public $item_class = "ProductOrderConfigBean";
    public $full_controller_name = "hazel/productOrderConfig";
    public $backend_fields = [
//		"id" => ["type" => "hidden"],
//        'package_number' => [],
//        'package_title' => [],
//        'brand' => [],
//        'default_days' => [],
    ];

    public $list_fields = [
        "id",
//        'trefzer_id',
//        'package_number',
//        'package_title',
//        'brand',
//        'default_days',
//		"lastname",
//		"firstname",
//		"company",
//		"position",
//		"action",
    ];

//	public $linked_by_knot = array("content","producer","offerer","location","package","hotel");

    public function __construct()
    {
        parent::__construct();
        $this->apply_schema($this->backend_fields);
//		var_dump($this->backend_fields);
    }


//	public function get_full_item_by_id($id) {
//        /** @var Customer_contact_model $customer_contact_model */
//        $customer_contact_model = ($this->loader()->model('hazel/customer_contact_model'))? $this->customer_contact_model : null;
//	    $this->db()
//            ->join("$customer_contact_model->table as cc", "customer.id = cc.customer_id");
//	    return $this->get_item_by_id($id, $this->table, [
//	        'select' => "*",
//	        'db' => $this->db(),
//        ]);
//    }

//	public function search($string) {
//	    /** @var Customer_contact_model $customer_contact_model */
//	    $customer_contact_model = ($this->loader()->model('hazel/customer_contact_model'))? $this->customer_contact_model : null;
//	    $result = $this->db()->select('*')
//            ->from("$this->table as c")
//            ->join("$customer_contact_model->table as cc", "c.id = cc.customer_id")
//            ->like('name', $string)
//            ->or_like('last_name', $string)
//            ->get();
//	    $items = new DynCollection($result, $this->item_class);
//	    return $items;
//    }

//    public function join_to_contact(&$db = null, $alias = "cc") {
//        if (is_null($db)) {
//            $db = $this->db();
//        }
////        $db->select("$this->table2.first_name as first_name");
////        $db->select("$this->table2.last_name as last_name");
//        $db->join($this->table2 . " as $alias", "$this->table.id = $alias.customer_id", "LEFT");
//        return $db;
//    }
//
//    public function join_to_phone(&$db = null, $alias3 = "ccp", $alias2 = "cc") {
//        if (is_null($db)) {
//            $db = $this->db();
//        }
//        $db->join($this->table3 . " as $alias3", "$alias2.id = $alias3.customer_contact_id", "LEFT");
//        return $db;
//    }
//
//    public function join_to_mail(&$db = null, $alias3 = "ccpe1", $alias2 = "cc") {
//        if (is_null($db)) {
//            $db = $this->db();
//        }
//        $db->select("$alias3.phone_number as email");
//        $db->join($this->table3 . " as $alias3", "$alias2.id = $alias3.customer_contact_id", "LEFT");
//        return $db;
//    }
}

require_once dirname(__FILE__). "Beans/ProductOrderConfigBean.php";