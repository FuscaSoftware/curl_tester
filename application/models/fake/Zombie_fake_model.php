<?php
/**
 * User: sbraun
 * Date: 11.06.18
 * Time: 12:22
 */

class Zombie_fake_model extends MY_Model
{
    public $table = false;
    public $database_group = 'zombie';

    public function get_regions_per_package() {
        $sql = "SELECT PaketNr, Region.Name, NameLandKurz
FROM Paket
inner join OrtRegion on OrtRegion.OrtID = Paket.OrtID
inner join Region on OrtRegion.RegionID = Region.RegionID";
        $data = $this->db()->query($sql)->result_array();

        return $data;
    }

    public function get_start_dates() {
        $sql = "select package_no, TO_DAYS(CURDATE())-TO_DAYS(first_seen) as days_online FROM package_new";
        $data = $this->db()->query($sql)->result_array();

        return $data;
    }
}