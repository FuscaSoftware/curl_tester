<?php

class Role_user_model extends MY_Model
{

    public $db_fields = ['user_id', 'role_id', 'creator', 'created'];
    public $table = 'acl_role_user';

}
