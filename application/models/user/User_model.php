<?php

class User_model extends MY_Model
{

    /**
    DROP TABLE IF EXISTS `user`;

    CREATE TABLE `user` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `displayname` varchar(100) DEFAULT NULL COMMENT 'Bassam Fatahulla',
    `mailnickname` varchar(100) NOT NULL COMMENT 'b.fatahulla',
    `mail` varchar(100) DEFAULT NULL COMMENT 'b.fatahulla@spar-mit.com',
    `password` varchar(100) DEFAULT NULL,
    `title` varchar(100) DEFAULT NULL COMMENT 'Entwicklung/ Web',
    `department` varchar(100) DEFAULT NULL COMMENT 'Entwicklung/ Web',
    `telephoneNumber` varchar(100) DEFAULT NULL COMMENT 'Telephone Number +41 61 68 52 580',
    `facsimileTelephoneNumber` varchar(100) DEFAULT NULL COMMENT 'Fax Number +41 61 68 52 580',
    `mobile` varchar(100) DEFAULT NULL COMMENT 'Mobile Number +49 172 3555042',
    `creator` varchar(50) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    `modifier` varchar(50) DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_mail_nick_name_uindex` (`mailnickname`)nv
    ) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8

     */

    public $table = 'acl_user';
    const TABLE = 'acl_user';

    protected $use_history = false;

    const PROP_ID = 'id';

    // Mitarbeiter Infos from ldap
    const PROP_MAILNICKNAME = 'mailnickname';       // b.fatahulla
    const PROP_DISPLAYNAME = 'displayname';         // Bassam Fatahulla
    const PROP_MAIL = 'mail';                       // b.fatahulla@spar-mit.com
    const PROP_PASSWORD = 'password';               // Geheim
    const PROP_DEPARTMENT = 'department';           // Entwicklung/ Web
    const PROP_TITLE = 'title';                     // Entwicklung/ Web
    const PROP_TELEPHONENUMBER = 'telephoneNumber';                     // Telephone Number
    const PROP_FACSIMILETELEPHONENUMBER = 'facsimileTelephoneNumber';   // Fax Number
    const PROP_MOBILE = 'mobile';                                       // Mobile Number

    const PROP_ROLE_ADMIN = 'role_admin';
    const PROP_ROLE_PRODUCER = 'role_producer';
    const PROP_ROLE_VERSAND = 'role_versand';

    const PROP_CREATED = 'created';
    const PROP_MODIFIER = 'modifier';
    const PROP_MODIFIED = 'modified';

    /**
     * Login_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param array $to_select
     * @param array $where
     * @return mixed
     */
    public function select_row($to_select = array(), $where = array())
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $result = $this->db->get();

        if (is_array($to_select)) {
            // liefert key => value
            $result = $result->result_array();
           return (!empty($result[0]))? $result[0] :array();
        } else {
            // liefert nur die value ohne key zurück
            return $result->row($to_select);
        }
    }

    /**
     * @param $to_select
     * @param array $where
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function select_rows($to_select, $where = array(), $likes= array(), $order_by = '', $sort = '')
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);

        if(!empty($likes)){
            foreach ($likes as $col => $value){
                $this->db->like($col, $value);
            }
        }
        $this->db->order_by($order_by, $sort);
        return $this->db->get()->result_array();
    }

    /**
     * @param $data_to_insert
     * @return int
     */
    public function insert_row($data_to_insert)
    {
        $this->db->set(self::PROP_CREATED, 'NOW()', false);
        $this->db->insert(self::TABLE, $data_to_insert);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        }
    }

    /**
     * @param $user_data
     * @return int
     */
    public function update_row($data_to_update, $where = array())
    {
        $this->db->set(self::PROP_MODIFIED, 'NOW()', false);
        $this->db->set($data_to_update);

        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->update(self::TABLE);
    }

    public function db():MY_DB_query_builder {
        return $this->db;
    }
}
