<?php

class User_login_log_model extends CI_Model
{


    /**
        DROP TABLE IF EXISTS `user_login_log`;
        CREATE TABLE `user_login_log` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `user_name` varchar(100) NOT NULL,
        `created` datetime NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
     */

    const TABLE = 'acl_user_login_log';

    const PROP_USER_NAME = 'user_name';
    const PROP_CREATED = 'created';


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert_row($user_name = '')
    {

        $this->db->set(self::PROP_CREATED, 'NOW()', false);
        $this->db->insert(self::TABLE, array(self::PROP_USER_NAME => $user_name));
//        if ($this->db->affected_rows()) {
//            return $this->db->insert_id();
//        }
    }

}


