<?php

class User_acl_model extends CI_Model
{

    /**
     *
    DROP TABLE IF EXISTS `user_acl`;

    CREATE TABLE `user_acl` (
    `group_id` int(11) unsigned NOT NULL,
    `role_id` int(11) unsigned NOT NULL,
    `right_id` int(11) unsigned NOT NULL,
    `creator` varchar(50) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    `modifier` varchar(50) DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`group_id`,`role_id`,`right_id`),
    KEY `user_acl_user_role_id_fk` (`role_id`),
    KEY `user_acl_user_right_id_fk` (`right_id`),
    CONSTRAINT `user_acl_user_group_id_fk` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_acl_user_right_id_fk` FOREIGN KEY (`right_id`) REFERENCES `user_right` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_acl_user_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8

     */

    const TABLE = 'user_acl';

    const PROP_ID = 'id';
    const PROP_GROUP_ID = 'group_id';
    const PROP_ROLE_ID = 'role_id';
    const PROP_RIGHT_ID = 'right_id';
    const PROP_CREATOR = 'creator';
    const PROP_CREATED = 'created';
    const PROP_MODIFIER = 'modifier';
    const PROP_MODIFIED = 'modified';

    /**
     * User_right_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param string | array $to_select
     * @param  array $where
     * @return null | array
     */
    public function select_row($to_select, $where = array())
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $result = $this->db->get();

        if (is_array($to_select) || $to_select == '*') {
            // liefert key => value
            $result = $result->result_array();
            return (!empty($result[0])) ? $result[0] : array();
        } else {
            // liefert nur die value ohne key zurück
            return $result->row($to_select);
        }
    }

    /**
     * @param string | array $to_select
     * @param array $where
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function select_rows($to_select, $where = array(), $order_by = '', $sort = '')
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $this->db->order_by($order_by, $sort);
        return $this->db->get()->result_array();
    }

    /**
     * @param array $where
     * @param $in
     * @param $where_in
     * @return array
     * @example
     *
     * select DISTINCT (user_right.right)
     * from user_acl as user_acl
     * JOIN user_right as user_right
     * on user_acl.right_id = user_right.id
     * where user_acl.group_id  = 27
     * and user_acl.right_id in (1, 2 ,3)
     */
    public function get_rights($where = array(), $in, $where_in)
    {
        $this->db->distinct();
        $this->db->select('user_right.right');
        $this->db->from(self::TABLE);
        $this->db->join(User_right_model::TABLE, 'user_acl.right_id = user_right.id');
        $this->db->where($where);
        $this->db->where_in($in, $where_in);
        $query = $this->db->get();

        $active_rights = array();
        foreach ($query->result_array() as $key => $value) {
            $active_rights[] = $value[User_right_model::PROP_RIGHT];
        }
        return $active_rights;
    }

    /**
     * @param $data_to_insert
     * @return int
     */
    public function insert_row($data_to_insert)
    {
        $this->db->set(self::PROP_CREATED, 'NOW()', false);
        $this->db->insert(self::TABLE, $data_to_insert);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        }
    }

    /**
     * @param array $where
     */
    public function delete_row($where = array())
    {
        $this->db->delete(self::TABLE, $where);
    }

    /**
     * @param $arrs
     * @param null $key1
     * @param null $key2
     * @return array
     */
    public function convert_2d_array_to_1d($arrs, $key1, $key2)
    {
        $tmp = array();
        foreach ($arrs as $arr) {
            $tmp[$arr[$key1]] = $arr[$key2];
        }
        return $tmp;
    }
}
