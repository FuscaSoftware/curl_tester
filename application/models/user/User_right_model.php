<?php

class User_right_model extends CI_Model
{

    /**

    DROP TABLE IF EXISTS `user_right`;

    CREATE TABLE `user_right` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `right` varchar(100) DEFAULT NULL,
    `description` tinytext,
    `creator` varchar(50) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    `modifier` varchar(50) DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_right_right_uindex` (`right`)
    ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8

     */

    /**
    INSERT INTO user_right (`right`, description, creator, created, modifier, modified) VALUES ('read', '', 'Bassam Fatahulla', '2017-04-24 09:00:33', null, null);
    INSERT INTO user_right (`right`, description, creator, created, modifier, modified) VALUES ('write', '', 'Bassam Fatahulla', '2017-04-24 09:00:39', null, null);
    INSERT INTO user_right (`right`, description, creator, created, modifier, modified) VALUES ('delete', '', 'Bassam Fatahulla', '2017-04-24 09:00:43', null, null);
    INSERT INTO user_right (`right`, description, creator, created, modifier, modified) VALUES ('publish', '', 'Bassam Fatahulla', '2017-04-24 10:34:30', null, null);
    INSERT INTO user_right (`right`, description, creator, created, modifier, modified) VALUES ('unpublish', '', 'Bassam Fatahulla', '2017-04-24 10:34:38', null, null);
     */

    const TABLE = 'user_right';

    const PROP_ID = 'id';
    const PROP_RIGHT = 'right';
    const PROP_DESCRIPTION = 'description';
    const PROP_CREATOR = 'creator';
    const PROP_CREATED = 'created';
    const PROP_MODIFIER = 'modifier';
    const PROP_MODIFIED = 'modified';

    // rights
    const RIGHT_DELETE = 'delete';
    const RIGHT_READ = 'read';
    const RIGHT_WRITE = 'write';
    const PUBLISH = 'publish';
    const UNPUBLISH = 'unpublish';

    /**
     * User_right_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param array $to_select
     * @param array $where
     * @return mixed
     */
    public function select_row($to_select = array(), $where = array())
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $result = $this->db->get();

        if (is_array($to_select)) {
            // liefert key => value
            $result = $result->result_array();
            return (!empty($result[0])) ? $result[0] : array();
        } else {
            // liefert nur die value ohne key zurück
            return $result->row($to_select);
        }
    }

    /**
     * @param $to_select
     * @param array $where
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function select_rows($to_select, $where = array(), $order_by = '', $sort = '')
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $this->db->order_by($order_by, $sort);
        return $this->db->get()->result_array();
    }

    /**
     * @param $data_to_insert
     * @return int
     */
    public function insert_row($data_to_insert)
    {
        $this->db->set(self::PROP_CREATOR, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_CREATED, 'NOW()', false);

        $this->db->insert(self::TABLE, $data_to_insert);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        }
    }

    /**
     * @param array $where
     */
    public function delete_row($where = array())
    {
        $this->db->delete(self::TABLE, $where);
    }


    /**
     * @param $user_data
     * @return int
     */
    public function update_row($data_to_update, $where = array())
    {
        $this->db->set(self::PROP_MODIFIER, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_MODIFIED, 'NOW()', false);

        $this->db->set($data_to_update);
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->update(self::TABLE);
    }

}
