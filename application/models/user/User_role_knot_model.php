<?php

class User_role_knot_model extends CI_Model
{

    /**
    DROP TABLE IF EXISTS `user_role_knot`;

    CREATE TABLE `user_role_knot` (
    `user_id` int(11) unsigned NOT NULL,
    `role_id` int(11) unsigned NOT NULL,
    `creator` varchar(50) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    PRIMARY KEY (`user_id`,`role_id`),
    KEY `user_role_knot_user_role_id_fk` (`role_id`),
    CONSTRAINT `user_role_knot_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_role_knot_user_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8

    */

    /**
    INSERT INTO user_role_knot (user_id, role_id, creator, created) VALUES (14, 1, 'Bassam Fatahulla', '2017-05-05 09:50:44');
    INSERT INTO user_role_knot (user_id, role_id, creator, created) VALUES (16, 1, 'Bassam Fatahulla', '2017-05-05 09:50:44');
    INSERT INTO user_role_knot (user_id, role_id, creator, created) VALUES (17, 1, 'Bassam Fatahulla', '2017-05-05 09:50:44');
    INSERT INTO user_role_knot (user_id, role_id, creator, created) VALUES (19, 1, 'Bassam Fatahulla', '2017-05-05 09:50:44');
    INSERT INTO user_role_knot (user_id, role_id, creator, created) VALUES (27, 1, 'Bassam Fatahulla', '2017-05-05 09:50:44');
    INSERT INTO user_role_knot (user_id, role_id, creator, created) VALUES (30, 1, 'Bassam Fatahulla', '2017-05-05 09:50:44');
     */

//    const TABLE = 'acl_user_role_knot';
    const TABLE = 'acl_role_user';

    const PROP_ID = 'id';
    const PROP_USER_ID = 'user_id';
    const PROP_ROLE_ID = 'role_id';
    const PROP_CREATOR = 'creator';
    const PROP_CREATED = 'created';

    /**
     * Login_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param $data_to_insert
     * @return int
     */
    public function insert_row($data_to_insert)
    {
        $this->db->set(self::PROP_CREATOR, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_CREATED, 'NOW()', false);
        $this->db->insert(self::TABLE, $data_to_insert);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        }
    }

    /**
     * @param array $where
     */
    public function delete_row($where = array())
    {
        $this->db->delete(self::TABLE, $where);
    }

    /**
     * @param $user_id
     * @return array
     *
     * SELECT user_role.role
     * FROM user_role_knot as user_role_knot
     * JOIN user_role as user_role
     * ON user_role_knot.role_id = user_role.id AND user_role_knot.user_id = 12
     *
     */
    public function get_user_active_roles_by_user_id($user_id)
    {
//        $this->db->select('user_role.role');
        $this->db->select('user_role.role, user_role.id');
        $this->db->from(self::TABLE . " as user_role_knot");
        $this->db->join(User_role_model::TABLE . " as user_role",
            'user_role_knot.role_id = user_role.id AND user_role_knot.user_id =' . $user_id);
        $query = $this->db->get();

        $active_roles = array();
        foreach ($query->result_array() as $key => $value) {
//            $active_roles[$value['role']] = 1;
            $active_roles[$value['role']] = $value['id'];
        }
        return $active_roles;
    }
}
