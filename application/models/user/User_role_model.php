<?php

class User_role_model extends CI_Model
{

    /**

    DROP TABLE IF EXISTS `user_role`;

    CREATE TABLE `user_role` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `role` varchar(100) DEFAULT NULL,
    `description` tinytext,
    `creator` varchar(50) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    `modifier` varchar(50) DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_role_role_uindex` (`role`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8

     */

    /**
    INSERT INTO user_role (role, description, creator, created, modifier, modified) VALUES ('superuser', '', 'Bassam Fatahulla', '2017-04-20 15:50:14', null, null);
    INSERT INTO user_role (role, description, creator, created, modifier, modified) VALUES ('admin', '', 'Bassam Fatahulla', '2017-04-20 15:50:24', null, null);
    INSERT INTO user_role (role, description, creator, created, modifier, modified) VALUES ('user', '', 'Bassam Fatahulla', '2017-04-20 15:50:32', null, null);
     *
     */

//    const TABLE = 'acl_user_role';
    const TABLE = 'acl_role';

    const PROP_ID = 'id';
    const PROP_ROLE = 'role';
    const PROP_DESCRIPTION = 'description';
    const PROP_CREATOR = 'creator';
    const PROP_CREATED = 'created';
    const PROP_MODIFIER = 'modifier';
    const PROP_MODIFIED = 'modified';

    // roles
    const PROP_SUPERUSER = 'superuser';
    const PROP_ADMIN = 'admin';
    const PROP_USER = 'user';

    /**
     * Login_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param array $to_select
     * @param array $where
     * @return mixed
     */
    public function select_row($to_select = array(), $where = array())
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $result = $this->db->get();

        if (is_array($to_select)) {
            // liefert key => value
            $result = $result->result_array();
            return (!empty($result[0])) ? $result[0] : array();
        } else {
            // liefert nur die value ohne key zurück
            return $result->row($to_select);
        }
    }

    /**
     * @param $to_select
     * @param array $where
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function select_rows($to_select, $where = array(), $order_by = '', $sort = '')
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $this->db->order_by($order_by, $sort);
        return $this->db->get()->result_array();
    }

    /**
     * @param $data_to_insert
     * @return int
     */
    public function insert_row($data_to_insert)
    {
        $this->db->set(self::PROP_CREATOR, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_CREATED, 'NOW()', false);

        $this->db->insert(self::TABLE, $data_to_insert);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        }
    }

    /**
     * @param $user_data
     * @return int
     */
    public function update_row($data_to_update, $where = array())
    {
        $this->db->set(self::PROP_MODIFIER, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_MODIFIED, 'NOW()', false);

        $this->db->set($data_to_update);
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->update(self::TABLE);
    }

    /**
     * @param $user_active_roles
     * @return mixed
     */
    public function get_user_active_and_inactive_roles($user_active_roles)
    {
        $result = self::select_rows(User_role_model::PROP_ROLE);

        $active_and_inactive_roles = array();
        foreach ($result as $key => $value) {
            if (!in_array($value['role'], array_keys($user_active_roles))) {
                $active_and_inactive_roles[$value['role']] = 0;
            } else {
                $active_and_inactive_roles[$value['role']] = 1;
            }
        }
        return $active_and_inactive_roles;
    }

    /**
     * @param $arr
     * @return array
     */
    public function get_role_ids_by_role_names($arr)
    {
        $role_ids = array();
        foreach ($arr as $key => $value) {
            $role_ids[] = self::select_row(User_role_model::PROP_ID, array(User_role_model::PROP_ROLE => $key));
        }
        return $role_ids;
    }

    /**
     * @param $arrs
     * @param null $key1
     * @param null $key2
     * @return array
     */
    public function convert_2d_array_to_1d($arrs, $key1, $key2)
    {
        $tmp = array();
        foreach ($arrs as $arr) {
            $tmp[$arr[$key1]] = $arr[$key2];
        }
        return $tmp;
    }


    /**
     * @param array $where
     */
    public function delete_row($where = array())
    {
        $this->db->delete(self::TABLE, $where);
    }

}
