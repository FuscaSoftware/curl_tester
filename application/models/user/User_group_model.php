<?php

class User_group_model extends MY_Model
{
    /**
    DROP TABLE IF EXISTS `user_group`;

    CREATE TABLE `user_group` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `group` varchar(100) DEFAULT NULL,
    `creator` varchar(50) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    `modifier` varchar(50) DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_group_group_uindex` (`group`)
    ) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8

     */

    const TABLE = 'user_group';

    const PROP_ID = 'id';
    const PROP_GROUP = 'group';
    const PROP_CREATOR = 'creator';
    const PROP_CREATED = 'created';
    const PROP_MODIFIER = 'modifier';
    const PROP_MODIFIED = 'modified';

    /**
     * User_group_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param string | array $to_select
     * @param  array $where
     * @return null | array
     */
    public function select_row($to_select, $where = array())
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $result = $this->db->get();

        if (is_array($to_select) || $to_select == '*' ) {
            // liefert key => value
            $result = $result->result_array();
            return (!empty($result[0])) ? $result[0] : array();
        } else {
            // liefert nur die value ohne key zurück
            return $result->row($to_select);
        }
    }

    /**
     * @param $to_select
     * @param array $where
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function select_rows($to_select, $where = array(), $order_by = '', $sort = '')
    {
        $this->db->select($to_select);
        $this->db->from(self::TABLE);
        $this->db->where($where);
        $this->db->order_by($order_by, $sort);
        return $this->db->get()->result_array();
    }

    /**
     * @param $data_to_insert
     * @return int
     */
    public function insert_row($data_to_insert)
    {
        $this->db->set(self::PROP_CREATOR, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_CREATED, 'NOW()', false);

        $this->db->insert(self::TABLE, $data_to_insert);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        }
    }

    /**
     * @param $user_data
     * @return int
     */
    public function update_row($data_to_update, $where = array())
    {
        $this->db->set(self::PROP_MODIFIER, $_SESSION['logged_in']['displayname']);
        $this->db->set(self::PROP_MODIFIED, 'NOW()', false);
        $this->db->set($data_to_update);
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->update(self::TABLE);
    }
}
