<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\Chat;

    require dirname(__DIR__) . '/vendor/autoload.php';

    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        8080
    );

    $server->run();

/* more samples :

<?php
// Your shell script
use Ratchet\Http\OriginCheck;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;

$checkedApp = new OriginCheck(new MyHttpApp, array('localhost'));
$checkedApp->allowedOrigins[] = 'mydomain.com';


$server = IoServer::factory(new HttpServer($checkedApp));
$server->run();


*/