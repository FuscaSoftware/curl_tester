<?php

/**
 * @param null $class
 * @param null $id
 * @return string
 */
function open_div($class = null, $id = null)
{
    $code = '<div ';
    $code .= ($class != null) ? 'class="' . $class . '" ' : '';
    $code .= ($id != null) ? 'id="' . $id . '" ' : '';
    $code .= '>';
    return $code;
}

/**
 * @return string
 */
function close_div()
{
    return '</div>';
}

/**
 * @param null $class
 * @param null $id
 * @return string
 */
function span($class = null, $id = null)
{
    $code = '<span ';
    $code .= ($class != null) ? 'class="' . $class . '" ' : '';
    $code .= ($id != null) ? 'id="' . $id . '" ' : '';
    $code .= '>';
    $code .= '</span>';
    return $code;
}

/**
 * @param null $class
 * @param null $id
 * @return string
 */
function open_span($class = null, $id = null)
{
    $code = '<span ';
    $code .= ($class != null) ? 'class="' . $class . '" ' : '';
    $code .= ($id != null) ? 'id="' . $id . '" ' : '';
    $code .= '>';
    return $code;
}

/**
 * @return string
 */
function close_span()
{
    return '</span>';
}


function strong($text)
{
    return '<strong>' . $text . '</strong>';
}

/**
 * @param $contents
 */
function ausgeben($contents)
{
    echo "<pre>";
    print_r($contents);
    echo "</pre>";
//    die;
}

/**
 * <table class="table">
 * @param null $class
 * @param null $id
 */
function open_table($class = null, $id = null)
{
    $code = '<table ';
    $code .= ($class != null) ? 'class="' . $class . '" ' : '';
    $code .= ($id != null) ? 'id="' . $id . '" ' : '';
    $code .= '>';
    return $code;
}

/**
 * </table>
 * @return string
 */
function close_table()
{
    return '</table>';
}

/**
 * <thead>
 * @return string
 */
function open_thead()
{
    return '<thead>';
}

/**
 * </thead>
 * @return string
 */
function close_thead()
{
    return '</thead>';
}

/**
 * @param array $cols
 * @return string
 */
function open_close_th($cols = array())
{
    $code = '';
    foreach ($cols as $col) {
        $code .= '<th>';
        $code .= $col;
        $code .= '</th>';
    }
    return $code;
}


/**
 * <tbody>
 * @return string
 */
function open_tbody()
{
    return '<tbody>';
}

/**
 * </tbody>
 * @return string
 */
function close_tbody()
{
    return '</tbody>';
}


/**
 * <tr>
 * @return string
 */
function open_tr()
{
    return '<tr>';
}

/**
 * </tr>
 * @return string
 */
function close_tr()
{
    return '</tr>';
}

/**
 * @param array $table_rows
 * @return string
 */
function open_close_td($table_rows = array())
{
    $code = '';
    foreach ($table_rows as $table_row) {
        $code .= '<td>';
        $code .= $table_row;
        $code .= '</td>';
    }
    return $code;
}

/**
 * @param null $class
 * @param null $id
 * @return string
 */
function open_label($class = null, $id = null)
{
    $code = '<label ';
    $code .= ($class != null) ? 'class="' . $class . '" ' : '';
    $code .= ($id != null) ? 'id="' . $id . '" ' : '';
    $code .= '>';
    return $code;
}

/**
 * @return string
 */
function close_label()
{
    return '</label>';
}
