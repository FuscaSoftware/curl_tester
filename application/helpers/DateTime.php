<?php
/**
 * User: sbraun
 * Date: 23.07.18
 * Time: 11:56
 */

namespace fusca;

setlocale(LC_TIME, 'de_DE');
ini_set('date.timezone', 'Europe/Berlin');
date_default_timezone_set('Europe/Berlin');


class DateTime extends \DateTime
{
    /**
     * DateTime constructor.
     *
     * @param string            $time       'now' | unix-timestamp (z.B: @946684800) | iso-datum (z.B: 2010-01-28T15:00:00+02:00).
     * @param DateTimeZone|null $timezone
     *
     * @usage
     *      $d = new \fusca\DateTime('2011-01-01T15:03:01.012345Z');
     *      $birthday = new \fusca\DateTime('1879-03-14');
     */
    public function __construct(string $time = 'now', DateTimeZone $timezone = null) {
//        $timezone_string = 'Europe/Berlin';
        $timezone_string = date_default_timezone_get();
        if ($timezone == 'CET')
            $this->timezone = new \DateTimeZone($timezone_string);
        parent::__construct($time, $this->timezone);
    }

    public function getInterval($days = 1) {
        $day_interval = new DateInterval("P{$days}D");
        return $day_interval;
    }

    public function getDatesUntil(\fusca\DateTime $secondDateTime):array {
        $diff = $this->diff($secondDateTime);
        $nights = (int)$diff->format('%a');
        $one_day_interval = new \DateInterval('P1D');
        $one_day_interval->d = 1;
        if ($nights > 0) {
            $dates = [];
            $dates[] = $obj = clone $this;
            for ($i = 1; $i <= $nights; $i++) {
                $obj = clone $obj;
                $dates[] = $obj->add($one_day_interval);
            }
        }
        return (@$dates)?: null;
    }


    public function getIsoDateFormat():string {
        return $this->format('Y-m-d');
    }

    /**
     * @param   $format
     * @see     http://php.net/manual/de/function.strftime.php
     *
     * @return string
     */
    public function getLocalFormat($format):string {
        if ($format == 'iso')
            $format = '%F';# = %Y-%m-%d
        elseif ($format == 'lang')
            $format = '%A, %e. %B %Y';
        elseif ($format == 'german10')
            $format = "%d.%m.%Y";
        elseif ($format == 'german8')
            $format = "%d.%m.%y";
        elseif ($format == 'german_with_weekday')
            $format = '%A, %d.%m.%Y';
        elseif ($format == 'weekday')
            $format = '%A';
        elseif ($format == 'weekday_short')
            $format = '%a';
        elseif ($format == 'monthname')
            $format = '%B';
        elseif ($format == 'monthname_short')
            $format = '%b';
        elseif ($format == 'kw')
            $format = '%V';
        elseif ($format == 'time5')
            $format = '%H:%M';# = %R
        elseif ($format == 'time8')
            $format = '%H:%M:%S';# = %T
        elseif ($format == 'local_time')
            $format = '%X';
        elseif ($format == 'local_date')
            $format = '%x';
        return strftime($format, $this->getTimestamp());
    }

    public function getGermanDate():string {
        return $this->getLocalFormat('german');
    }


    /**
     * Return Date in ISO8601 format
     *
     * @return String
     */
    public function __toString() {
        return $this->format('Y-m-d H:i');
    }

    /**
     * Return difference between $this and $now
     *
     * @param Datetime|String $now
     * @return DateInterval
     */
    public function diff($now = 'NOW') {
        if(!($now instanceOf DateTime)) {
            $now = new DateTime($now);
        }
        return parent::diff($now);
    }

    /**
     * Return Age in Years
     *
     * @param Datetime|String $now
     * @return Integer
     */
    public function getAge($now = 'NOW') {
        return $this->diff($now)->format('%y');
    }

}