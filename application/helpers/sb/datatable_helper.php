<?php

namespace de\fusca\hazelnut\datatables {
    /**
     * User: sbraun
     * Date: 2019-02-25
     * Time: 15:44
     */
    class DatatableHelper
    {

        public function data_for_json(\MY_Model $model, \Collection $collection, array $columns, int $limit_step, int $limit_skip) {
            $returned_data = $collection->get_array($limit_step, $limit_skip);
            $data =
                [
                    'recordsTotal' => $collection->count(),
                    'recordsFiltered' => $collection->count()/*count($returned_data)*/,
                    'data' => $model->mh()->get_keyless_items(
                        $returned_data,
                        $columns
                    ),
//            'test' => $collection->get_array($dataIn['length'], $dataIn['start']),

            ];
            return $data;
        }

        public function prepare_view_data(
            string  $js_file,
            array   $columns,
            string  $ajax_source,
            string  $config_key
        ) {

            $view_data['js_file'] = "js_hazel/datatables/user.js";
            $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
                'datatable_id' => uniqid('dt_customer_'),
                'columns' => $columns,
                'ajax_source' => site_url() . "json/customer/ajax_data_json/?request_type=ajax",
                'js_file' => "js_hazel/datatables/customer.js",
                'config_key' => 'customer',
                ];
            return $view_data['dt_data'];
        }
    }
}

namespace {

    if (is_object(ci()))
        ci()->dth = new de\fusca\hazelnut\datatables\DatatableHelper();
    else
        $dth = new de\fusca\hazelnut\datatables\DatatableHelper();

    if (!function_exists("DTH")) {
        function DTH(): de\fusca\hazelnut\datatables\DatatableHelper {
            return ci()->dth;
        }
    }
}