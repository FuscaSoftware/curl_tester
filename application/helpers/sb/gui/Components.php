<?php
/**
 * User: sbraun
 * Date: 2019-04-07
 * Time: 02:42
 */

namespace de\fusca\hazelnut\gui\components {

    use mysql_xdevapi\Exception;

    const   COLOR_DANGER = 'danger';
    const   COLOR_WARNING = 'warning';
    const   COLOR_LIGHT = 'light';
    const   COLOR_CYAN = 'cyan';
    const   COLOR_ORANGE = 'orange';
    const   ICON_ACCOUNT_PLUS = 'mdi mdi-account-plus';
    const   ICON_ACCOUNT_MULTIPLE_OUTLINE = 'mdi mdi-account-multiple-outline';

    abstract class Block
    {
        /** @var \Mustache_lib $mustache_lib */
        public $mustache_lib;
        public $id;
//        public function __construct($view_data) {
//
//        }

        public function __construct($function = null) {
            if (is_callable($function)) {
                $r = $function->__invoke($this);
                if (is_array($r))
                    $this->inner = $r;
                else
                    $this->addChild($r);
            }
            if (preg_match('/\.mustache$|^mustache\//i', $this->template)) {
                if (!(ci()->mustache_lib ?? 0))
                    ci()->loader()->library('mustache_lib');
                /** @var \Mustache_lib $mustache_lib */
                $this->mustache_lib = ci()->mustache_lib;
                $this->format = $this->mustache_lib->_load_template($this->template);
            }
        }

        public function __toString() {
            return $this->html();
        }

        public function __get($name) {
            return $this->$name ?? $this->$name() ?? null;
        }

        public function __set($name, $value) {
            if ($name == "inner")
                if (!is_array($value)) {
                    throw new Exception("inner have to be an array. It is " . get_class($value));
                    return false;
                }
            $this->$name = $value;
        }

//        abstract function html();
        public function html($view_data = null) {
            if (preg_match('/\.mustache$|^mustache\//i', $this->template)) {
//                if (!(ci()->mustache_lib ?? 0))
//                    ci()->loader()->library('mustache_lib');
//                /** @var \Mustache_lib $mustache_lib */
//                $mustache_lib = ci()->mustache_lib;
//                $this->format = $mustache_lib->_load_template($this->template);
                $this->base = function () { return site_url(); };
                return $this->mustache_lib->_render($this->format, $this);
            } else {
                if (!$this->template) {
                    print("ERROR: Template not set, of class" . get_class($this));
                }
                $this->format = ci()->get_view($this->template, $view_data ?? $this);
                return sprintf($this->format, $this->innerHtml());
            }
        }

        public function innerHtml() {
//            if (!is_array($this->inner))
//                return implode("\n", $this->inner);
            return $this->arrayToString($this->inner);
//            $fn = null;
//            $fn = function ($inner, $fn) {
//                var_dump($this->inner);
//                die;
//                if (is_array($inner))
//                    return $fn($inner, $fn);
//                return implode("\n", $inner);
//            };
//            return $fn($this->inner, $fn);
        }

        protected function arrayToString($array) {
            if (is_array($array) || is_countable($array))
                if (count($array) > 0 && is_array(current($array)))
                    die("Do not nest arrays in inner. Error in class: " . var_export(get_class($this), 1));
            return implode("\n", $array);
        }

        protected $inner = [];

        public function addChild($child) {
            $childs = func_get_args();
            foreach ($childs as $child)
                $this->inner[] = $child;
        }

        protected $template;
        public $view_data = [];
    }

    class Div extends Block
    {
        protected $template = "components/div";
        public $id = '';
        public $style = '';
    }

    class Row extends Block
    {
        protected $template = "components/row";
//        public function __construct($function) {
//            $function->__invoke($this);
//        }
    }

    class RowFormGroup extends Row
    {
        protected $template = "mustache/input/fg_row.mustache";

    }

    class Card extends Block
    {
        protected $template = "components/card";
        public $icon = "mdi mdi-account-plus";
        public $text = "TEXT";
        public $href = '#';
        public $is_linked = false;
        public $text_center = true;
        public $bg_color = "bg-cyan";

        public function setText($text) {
            $this->text = $text;
        }

        public function setBgColor($color) {
            $this->bg_color = ($color) ? "bg-" . $color : false;
        }

        public function setLink($url) {
            $this->is_linked = true;
            $this->href = $url;
        }
    }

    class LinkedCard extends Card
    {
        public $is_linked = true;

        /**
         * @param string $icon e.g. "mdi mdi-account-plus"
         */
        public function setIcon($icon) {
            $this->icon = $icon;
        }
    }

    class SimpleCard extends Card
    {
        protected $template = "components/cardC";

        public function __construct($function = null) {
            parent::__construct($function);
//            $this->body = new CardBody();
//            $this->body = "";
        }

        /**
         * @param string                $title (Text)
         * @param string|array|callable $body  (Text)
         *
         * @return CardBody
         */
        public function addBody($title, $body) {
            $inner = &$this->inner;
            $inner[] = $cardBody = new CardBody(function (CardBody $cardBody) use (&$inner, &$title, &$body) {
                if (count($inner) >= 1)
                    $cardBody->addClass = "border-top";
                $cardBody->setTitle($title);
                if (is_array($body))
                    $cardBody->setText($body);
                elseif (is_string($body) || method_exists($body, '__toString'))
                    $cardBody->setText($body);
                elseif (is_callable($body))
                    $cardBody->setContent($body($cardBody));
//                var_dump($body);
            });
            return $cardBody;
        }

        public function setText($text) {
            $cardBody = $this->getBody();
            $cardBody->setText($text);
        }

        public function setTitle($title) {
            $cardBody = $this->getBody();
            $cardBody->setTitle($title);
        }

        public function getBody(): CardBody {
            if (!empty($this->inner) && is_a($this->inner[0], CardBody::class)) {
                /** @var CardBody $cardBody */
                $cardBody = $this->inner[0];
            } else {
                $this->inner[] = $cardBody = new CardBody();
            }
            return $cardBody;
        }
//        public $body;
    }

    class CardBody extends Block
    {
        protected $template = "components/card/card_body";

        /**
         * @param string|array $text
         */
        public function setText($text) {
            if (property_exists($this, 'text'))
                $this->text = '';
            if (is_array($text))
                $this->inner = $text;
            else
                $this->inner[0] = $text;
//            ci()->dump($this->inner);
//            ci()->dump($text);
        }

        public function setContent(array $content) {
            return $this->setText($content);
        }

        public function setTitle(string $title) {
            $this->title = $title;
        }

        public function setId(string $id) {
            $this->id = $id;
        }
    }

    class Column extends Block
    {
        protected $template = "components/column";

        public function __construct($function = null) {
            $this->md = 12;
            $this->lg = 12;
            $this->xlg = 12;
            parent::__construct($function);
        }

        public function setSmall() {
            $this->md = 6;
            $this->lg = 2;
            $this->xlg = 3;
        }

        public function setWide() {
            $this->md = 12;
            $this->lg = 12;
            $this->xlg = 12;
        }

        public function setHalf() {
            $this->md = 12;
            $this->lg = 6;
            $this->xlg = 6;
        }
    }

    class Form extends Block
    {
        protected $template = "components/form_view";
        public $action = null;
        public $onSubmit = null;
        public $id = null;
    }

    abstract class aInput extends Block
    {
        public $class = "";
        public $onchange = "";
        public $value = null;
    }

    class Input extends aInput
    {
        protected $template = "boxes/input";
        public $field_info = ['type' => 'text'];
        public $name = "data[checkbox]";
        public $id = null;
        public $label_text_after = "";
        public $label_text_before = "";
        public $onchange = "";
        public $placeholder = null;
        public $type = null;

        public function setType(string $type) {
            if (!in_array($type, ['text', 'password']))
                throw new Exception("Type $type is invalid for " . get_class($this));
            $this->field_info['type'] = $type;
        }
    }

    class Checkbox extends aInput
    {
        protected $template = "boxes/checkbox";
        public $field_info = ['type' => 'checkbox'];
        public $name = "data[checkbox]";
        public $value = "0";
        public $id = null;
        public $label_text_after = "";
        public $label_text_before = "";
        public $onchange = "";
    }

    abstract class aInputRow extends aInput
    {
        protected $template = "mustache/input/input_row.mustache";
        public $title = "";
        public $text = "";
        public $md1 = 4;
        public $md2 = 8;
        public $id = null;
        public $id_row = null;
        public $value = 'null';
        public $placeholder = null;
        public $name = null;

        public function __toString() {
            if (($this->md1 + $this->md2) > 12)
                ci()->dump("Widths are to big!");
            return parent::__toString();
        }
    }

    class InputTextRow extends aInputRow
    {

    }

    class SelectRow extends aInput
    {
        protected $template = "mustache/input/select2_row.mustache";
//        public $field_info = ['type' => 'checkbox'];
//        public $name = "data[checkbox]";
//        public $value = "0";
//        public $id = null;
        public $title = "";
        public $text = "";
        public $onchange = "";

        public $optgroups = [];

        public function new_optgroup($group_label, $group_options) {
            return [L => $group_label, O => $group_options];
        }

        public function new_option($option_value, $option_label) {
            return [V => $option_value, L => $option_label];
        }


    }

    class InputButtonRow extends aInputRow
    {
        protected $template = "mustache/input/input_button_row.mustache";
        /** @var Button $button */
        public $button;

        public function __construct($function = null) {
            $this->button = new Button();
//            $this->button->class = "form-control";
            $this->button->class = "";
            parent::__construct($function);
        }
    }

    class InputSubmitRow extends aInputRow
    {
        protected $template = "mustache/input/input_inputSubmit_row.mustache";
        /** @var InputSubmit $inputSubmit */
        public $inputSubmit;

        public function __construct($function = null) {
            $this->inputSubmit = new InputSubmit();
//            $this->inputSubmit->class = "form-control";
            $this->inputSubmit->class = "";
            parent::__construct($function);
        }
    }

    class DataTable extends Block
    {
        protected $template = "datatables/table";
        public $columns = [
            'id' => 'ID',
            'name' => 'NAME',
        ];

        public function prepare_view_data(
            string $js_file,
            array  $columns,
            string $ajax_source,
            string $config_key
        ) {
            $view_data['js_file'] = "js_hazel/datatables/user.js";
            $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
                'datatable_id' => uniqid('dt_customer_'),
                'columns' => $columns,
                'ajax_source' => site_url() . "json/customer/ajax_data_json/?request_type=ajax",
                'js_file' => "js_hazel/datatables/customer.js",
                'config_key' => 'customer',
            ];
            $this->view_data = $view_data;
        }

        public $ajax_source = null;
    }

    class Alert extends Block
    {
        public function setText($text) {
            $this->text = $text;
        }
    }

    class Dump extends Alert
    {
//        public $text_center = false;
//        public $href = false;
        protected $template = "components/alert";
        public $alert_dismissible = true;
        public $alert_type = "alert-warning";

        public function __construct($function = null, $variable = null) {
            parent::__construct($function);
//            $this->setText($variable);
//            $this->setBgColor(COLOR_LIGHT);
        }

        public $icon = false;

        public function setText($variable) {
//            $variable = 5;
//            var_dump(["hä" => $variable]);
            ob_start();
            var_dump($variable);
//            $text = '<pre>' . var_export($variable, 1) . "</pre>";
            $text = '<pre>' . ob_get_clean() . "</pre>";
            parent::setText($text);
        }
    }

    class InputSubmit extends Input
    {
        protected $template = "mustache/input/submit.mustache";

    }

    class Button extends Block
    {
        public $title = 'Button';
        protected $template = "mustache/input/button.mustache";
//        public $onClick = 'void(0);';
        public $onClick = '';
    }
}