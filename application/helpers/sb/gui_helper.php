<?php

namespace de\fusca\hazelnut\gui {

//    use de\fusca\hazelnut\gui\elements\Card;
//    use de\fusca\hazelnut\gui\elements\Column;
//    use de\fusca\hazelnut\gui\elements\Row;
    use de\fusca\hazelnut\gui\components;

    /**
     * User: sbraun
     * Date: 2019-02-25
     * Time: 15:44
     */
    class GuiHelper
    {
        public function div(callable $function = null) {
            return new components\Div($function);
        }

        public function row(callable $function) {
            return new components\Row($function);
        }

        public function card(callable $function = null) {
            return new components\Card($function);
        }

        public function simpleCard(callable $function = null) {
            return new components\SimpleCard($function);
        }

        public function linkedCard(callable $function = null) {
            return new components\LinkedCard($function);
        }

        public function col(callable $function = null) {
            return new components\Column($function);
        }

        public function datatable(callable $function = null) {
            return new components\DataTable($function);
        }

        public function form(callable $function = null) {
            return new components\Form($function);
        }
        public function input(callable $function = null) {
            return new components\Input($function);
        }

        public function input_checkbox(callable $function = null) {
            return $this->checkbox($function);
        }

        public function checkbox(callable $function = null) {
            return new components\Checkbox($function);
        }

        public function input_text_row(callable $function = null) {
            return new components\InputTextRow($function);
        }

        public function input_select_row(callable $function = null) {
            return new components\SelectRow($function);
        }

        public function input_button_row(callable $function = null) {
            return new components\InputButtonRow($function);
        }

        public function input_submit_row(callable $function = null) {
            return new components\InputSubmitRow($function);
        }

        public function row_form_group(callable $function = null) {
            return new components\RowFormGroup($function);
        }

        public function dump(callable $function = null, $variable = null) {
            return new components\Dump($function, $variable);
        }

        public function prepare_view_data(
            string $js_file,
            array $columns,
            string $ajax_source,
            string $config_key
        ) {

            $view_data['js_file'] = "js_hazel/datatables/user.js";
            $view_data['dt_data'] = [
//                'modal_id' => ci()->data['box_modal']['modal_id'],
                'datatable_id' => uniqid('dt_customer_'),
                'columns' => $columns,
                'ajax_source' => site_url() . "json/customer/ajax_data_json/?request_type=ajax",
                'js_file' => "js_hazel/datatables/customer.js",
                'config_key' => 'customer',
            ];
            return $view_data['dt_data'];
        }
    }
}

namespace {
    require_once __DIR__ . "/gui/Components.php";
    if (is_object(ci()))
        ci()->gui = new de\fusca\hazelnut\gui\GuiHelper();
    else
        $gui = new de\fusca\hazelnut\gui\GuiHelper();

    if (!function_exists("GUI")) {
        define("V", "value");
        define("L", "label");
        define("O", "options");
        function GUI(): de\fusca\hazelnut\gui\GuiHelper {
            return ci()->gui;
        }
    }
}