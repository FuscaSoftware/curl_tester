<?php
/**
 * User: sbraun
 * Date: 29.03.17
 * Time: 16:41
 */

$db['cred'] = [
//    'username' => 'root',
//    'password' => 'root',
];

$db['default_localhost'] = array_merge(
    $db['mysql_default'],
    $db['default_webdev'],
    $db['cred'],
    [
//        'hostname' => '127.0.0.1',
        'database' => 'hazelnut2',
        'cache_on' => false,
        'hostname' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
    ]
);

$db['smr_release_test'] = array_merge(
    $db['mysql_default'],
    $db['smr_release_test'],
    $db['cred'],
    [
//    'hostname' => '127.0.0.1',
    'options' => array(PDO::ATTR_TIMEOUT => 5),
    ]
);

$db['smr-picture_localhost'] = array_merge(
    $db['mysql_default'],
    $db['smr-picture_org'],
    $db['cred'],
    [
//        'hostname' => '127.0.0.1',

    ]
);

$db['smr_frontend'] = array_merge(
    $db['mysql_default'],
    $db['smr_frontend'],
    $db['cred'],
    [
        'database' => 'smr_frontend',
    ]
);
