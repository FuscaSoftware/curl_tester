<?php
/**
 * User: sbraun
 * Date: 29.03.17
 * Time: 16:41
 */

# use live db from HO with localhost
if (0) {
    require __DIR__."/../database/live.php";
//    $db['default_localhost'] = $db['default_cms_productive'];
    $db['default_localhost'] = $db['default_cms_productive'];
    $db['smr-picture_org'] = $db['smr-picture_copy2'];
    $db['smr_release_test'] = $db['smr_live_public'];
    # this fix session to database
    $db['default']['pconnect'] = false;

    $db['default_localhost']['hostname'] = 'cms.spar-mit.com';
    $db['smr_release_test']['hostname'] = 'cms.spar-mit.com';
    $db['smr-picture_org']['hostname'] = 'cms.spar-mit.com';
}
# HOMEOFFICE (local db)
elseif (0) {
//    require_once __DIR__."/entwicklung.php";


    $db['cred'] = [
    'username' => 'root',
    'password' => 'root',
    ];

    $db['default_localhost'] = array_merge(
        $db['mysql_default'],
//        $db['default_webdev'],
        $db['cred'],
        [
            'database' => 'hazelnut2',
            'cache_on' => false,
            'hostname' => '127.0.0.1',
        ]
    );

//    $db['smr_release_test'] = array_merge(
//        $db['mysql_default'],
////        $db['smr_release_test'],
//        $db['cred'],
//        [
//            'database' => 'smr_release_test',
//            'cache_on' => TRUE,
//            'hostname' => '127.0.0.1',
//            'options' => array(PDO::ATTR_TIMEOUT => 5),
//
//        ]
//    );

    $db['smr-picture_localhost'] = array_merge(
        $db['mysql_default'],
//        $db['smr-picture_copy'],
        $db['cred'],
        [
            'cache_on' => TRUE,
            'hostname' => '127.0.0.1',
            'database' => 'smr_dms_copy',
        ]
    );
} else {
    $db['cred'] = [
    ];

    $db['default_localhost'] = array_merge(
        $db['mysql_default'],
//        $db['default_webdev'],
//        $db['default_cms_productive'],
        $db['cred'],
        [
//            'hostname' => '83.169.6.138',
            'hostname' => '192.168.1.223',
            'username' => 'root',
            'password' => 'ciafbi',
            'database' => 'hazelnut',
        ]
    );

    $db['booking'] = array_merge(
        $db['mysql_default'],
//        $db['default_webdev'],
//        $db['default_cms_productive'],
        $db['cred'],
        [
            'hostname' => '83.169.6.138',
//            'hostname' => '192.168.1.223',
//            'username' => 'root',
//            'password' => 'ciafbi',
            'username' => 'smr_live',
            'password' => 'oD60@a2sMv6q!q58',
            'database' => 'hazelnut_booking',
        ]
    );
//
//    $db['default_localhost'] = array(
//        'dsn'	=> '',
//        'hostname' => '192.168.1.223',
//        'username' => 'root',
//        'password' => 'ciafbi',
//        'database' => 'hazelnut',
//        'dbdriver' => 'mysqli',
//        'dbprefix' => '',
//        'pconnect' => FALSE,
//        'db_debug' => (ENVIRONMENT !== 'production'),
//        'cache_on' => FALSE,
//        'cachedir' => '',
//        'char_set' => 'utf8',
//        'dbcollat' => 'utf8_general_ci',
//        'swap_pre' => '',
//        'encrypt' => FALSE,
//        'compress' => FALSE,
//        'stricton' => FALSE,
//        'failover' => array(),
//        'save_queries' => TRUE
//    );

}

ini_set("display_errors", 1);
error_reporting(E_ALL);