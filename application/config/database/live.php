<?php
/**
 * User: sbraun
 * Date: 29.03.17
 * Time: 16:41
 */

$db['smr_live_public'] = [
    'dsn'	=> '',
    'hostname' => '127.0.0.1',
    'username' => 'smr_live',
    'password' => 'oD60@a2sMv6q!q58',
    'database' => 'smr_live',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => FALSE,
    'options' => array(PDO::ATTR_TIMEOUT => 5),
];

$db['default_cms_productive'] = array(
    'dsn'	=> '',
    'hostname' => '127.0.0.1',
    'username' => 'smr_cms',
    'password' => 'oD60@a2sMv6q!q58',
    'database' => 'smr_cms',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => FALSE
);

$db['smr-picture_copy2'] = array(
    'dsn'	=> '',
    'hostname' => '127.0.0.1',
    'username' => 'smr_cms',
    'password' => 'oD60@a2sMv6q!q58',
    'database' => 'smr_dms_copy',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => TRUE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => FALSE,
    'options' => array(PDO::ATTR_TIMEOUT => 6),
);

$db['smr_frontend'] = array(
    'dsn'	=> '',
    'hostname' => '127.0.0.1',
    'username' => 'smr_cms',
    'password' => 'oD60@a2sMv6q!q58',
    'database' => 'smr_frontend',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => TRUE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => FALSE,
    'options' => array(PDO::ATTR_TIMEOUT => 6),
);
