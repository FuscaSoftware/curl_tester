# Curl_Tester

Curl & API Tester

## SETUP

You need to copy the system folder of a codeigniter package to this folder.


* Go to mustache folder (perhaps you have to change path).
    `cd /var/www/vhosts/curltester.spar-mit.com/httpdocs/application/third_party/mustache.php/`
  
* and run `composer install`
* if you have done it as `root` or another user which is not the web you need, run `chown -R curltester.spar-mit.com:psacln .` with the needed user and group.
  

    cd application/third_party/
    git clone https://github.com/bobthecow/mustache.php.git
    cd mustache.php
    composer install
    chown -R curltester.spar-mit.com:psacln .


### 403? 
if Modsecurity makes problems:

    sudo nano /etc/apache2/modsecurity.d/rules/comodo_free/08_Global_Other.conf

make these line to a comment by adding 2 `#`s.

    #SecRule REQUEST_COOKIES|!REQUEST_COOKIES:/__utm/|!REQUEST_COOKIES_NAMES:scarab.profile|REQUEST_COOKIES_NAMES|ARGS_NAMES|ARGS|XML:/* "@pmf bl_os_files" \
    #       "id:210580,msg:'COMODO WAF: OS File Access Attempt||%{tx.domain}|%{tx.mode}|2',phase:2,capture,block,setvar:'tx.points=+%{tx.points_limit4}',logdata:'Matched Data: %{TX>
