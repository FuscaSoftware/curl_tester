#!/usr/bin/env bash

# linux/mac

## ensure to be in project-root-dir:
cd `dirname "$0"`
cd ..
echo `pwd`

ln -s ../hazelnut2_libs/js/           js



#ln -s templates/matrix-admin-bt4-1/assets/   assets
#ln -s templates/matrix-admin-bt4-1/dist/     dist
#ln -s ../hazelnut2_libs/system/       system
#ln -s ../hazelnut2_libs/js/           js
#
#
#
#cd application/
##ln -s ../../hazelnut2_libs/application/helpers/      helpers
#ln -s ../../smr-vier-null-backend/application/helpers/      helpers
#ln -s ../../hazelnut2_libs/application/libraries/    libraries
#mkdir core
#cd core/
##ln -s ../../../hazelnut2_libs/application/core/classes         classes
##ln -s ../../../hazelnut2_libs/application/core/fusca         fusca
##ln -s ../../../hazelnut2_libs/application/core/traits         traits
##ln -s ../../../hazelnut2_libs/application/core/_default         _default
#ln -s ../../../smr-vier-null-backend/application/core/classes         classes
#ln -s ../../../smr-vier-null-backend/application/core/fusca         fusca
#ln -s ../../../smr-vier-null-backend/application/core/traits         traits
#ln -s ../../../smr-vier-null-backend/application/core/_default         _default
#rsync -r _default/ .
#
#cd ..
#mkdir third_party
#cd third_party
#ln -s ../../../hazelnut2_libs/application/third_party/mustache.php/                mustache.php
#cd mustache.php
#composer install
#cd ../../..
#cd application/views/
#ln -s ../../../hazelnut2_libs/application/views/ajax/        ajax
#ln -s ../../../hazelnut2_libs/application/views/boxes/       boxes
#cd ..
#
## logo
## templates
